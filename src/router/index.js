import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import CustomerBOQ from '@/components/CustomerBOQ'
import CustomerBOQListing from '@/components/BOQListing'
import BOQClient from '../services/http/boq.js'
import SummaryCard from '@/components/SummaryCard'
import DesignerCard from '@/components/DesignerCard'
import CostBreakout from '@/components/CostBreakout'
import CustomerBOQV2 from '@/components/CustomerBOQV2'
Vue.use(Router)

const domain = 'http://0.0.0.0:7011//pms/v1/projects/566114/proposals'
const cmsDomain = 'http://axle.alpha.livspace.com/proxy/cmsapi/v2/skus'
const boqListingDomain = 'http://axle.alpha.livspace.com/proxy/backoffice/api/v1/project/566114/proposals?page=1&count=100&filters=project_id:566114;status:All'
const vendorDomain = 'http://axle.alpha.livspace.com/vms/api/v1/vendoritems/sku'
const novaDomain = "http://axle.alpha.livspace.com/proxy/livspace-web-backend/v1/contents/by.slug"
const client = new BOQClient(domain, cmsDomain, boqListingDomain, vendorDomain, novaDomain)

import VueApexCharts from 'vue-apexcharts'
Vue.use(VueApexCharts)

Vue.component('apexchart', VueApexCharts)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/boqs',
      name: 'CustomerBOQListing',
      component: CustomerBOQListing,
      props: {dataClient: client}
      // props: { domain: domain, project_id: project_id, boq_id:boq_id, cmsDomain: cmsDomain }
    },
    {
      path: '/boqs/:boqId',
      name: 'CustomerBOQ',
      component: CustomerBOQ,
      props: (route) => ({ dataClient: client, boqId: parseInt(route.params.boqId)})
    },
    {
      path: '/cards/summary',
      name: 'SummaryCard',
      component: SummaryCard,
      // props: (route) => ({ dataClient: client, boqId: route.params.boqId})
    },
    {
      path: '/cards/designer',
      name: 'DesignerCard',
      component: DesignerCard,
      // props: (route) => ({ dataClient: client, boqId: route.params.boqId})
    },
    {
      path: '/cards/costbreakout',
      name: 'CostBreakout',
      component: CostBreakout,
      // props: (route) => ({ dataClient: client, boqId: route.params.boqId})
    },
    {
      path: '/v2/boqs/:boqId',
      name: 'CustomerBOQV2',
      component: CustomerBOQV2,
      props: (route) => ({ dataClient: client, boqId: parseInt(route.params.boqId)})
    },
  ]
})
