// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

// bootstrapVue config
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import VueMasonry from 'vue-masonry-css'

Vue.use(BootstrapVue)
Vue.use(VueMasonry);

// import { library } from '@fortawesome/fontawesome-svg-core'
// import { faChevronRight } from '@fortawesome/free-solid-svg-icons'
// import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// library.add(faChevronRight)
// Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

Vue.filter('toCurrency', function (value, currencyType, countryISOCode) {
  if (!currencyType) {
    currencyType = 'INR'
  }
  if (!countryISOCode) {
    countryISOCode = 'IN'
  }
  let formatter = new Intl.NumberFormat("en-"+countryISOCode, {
    style: 'currency',
    currency: currencyType,
    minimumFractionDigits: 0
  })

  return formatter.format(value)
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
