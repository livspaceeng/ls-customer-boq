import axios from 'axios'
// Vue.use(VueResource);

export default class BOQClient {
  constructor (boqDomain, cmsDomain, boqListingDomain, vendorDomain, novaDomain) {
    this.boqDomain = boqDomain
    this.cmsDomain = cmsDomain
    this.boqListingDomain = boqListingDomain
    this.vendorDomain = vendorDomain
    this.novaDomain = novaDomain
  }

  getBOQDetails (boqId) {
    let headers = {
      headers: {
        'X-Requested-By': 0,
        'Authorization': 'Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln'
      }
    }
    return axios.get(this.boqDomain + '/' + boqId + '?include=settings,boq_tnc', headers)
  }

  getCMSDetails (skuList) {
    let headers = {
      headers: {
        'X-Client-Id': 'StarMSFe-G4DsKI',
        'X-Requested-By': 0,
        'X-Client-Secret': '2SXhLxG7prabkWl2rHQptbpIyt8XH9yg',
        'Content-Type': 'application/json'
      }
    }
    let data = {
      code: skuList
    }
    return axios.post(this.cmsDomain, data, headers)
  }

  getProjectBoqs () {
    let headers = {
      headers: {
        'X-Requested-By': 0,
        'Authorization': 'Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln'
      }
    }
    return axios.get(this.boqListingDomain, headers)
  }

  getVendorDetails (skuCode) {
    let headers = {
      headers: {
        'X-Client-Id': 'StarMSFe-G4DsKI',
        'X-Requested-By': 0,
        'X-Client-Secret': '2SXhLxG7prabkWl2rHQptbpIyt8XH9yg',
        'Content-Type': 'application/json'
      }
    }
    return axios.get(this.vendorDomain+"/"+skuCode+"/vendors", headers)
  }

  getNovaContent (slug) {
    let headers = {
      headers: {
        'X-Client-Id': 'StarMSFe-G4DsKI',
        'X-Requested-By': 0,
        'X-Client-Secret': '2SXhLxG7prabkWl2rHQptbpIyt8XH9yg',
        'Content-Type': 'application/json'
      }
    }
    return axios.get(this.novaDomain+"/"+slug, headers)
  }

}
