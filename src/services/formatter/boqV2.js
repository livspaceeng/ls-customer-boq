import moment from 'moment'

export default class BOQFormatter {

  constructor (dataClient, boqId) {
    this.dataClient = dataClient;
    this.boqId = boqId;
    this.boqDetails = null;
    this.videoUrls = null;
    this.currencyType = "INR";
    this.countryISOCode = "IN";
    this.tncContent = null;
  }

  setBOQDetails(boqId, loaded, videoLoader){
    this.dataClient.getBOQDetails(boqId).then(data => {
      this.boqDetails = data.data;
      this.countryISOCode = this.boqDetails.project.country_code;
      this.currencyType = this.boqDetails.project.currency;
      loaded.value = true;
      console.log(data);
      this.getLivHomeVideos(videoLoader)
      // return this.boqDetails;
    }).catch(error => {
      console.log(error)
    })
  }

  getData(){
    return this.boqDetails;
  }

  getSummaryDetails(){
    var summary = {}
    summary["totalProducts"] = this.boqDetails.products.length;
    var rooms = new Set();
    this.boqDetails.products.forEach(function(product){
      if(!rooms.has(product.room_tag.id))
          rooms.add(product.room_tag.id);
    });
    summary["roomCount"] = rooms.size;
    summary["totalPrice"] = Math.round(this.boqDetails.proposal.net_payable);
    summary["payByDate"] = moment(this.boqDetails.proposal.pay_by_date).format('Do MMM, YYYY');
    summary["remainingDays"] = Math.round((new Date(this.boqDetails.proposal.pay_by_date.replace(/-/g, "/")) - new Date())/(1000*60*60*24))
    return summary;
  }

  getLivHomeVideoUrls(){
    return this.videoUrls;
  }


  getLivHomeVideos(isLoaded){
    //get custom sku details and fetch url in it
    var modular_skus = []
    this.boqDetails.products.forEach(function(product){
      if(product.group_skus.length>0){
        modular_skus.push(product.sku_code);
      }
    });
    var videoUrls = new Set();

    if(modular_skus.length>0){
      this.dataClient.getCMSDetails(modular_skus).then(data => {
        data.data.forEach(function(skuDetail){
          skuDetail.images.forEach(function(image){
            var imageUrl = image.image_url;
            var parser = new URL(imageUrl);
            if(parser.pathname.substring(parser.pathname.length-4, parser.pathname.length) == "webm" || parser.pathname.substring(parser.pathname.length-3, parser.pathname.length) == "mp4")
              videoUrls.add(imageUrl);
          });
        });
        isLoaded.value = true
        var urls = Array.from(videoUrls);
        var sortedUrls = []
        urls.forEach(function(url){
          var parser = new URL(url);
          if(parser.pathname.substring(parser.pathname.length-4, parser.pathname.length) == "webm")
            sortedUrls.push(url);
          else {
            sortedUrls.unshift(url);
          }
        });

        this.videoUrls = sortedUrls;
      }).catch(error => {
        console.log(error)
      })
    }
  }

  getCostBreakOutDetails(){
    var costBreakOutDetails = {};
    var scopeWiseDetails = {
      "modular": 0,
      "civil": 0,
      "others": 0
    };
    var roomWiseDetails = new Map();
    this.boqDetails.products.forEach(function(product){
      var productPrice = Math.ceil(product.quantity * product.selling_price)
      if(!roomWiseDetails.has(product.room_tag.display_name))
          roomWiseDetails.set(product.room_tag.display_name, {'value':0, 'roomId':product.room_tag.id})
      var room = roomWiseDetails.get(product.room_tag.display_name)
      room['value'] = room['value'] + productPrice;
      if(product.group_skus.length>0)
        scopeWiseDetails["modular"] = scopeWiseDetails["modular"] + productPrice;
      else if(product.vendor_type === 'CANVAS_VENDOR')
        scopeWiseDetails["civil"] = scopeWiseDetails["civil"] + productPrice;
      else
        scopeWiseDetails["others"] = scopeWiseDetails["others"] + productPrice;
    });
    costBreakOutDetails["scope"] = scopeWiseDetails;
    costBreakOutDetails["room"] = roomWiseDetails;
    return costBreakOutDetails;
  }

  getDesignerDetails(){
    var designerDetails = {};
    var designer = this.boqDetails.primary_designer || {};
    designerDetails["designerName"] = designer.display_name;
    designerDetails["designerPhone"] = designer.phone;
    designerDetails["designerEmail"] = designer.email;
    designerDetails["designerImage"] = designer.image_url;
    designerDetails["customerName"] = (this.boqDetails.project && this.boqDetails.project.customer_display_name);
    return designerDetails;
  }

  getProjectAmountDetails(){
    var amountDetails = {};
    amountDetails["handlingFee"] = Math.round(this.boqDetails.proposal.handling_fee);
    amountDetails["subTotal"] = Math.round(this.boqDetails.proposal.total_price);
    amountDetails["discount"] = Math.round(this.boqDetails.proposal.discount);
    amountDetails["total"] = Math.round(this.boqDetails.proposal.net_payable);
    amountDetails["coupons"] = this.boqDetails.proposal.coupons_metadata
    return amountDetails;
  }

  getRoomScopeBreakOutDetails(){
    var roomScopeBreakOutDetails = new Map();
    this.boqDetails.products.forEach(function(product){
      var productPrice = Math.ceil(product.quantity * product.selling_price)
      if(!roomScopeBreakOutDetails.has(product.room_tag.id))
          roomScopeBreakOutDetails.set(product.room_tag.id, {
              'scopes':{
                'modular': {
                  'skus': [],
                  'total': 0,
                  'children': new Map(),
                  'skuQuantity': new Map(),
                  'skuPrice': new Map(),
                  'childSkuPrices': new Map()
                },
                'civil': {
                  'skus': [],
                  'total': 0,
                  'skuQuantity': new Map(),
                  'skuPrice': new Map()
                },
                'others': {
                  'skus': [],
                  'total': 0,
                  'skuQuantity': new Map(),
                  'skuPrice': new Map()
                }
              },
              'skus': [],
              'total': 0
          });
      var currentRoomBreakUp = roomScopeBreakOutDetails.get(product.room_tag.id)
      currentRoomBreakUp['skus'].push(product.sku_code);
      currentRoomBreakUp['total'] = currentRoomBreakUp['total'] + productPrice;
      if(product.group_skus.length>0){
        currentRoomBreakUp['scopes']["modular"]["total"]  = currentRoomBreakUp['scopes']["modular"]["total"] + productPrice;
        currentRoomBreakUp['scopes']["modular"]['skus'].push(product.sku_code);
        currentRoomBreakUp['scopes']["modular"]['skuQuantity'].set(product.sku_code, product.quantity);
        var price = ("selling_price" in product) ? product.selling_price: product.price;
        currentRoomBreakUp['scopes']["modular"]['skuPrice'].set(product.sku_code, price);
        currentRoomBreakUp['scopes']["modular"]["children"].set(product.sku_code, []);
        product.group_skus.forEach(function(childSku){
          currentRoomBreakUp['skus'].push(childSku.sku_code);
          currentRoomBreakUp['scopes']["modular"]["children"].get(product.sku_code).push(childSku);
          var childPrice = ("selling_price" in childSku) ? childSku.selling_price: childSku.price;
          currentRoomBreakUp['scopes']["modular"]["childSkuPrices"].set(childSku.sku_code, childPrice);
        });
      }
      else if(product.vendor_type === 'CANVAS_VENDOR'){
        currentRoomBreakUp['scopes']["civil"]["total"]  = currentRoomBreakUp['scopes']["civil"]["total"] + productPrice;
        currentRoomBreakUp['scopes']["civil"]['skus'].push(product.sku_code);
        currentRoomBreakUp['scopes']["civil"]['skuQuantity'].set(product.sku_code, product.quantity);
        currentRoomBreakUp['scopes']["civil"]['skuPrice'].set(product.sku_code, product.selling_price);
      }
      else{
        currentRoomBreakUp['scopes']["others"]["total"]  = currentRoomBreakUp['scopes']["others"]["total"] + productPrice;
        currentRoomBreakUp['scopes']["others"]['skus'].push(product.sku_code);
        currentRoomBreakUp['scopes']["others"]['skuQuantity'].set(product.sku_code, product.quantity);
        currentRoomBreakUp['scopes']["others"]['skuPrice'].set(product.sku_code, product.selling_price);
      }
    });
    return roomScopeBreakOutDetails;
  }

  getRoomsDetails(){
    var roomsDetails = this.boqDetails.rooms
    var validRooms = 0;
    var roomScopeBreakOutDetails = this.getRoomScopeBreakOutDetails();
    this.boqDetails.rooms.forEach(function(room){
      if(roomScopeBreakOutDetails.has(room.id)){
        room["scopes"] = roomScopeBreakOutDetails.get(room.id)["scopes"]
        room["total"] = roomScopeBreakOutDetails.get(room.id)["total"]
      }
      room["images"] = []
      room.attachments.forEach(function(attachment){
        if(!attachment.is_cover_image){
          room["images"].push(attachment.url)
        }
        else{
          room["images"].unshift(attachment.url)
        }
      });
      if(room.total)
        validRooms = validRooms + 1;
    });
    roomsDetails["validRooms"] = validRooms;
    return roomsDetails
  }

  getRoomDetails(roomId, isLoaded){
    var roomDetails = null
    var roomScopeBreakOutDetails = this.getRoomScopeBreakOutDetails();
    this.boqDetails.rooms.forEach(function(room){
      if(room.id == roomId){
        if(roomScopeBreakOutDetails.has(room.id)){
          room["scopes"] = roomScopeBreakOutDetails.get(room.id)["scopes"]
          room["skus"] = roomScopeBreakOutDetails.get(room.id)["skus"]
          room["skuQuantity"] = roomScopeBreakOutDetails.get(room.id)["skuQuantity"]
          room["items"] = new Map()
        }
        room["images"] = []
        room.attachments.forEach(function(attachment){
          room["images"].push(attachment.url)
        });
        roomDetails = room;
      }
    });
    if(roomDetails.skus.length>0){
      this.dataClient.getCMSDetails(roomDetails.skus).then(data => {
        data.data.forEach(function(skuDetail){
          roomDetails.items.set(skuDetail.code, skuDetail)
        });
        isLoaded.value = true
      }).catch(error => {
        console.log(error)
      })
    }
    return roomDetails
  }

  getTnCContent(isLoaded){
    var slug = "boq-tnc-"+ this.countryISOCode.toLowerCase() +"?include=content.blocks"
    var self = this;
    this.dataClient.getNovaContent(slug).then(data => {
      console.log(data);
      isLoaded.value = true;
      self.tncContent = data.data;
    });
  }



}
