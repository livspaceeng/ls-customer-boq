(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["livspace-customer-boq"] = factory();
	else
		root["livspace-customer-boq"] = factory();
})((typeof self !== 'undefined' ? self : this), function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "fae3");
/******/ })
/************************************************************************/
/******/ ({

/***/ "07e3":
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "0fc9":
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__("3a38");
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),

/***/ "13c8":
/***/ (function(module, exports, __webpack_require__) {

var getKeys = __webpack_require__("c3a1");
var toIObject = __webpack_require__("36c3");
var isEnum = __webpack_require__("355d").f;
module.exports = function (isEntries) {
  return function (it) {
    var O = toIObject(it);
    var keys = getKeys(O);
    var length = keys.length;
    var i = 0;
    var result = [];
    var key;
    while (length > i) if (isEnum.call(O, key = keys[i++])) {
      result.push(isEntries ? [key, O[key]] : O[key]);
    } return result;
  };
};


/***/ }),

/***/ "1654":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $at = __webpack_require__("71c1")(true);

// 21.1.3.27 String.prototype[@@iterator]()
__webpack_require__("30f1")(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});


/***/ }),

/***/ "1691":
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),

/***/ "1727":
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__("469f"), __esModule: true };

/***/ }),

/***/ "199b":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("a712");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("2fb2").default
var update = add("6b1cab24", content, true, {});

/***/ }),

/***/ "1bc3":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__("f772");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "1ec9":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("f772");
var document = __webpack_require__("e53d").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "2350":
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ "241e":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__("25eb");
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),

/***/ "25eb":
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),

/***/ "294c":
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "2f2d":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, ".payment-info{background-color:#fff;border-radius:5px;-webkit-box-shadow:rgba(0,0,0,.2) 0 1px 3px 0,rgba(0,0,0,.14) 0 1px 1px 0,rgba(0,0,0,.12) 0 2px 1px -1px;box-shadow:0 1px 3px 0 rgba(0,0,0,.2),0 1px 1px 0 rgba(0,0,0,.14),0 2px 1px -1px rgba(0,0,0,.12);padding:5px}.tab-content{height:calc(100vh - 58px);overflow-y:scroll}.payment-info .paymetn-item{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;padding:35px 20px;border-bottom:1px solid #eaeaea}.payment-info .paymetn-item:last-child{border-bottom:0}.payment-info .paymetn-item p{margin:0;font-size:16px}.payment-info .paymetn-item p:last-child{min-width:100px;text-align:right}.tab-body-wrap{background-color:#f2f4f5;padding:30px 11%}.tab-body-title{color:#000;font-weight:500;font-size:20px}.nav.nav-tabs{position:relative;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;outline:none}.emi-banner{background:url(https://images.livmatrix.com/img/customer_boq/emi_banner_desktop.png);background-repeat:no-repeat;background-size:contain;width:100%;height:250px;display:inline-block;background-color:#f2f4f5}.edge-banner{margin-top:-7px;display:inline-block}.desktop-banner{display:block;width:100%}.mobile-banner{display:none;width:100%}@media(max-width:500px){.tab-body-wrap{padding:15px!important}}.nav-tabs .nav-link.active{color:#f74861;border-bottom:3px solid;border-color:transparent transparent #f74861}.nav-tabs .nav-link{margin:20px 30px 0;padding:0 0 20px;color:#707171;font-weight:700}.nav-tabs .nav-link:not(.active):hover{border-color:transparent}.nav-tabs .nav-item{margin-left:5%;margin-right:5%}.ls-loader-circle{display:inline-block;position:relative;width:inherit;height:inherit}.ls-loader-circle div{-webkit-box-sizing:border-box;box-sizing:border-box;display:block;position:absolute;top:calc(50% - 50px);left:calc(50% - 38px);-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);width:50px;height:50px;margin:6px;border:6px solid #f74861;border-radius:50%;-webkit-animation:ls-loader-circle 1.2s cubic-bezier(.5,0,.5,1) infinite;animation:ls-loader-circle 1.2s cubic-bezier(.5,0,.5,1) infinite;border-color:#f74861 transparent transparent}.ls-loader-circle div:first-child{-webkit-animation-delay:-.45s;animation-delay:-.45s}.ls-loader-circle div:nth-child(2){-webkit-animation-delay:-.3s;animation-delay:-.3s}.ls-loader-circle div:nth-child(3){-webkit-animation-delay:-.15s;animation-delay:-.15s}@-webkit-keyframes ls-loader-circle{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}@keyframes ls-loader-circle{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}@media (max-width:800px){.l-row{display:block!important}.l-col{width:100%!important;margin:0 0 50px!important}}.l-row{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;margin-top:50px;margin-bottom:50px}.l-col{width:50%}.l-col:first-child{margin-right:15px}.l-col:last-child{margin-left:15px}@media (min-width:500px){.tab-body-wrap{margin-top:70px}.nav.nav-tabs{position:fixed;left:0;right:0;background:#fff;top:0;z-index:11111;-webkit-box-shadow:0 1px 12px #cfcfcf;box-shadow:0 1px 12px #cfcfcf}.cost-breakout .nav.nav-tabs{position:relative!important;-webkit-box-shadow:none!important;box-shadow:none!important}}@media(max-width:500px){.nav.nav-tabs{-ms-flex-pack:distribute;justify-content:space-around;padding-top:20px;position:fixed;left:0;right:0;background:#fff;bottom:0;z-index:11111;-webkit-box-shadow:0 -1px 12px #cfcfcf;box-shadow:0 -1px 12px #cfcfcf}.nav.nav-tabs .nav-item,.nav.nav-tabs .nav-item .nav-link{margin:0!important}.cost-breakout .nav.nav-tabs{position:relative!important;-webkit-box-shadow:none!important;box-shadow:none!important}.emi-banner{background:url(https://images.livmatrix.com/img/customer_boq/emi_banner_mobile.png);background-repeat:no-repeat;background-size:cover;height:250px}.edge-banner,.emi-banner{width:100%;display:inline-block}.edge-banner{height:100vh}.desktop-banner{display:none;width:100%}.mobile-banner{display:block;width:100%;margin-bottom:20px}}", ""]);

// exports


/***/ }),

/***/ "2fb2":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/node_modules/vue-style-loader/lib/listToStyles.js
/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}

// CONCATENATED MODULE: ./node_modules/vue-loader/node_modules/vue-style-loader/lib/addStylesClient.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return addStylesClient; });
/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/



var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

function addStylesClient (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ "30f1":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__("b8e3");
var $export = __webpack_require__("63b6");
var redefine = __webpack_require__("9138");
var hide = __webpack_require__("35e8");
var Iterators = __webpack_require__("481b");
var $iterCreate = __webpack_require__("8f60");
var setToStringTag = __webpack_require__("45f2");
var getPrototypeOf = __webpack_require__("53e2");
var ITERATOR = __webpack_require__("5168")('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),

/***/ "32fc":
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__("e53d").document;
module.exports = document && document.documentElement;


/***/ }),

/***/ "335c":
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__("6b4c");
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),

/***/ "355d":
/***/ (function(module, exports) {

exports.f = {}.propertyIsEnumerable;


/***/ }),

/***/ "35e8":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("d9f6");
var createDesc = __webpack_require__("aebd");
module.exports = __webpack_require__("8e60") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "36c3":
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__("335c");
var defined = __webpack_require__("25eb");
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),

/***/ "3a38":
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),

/***/ "40c3":
/***/ (function(module, exports, __webpack_require__) {

// getting tag from 19.1.3.6 Object.prototype.toString()
var cof = __webpack_require__("6b4c");
var TAG = __webpack_require__("5168")('toStringTag');
// ES3 wrong here
var ARG = cof(function () { return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (e) { /* empty */ }
};

module.exports = function (it) {
  var O, T, B;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T
    // builtinTag case
    : ARG ? cof(O)
    // ES3 arguments fallback
    : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
};


/***/ }),

/***/ "42d1":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/***/ }),

/***/ "45f2":
/***/ (function(module, exports, __webpack_require__) {

var def = __webpack_require__("d9f6").f;
var has = __webpack_require__("07e3");
var TAG = __webpack_require__("5168")('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),

/***/ "469f":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("6c1c");
__webpack_require__("1654");
module.exports = __webpack_require__("7d7b");


/***/ }),

/***/ "481b":
/***/ (function(module, exports) {

module.exports = {};


/***/ }),

/***/ "4f25":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("f4be");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("2fb2").default
var update = add("44a72224", content, true, {});

/***/ }),

/***/ "50ed":
/***/ (function(module, exports) {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),

/***/ "5168":
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__("dbdb")('wks');
var uid = __webpack_require__("62a0");
var Symbol = __webpack_require__("e53d").Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),

/***/ "53e2":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__("07e3");
var toObject = __webpack_require__("241e");
var IE_PROTO = __webpack_require__("5559")('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),

/***/ "54a1":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("6c1c");
__webpack_require__("1654");
module.exports = __webpack_require__("95d5");


/***/ }),

/***/ "5559":
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__("dbdb")('keys');
var uid = __webpack_require__("62a0");
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),

/***/ "584a":
/***/ (function(module, exports) {

var core = module.exports = { version: '2.5.7' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "5b4e":
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__("36c3");
var toLength = __webpack_require__("b447");
var toAbsoluteIndex = __webpack_require__("0fc9");
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),

/***/ "62a0":
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),

/***/ "62e4":
/***/ (function(module, exports) {

module.exports = function(module) {
	if (!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if (!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),

/***/ "63b6":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("e53d");
var core = __webpack_require__("584a");
var ctx = __webpack_require__("d864");
var hide = __webpack_require__("35e8");
var has = __webpack_require__("07e3");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && has(exports, key)) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "6605":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("df74");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("2fb2").default
var update = add("07029324", content, true, {});

/***/ }),

/***/ "699e":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("b7e1");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("2fb2").default
var update = add("551606f4", content, true, {});

/***/ }),

/***/ "6b4c":
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),

/***/ "6c1c":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("c367");
var global = __webpack_require__("e53d");
var hide = __webpack_require__("35e8");
var Iterators = __webpack_require__("481b");
var TO_STRING_TAG = __webpack_require__("5168")('toStringTag');

var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +
  'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +
  'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +
  'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +
  'TextTrackList,TouchList').split(',');

for (var i = 0; i < DOMIterables.length; i++) {
  var NAME = DOMIterables[i];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
  Iterators[NAME] = Iterators.Array;
}


/***/ }),

/***/ "71c1":
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__("3a38");
var defined = __webpack_require__("25eb");
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};


/***/ }),

/***/ "794b":
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__("8e60") && !__webpack_require__("294c")(function () {
  return Object.defineProperty(__webpack_require__("1ec9")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "79aa":
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "7cd6":
/***/ (function(module, exports, __webpack_require__) {

var classof = __webpack_require__("40c3");
var ITERATOR = __webpack_require__("5168")('iterator');
var Iterators = __webpack_require__("481b");
module.exports = __webpack_require__("584a").getIteratorMethod = function (it) {
  if (it != undefined) return it[ITERATOR]
    || it['@@iterator']
    || Iterators[classof(it)];
};


/***/ }),

/***/ "7d7b":
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__("e4ae");
var get = __webpack_require__("7cd6");
module.exports = __webpack_require__("584a").getIterator = function (it) {
  var iterFn = get(it);
  if (typeof iterFn != 'function') throw TypeError(it + ' is not iterable!');
  return anObject(iterFn.call(it));
};


/***/ }),

/***/ "7e6a":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("42d1");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("2fb2").default
var update = add("120a1e4a", content, true, {});

/***/ }),

/***/ "7e90":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("d9f6");
var anObject = __webpack_require__("e4ae");
var getKeys = __webpack_require__("c3a1");

module.exports = __webpack_require__("8e60") ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),

/***/ "8436":
/***/ (function(module, exports) {

module.exports = function () { /* empty */ };


/***/ }),

/***/ "8e60":
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__("294c")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "8f60":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var create = __webpack_require__("a159");
var descriptor = __webpack_require__("aebd");
var setToStringTag = __webpack_require__("45f2");
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__("35e8")(IteratorPrototype, __webpack_require__("5168")('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),

/***/ "9138":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("35e8");


/***/ }),

/***/ "93ff":
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__("54a1"), __esModule: true };

/***/ }),

/***/ "95d5":
/***/ (function(module, exports, __webpack_require__) {

var classof = __webpack_require__("40c3");
var ITERATOR = __webpack_require__("5168")('iterator');
var Iterators = __webpack_require__("481b");
module.exports = __webpack_require__("584a").isIterable = function (it) {
  var O = Object(it);
  return O[ITERATOR] !== undefined
    || '@@iterator' in O
    // eslint-disable-next-line no-prototype-builtins
    || Iterators.hasOwnProperty(classof(O));
};


/***/ }),

/***/ "9c60":
/***/ (function(module, exports, __webpack_require__) {

// https://github.com/tc39/proposal-object-values-entries
var $export = __webpack_require__("63b6");
var $entries = __webpack_require__("13c8")(true);

$export($export.S, 'Object', {
  entries: function entries(it) {
    return $entries(it);
  }
});


/***/ }),

/***/ "9f91":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, ".thumbnails{display:-webkit-box;display:-ms-flexbox;display:flex;margin-top:20px}.thumbnail-image{width:100px;height:60px;cursor:pointer;border:2px solid transparent;margin-right:20px;padding:5px}.thumbnail-image.active{border-width:2px;border-color:#f75c72;border-radius:5px}.thumbnail-image img{width:100%;height:100%}.img-responsive{width:100%;height:auto}.next{right:20px}.next,.prev{position:absolute;top:0;bottom:0;margin:auto;width:20px;height:25px;cursor:pointer}.prev{left:20px;-webkit-transform:rotateY(180deg);transform:rotateY(180deg)}.card-img{position:relative;border-radius:5px!important;overflow:hidden;width:100%;min-height:150px;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center}", ""]);

// exports


/***/ }),

/***/ "a022":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("adc4");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("2fb2").default
var update = add("d212d3a4", content, true, {});

/***/ }),

/***/ "a05d":
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__("b606"), __esModule: true };

/***/ }),

/***/ "a159":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__("e4ae");
var dPs = __webpack_require__("7e90");
var enumBugKeys = __webpack_require__("1691");
var IE_PROTO = __webpack_require__("5559")('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__("1ec9")('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__("32fc").appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),

/***/ "a58e":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("c602");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("2fb2").default
var update = add("3dae2dae", content, true, {});

/***/ }),

/***/ "a712":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, ".summary-card{margin-bottom:50px}.summary-card .card{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;background-color:#fff;padding:30px 20px;border-radius:5px;-webkit-box-shadow:rgba(0,0,0,.2) 0 1px 3px 0,rgba(0,0,0,.14) 0 1px 1px 0,rgba(0,0,0,.12) 0 2px 1px -1px;box-shadow:0 1px 3px 0 rgba(0,0,0,.2),0 1px 1px 0 rgba(0,0,0,.14),0 2px 1px -1px rgba(0,0,0,.12);position:relative}@media (min-width:500px){.summary-card .card .learn-more{position:absolute;bottom:-30px;right:10px}}.summary-card .card .learn-more{text-decoration:none;font-size:12px;cursor:pointer}.summary-card .card .learn-more:hover,.summary-card .card .learn-more:link,.summary-card .card .learn-more:visited{color:#d31e2d}@media (max-width:500px){.summary-card .card{display:block}.summary-card .card-item{margin-bottom:20px}}.summary-card .card-item-last{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row;-webkit-box-pack:space-evenly;-ms-flex-pack:space-evenly;justify-content:space-evenly}.summary-card .card-item-last,.summary-card .card-item-last .card-item{-ms-flex-preferred-size:50%;flex-basis:50%}.summary-card .card-item-last .card-item:last-child{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center}.summary-card .card-item-last .card-item .days-in-circle{width:40px;height:40px;border:2px solid red;border-radius:50%;margin-right:10px;text-align:center;line-height:40px;margin-bottom:0}.summary-card .card-item{-ms-flex-preferred-size:25%;flex-basis:25%}.summary-card .card-item .title{color:#676767;display:inline-block;font-size:12px;margin-bottom:10px;letter-spacing:1px}.summary-card .card-item .text{color:#000;font-size:.8rem;margin:0;letter-spacing:1px}", ""]);

// exports


/***/ }),

/***/ "adc4":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "section[data-v-2c9777bc]{background-color:#fff!important;margin-bottom:15px}h2[data-v-2c9777bc]{padding-top:10px}", ""]);

// exports


/***/ }),

/***/ "aebd":
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "b1d6":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "#product-detials{position:fixed;background-color:#fff;left:0;right:0;top:0;bottom:0;z-index:100000;font-size:18px;overflow-y:scroll;display:none}@media (max-width:500px){.product-detials-wrap{width:100%;height:100%}}@media (min-width:500px){#product-detials{background-color:rgba(0,0,0,.9)!important;left:30%;top:0;bottom:0;position:fixed;z-index:100000000}.product-detials-wrap{background-color:#fff;position:absolute;width:100%;height:100%;overflow:scroll}.product-detials-wrap .details .product-image{height:50vh;text-align:center;margin-bottom:50px}.product-detials-wrap .details .product-image img{height:inherit}.product-detials-wrap .details .product-name{font-weight:700}}.header{padding:15px}.header-text{padding-left:15px}.details .product-name{font-size:.8em;padding:15px;color:#000;margin:0}.details .product-image{padding:15px}.details .product-image img{max-width:100%;height:auto;margin:auto}.details-item{padding:10px 15px;border-bottom:1px solid #eaeaea}.openProductDetials{display:-webkit-box!important;display:-ms-flexbox!important;display:flex!important;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center}.details-item .heading{font-size:.8em;color:#000;margin-bottom:5px}.details-item .text{font-size:.7em;color:#8e8e8e;margin-bottom:5px}.product-information{padding:15px}.product-information .heading{font-size:.8em;margin-bottom:15px}.product-information .info-item{font-size:.7em;color:#4c4c4c;margin-bottom:8px;-ms-flex-preferred-size:50%;flex-basis:50%}.product-information .info-item,.product-information .info-item .item-image{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center}.product-information .info-item .item-image img{margin-right:15px}.product-information .info-item span{width:50%;display:inline-block}.product-information .info-item .item-type{color:#000;font-weight:700}", ""]);

// exports


/***/ }),

/***/ "b24f":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _isIterable2 = __webpack_require__("93ff");

var _isIterable3 = _interopRequireDefault(_isIterable2);

var _getIterator2 = __webpack_require__("1727");

var _getIterator3 = _interopRequireDefault(_getIterator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function sliceIterator(arr, i) {
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = (0, _getIterator3.default)(arr), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"]) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  return function (arr, i) {
    if (Array.isArray(arr)) {
      return arr;
    } else if ((0, _isIterable3.default)(Object(arr))) {
      return sliceIterator(arr, i);
    } else {
      throw new TypeError("Invalid attempt to destructure non-iterable instance");
    }
  };
}();

/***/ }),

/***/ "b311":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	var e = new Error("Cannot find module '" + req + "'");
	e.code = 'MODULE_NOT_FOUND';
	throw e;
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "b311";

/***/ }),

/***/ "b447":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__("3a38");
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),

/***/ "b606":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("9c60");
module.exports = __webpack_require__("584a").Object.entries;


/***/ }),

/***/ "b6c3":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("b852");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("2fb2").default
var update = add("afe40c80", content, true, {});

/***/ }),

/***/ "b7e1":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, ".thumbnails{display:-webkit-box;display:-ms-flexbox;display:flex;margin-top:20px}.thumbnail-image{width:100px;height:60px;cursor:pointer;border:2px solid transparent;margin-right:20px;padding:5px}.thumbnail-image.active{border-width:2px;border-color:#f75c72;border-radius:5px}.thumbnail-image img{width:100%;height:100%}.img-responsive{width:100%;height:auto}.next{right:20px}.next,.prev{position:absolute;top:0;bottom:0;margin:auto;width:20px;height:25px;cursor:pointer}.prev{left:20px;-webkit-transform:rotateY(180deg);transform:rotateY(180deg)}.card-img{position:relative;border-radius:5px!important;overflow:hidden;width:100%;min-height:150px;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center}", ""]);

// exports


/***/ }),

/***/ "b7fc":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("b1d6");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("2fb2").default
var update = add("5e233e9e", content, true, {});

/***/ }),

/***/ "b852":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, ".listitem_23E2iSpc{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;padding:15px;padding-right:10px;border-bottom:1px solid #e9e9e9}.listitem_23E2iSpc .colorlegend_1e44FdpK{width:16px;height:16px;border-radius:4px;margin-right:14px}.listitem_23E2iSpc.compact_pztR9vbZ{padding:5px;padding-left:15px;border-bottom:1px solid transparent}.listitem_23E2iSpc.compact_pztR9vbZ .colorlegend_1e44FdpK{margin-right:7px}.propertyname_tZSKT4cD{text-align:left}.price_2FjieMZM,.propertyname_tZSKT4cD{font-family:Roboto,sans-serif;font-size:14px;font-weight:400}.price_2FjieMZM{min-width:90px;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end;margin-right:-4px;-webkit-box-align:center;-ms-flex-align:center;align-items:center}.listitem_23E2iSpc.compact_pztR9vbZ .price_2FjieMZM{padding-right:13px}.chevronstyle_2U1Ud1m0{width:20px;height:9px;background-position-x:0;background-position-y:-7px;background-size:100%;background-size:20px}", ""]);

// exports
exports.locals = {
	"listitem": "listitem_23E2iSpc",
	"colorlegend": "colorlegend_1e44FdpK",
	"compact": "compact_pztR9vbZ",
	"propertyname": "propertyname_tZSKT4cD",
	"price": "price_2FjieMZM",
	"chevronstyle": "chevronstyle_2U1Ud1m0"
};

/***/ }),

/***/ "b8e3":
/***/ (function(module, exports) {

module.exports = true;


/***/ }),

/***/ "c1df":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {var require;//! moment.js

;(function (global, factory) {
     true ? module.exports = factory() :
    undefined
}(this, (function () { 'use strict';

    var hookCallback;

    function hooks () {
        return hookCallback.apply(null, arguments);
    }

    // This is done to register the method called with moment()
    // without creating circular dependencies.
    function setHookCallback (callback) {
        hookCallback = callback;
    }

    function isArray(input) {
        return input instanceof Array || Object.prototype.toString.call(input) === '[object Array]';
    }

    function isObject(input) {
        // IE8 will treat undefined and null as object if it wasn't for
        // input != null
        return input != null && Object.prototype.toString.call(input) === '[object Object]';
    }

    function isObjectEmpty(obj) {
        if (Object.getOwnPropertyNames) {
            return (Object.getOwnPropertyNames(obj).length === 0);
        } else {
            var k;
            for (k in obj) {
                if (obj.hasOwnProperty(k)) {
                    return false;
                }
            }
            return true;
        }
    }

    function isUndefined(input) {
        return input === void 0;
    }

    function isNumber(input) {
        return typeof input === 'number' || Object.prototype.toString.call(input) === '[object Number]';
    }

    function isDate(input) {
        return input instanceof Date || Object.prototype.toString.call(input) === '[object Date]';
    }

    function map(arr, fn) {
        var res = [], i;
        for (i = 0; i < arr.length; ++i) {
            res.push(fn(arr[i], i));
        }
        return res;
    }

    function hasOwnProp(a, b) {
        return Object.prototype.hasOwnProperty.call(a, b);
    }

    function extend(a, b) {
        for (var i in b) {
            if (hasOwnProp(b, i)) {
                a[i] = b[i];
            }
        }

        if (hasOwnProp(b, 'toString')) {
            a.toString = b.toString;
        }

        if (hasOwnProp(b, 'valueOf')) {
            a.valueOf = b.valueOf;
        }

        return a;
    }

    function createUTC (input, format, locale, strict) {
        return createLocalOrUTC(input, format, locale, strict, true).utc();
    }

    function defaultParsingFlags() {
        // We need to deep clone this object.
        return {
            empty           : false,
            unusedTokens    : [],
            unusedInput     : [],
            overflow        : -2,
            charsLeftOver   : 0,
            nullInput       : false,
            invalidMonth    : null,
            invalidFormat   : false,
            userInvalidated : false,
            iso             : false,
            parsedDateParts : [],
            meridiem        : null,
            rfc2822         : false,
            weekdayMismatch : false
        };
    }

    function getParsingFlags(m) {
        if (m._pf == null) {
            m._pf = defaultParsingFlags();
        }
        return m._pf;
    }

    var some;
    if (Array.prototype.some) {
        some = Array.prototype.some;
    } else {
        some = function (fun) {
            var t = Object(this);
            var len = t.length >>> 0;

            for (var i = 0; i < len; i++) {
                if (i in t && fun.call(this, t[i], i, t)) {
                    return true;
                }
            }

            return false;
        };
    }

    function isValid(m) {
        if (m._isValid == null) {
            var flags = getParsingFlags(m);
            var parsedParts = some.call(flags.parsedDateParts, function (i) {
                return i != null;
            });
            var isNowValid = !isNaN(m._d.getTime()) &&
                flags.overflow < 0 &&
                !flags.empty &&
                !flags.invalidMonth &&
                !flags.invalidWeekday &&
                !flags.weekdayMismatch &&
                !flags.nullInput &&
                !flags.invalidFormat &&
                !flags.userInvalidated &&
                (!flags.meridiem || (flags.meridiem && parsedParts));

            if (m._strict) {
                isNowValid = isNowValid &&
                    flags.charsLeftOver === 0 &&
                    flags.unusedTokens.length === 0 &&
                    flags.bigHour === undefined;
            }

            if (Object.isFrozen == null || !Object.isFrozen(m)) {
                m._isValid = isNowValid;
            }
            else {
                return isNowValid;
            }
        }
        return m._isValid;
    }

    function createInvalid (flags) {
        var m = createUTC(NaN);
        if (flags != null) {
            extend(getParsingFlags(m), flags);
        }
        else {
            getParsingFlags(m).userInvalidated = true;
        }

        return m;
    }

    // Plugins that add properties should also add the key here (null value),
    // so we can properly clone ourselves.
    var momentProperties = hooks.momentProperties = [];

    function copyConfig(to, from) {
        var i, prop, val;

        if (!isUndefined(from._isAMomentObject)) {
            to._isAMomentObject = from._isAMomentObject;
        }
        if (!isUndefined(from._i)) {
            to._i = from._i;
        }
        if (!isUndefined(from._f)) {
            to._f = from._f;
        }
        if (!isUndefined(from._l)) {
            to._l = from._l;
        }
        if (!isUndefined(from._strict)) {
            to._strict = from._strict;
        }
        if (!isUndefined(from._tzm)) {
            to._tzm = from._tzm;
        }
        if (!isUndefined(from._isUTC)) {
            to._isUTC = from._isUTC;
        }
        if (!isUndefined(from._offset)) {
            to._offset = from._offset;
        }
        if (!isUndefined(from._pf)) {
            to._pf = getParsingFlags(from);
        }
        if (!isUndefined(from._locale)) {
            to._locale = from._locale;
        }

        if (momentProperties.length > 0) {
            for (i = 0; i < momentProperties.length; i++) {
                prop = momentProperties[i];
                val = from[prop];
                if (!isUndefined(val)) {
                    to[prop] = val;
                }
            }
        }

        return to;
    }

    var updateInProgress = false;

    // Moment prototype object
    function Moment(config) {
        copyConfig(this, config);
        this._d = new Date(config._d != null ? config._d.getTime() : NaN);
        if (!this.isValid()) {
            this._d = new Date(NaN);
        }
        // Prevent infinite loop in case updateOffset creates new moment
        // objects.
        if (updateInProgress === false) {
            updateInProgress = true;
            hooks.updateOffset(this);
            updateInProgress = false;
        }
    }

    function isMoment (obj) {
        return obj instanceof Moment || (obj != null && obj._isAMomentObject != null);
    }

    function absFloor (number) {
        if (number < 0) {
            // -0 -> 0
            return Math.ceil(number) || 0;
        } else {
            return Math.floor(number);
        }
    }

    function toInt(argumentForCoercion) {
        var coercedNumber = +argumentForCoercion,
            value = 0;

        if (coercedNumber !== 0 && isFinite(coercedNumber)) {
            value = absFloor(coercedNumber);
        }

        return value;
    }

    // compare two arrays, return the number of differences
    function compareArrays(array1, array2, dontConvert) {
        var len = Math.min(array1.length, array2.length),
            lengthDiff = Math.abs(array1.length - array2.length),
            diffs = 0,
            i;
        for (i = 0; i < len; i++) {
            if ((dontConvert && array1[i] !== array2[i]) ||
                (!dontConvert && toInt(array1[i]) !== toInt(array2[i]))) {
                diffs++;
            }
        }
        return diffs + lengthDiff;
    }

    function warn(msg) {
        if (hooks.suppressDeprecationWarnings === false &&
                (typeof console !==  'undefined') && console.warn) {
            console.warn('Deprecation warning: ' + msg);
        }
    }

    function deprecate(msg, fn) {
        var firstTime = true;

        return extend(function () {
            if (hooks.deprecationHandler != null) {
                hooks.deprecationHandler(null, msg);
            }
            if (firstTime) {
                var args = [];
                var arg;
                for (var i = 0; i < arguments.length; i++) {
                    arg = '';
                    if (typeof arguments[i] === 'object') {
                        arg += '\n[' + i + '] ';
                        for (var key in arguments[0]) {
                            arg += key + ': ' + arguments[0][key] + ', ';
                        }
                        arg = arg.slice(0, -2); // Remove trailing comma and space
                    } else {
                        arg = arguments[i];
                    }
                    args.push(arg);
                }
                warn(msg + '\nArguments: ' + Array.prototype.slice.call(args).join('') + '\n' + (new Error()).stack);
                firstTime = false;
            }
            return fn.apply(this, arguments);
        }, fn);
    }

    var deprecations = {};

    function deprecateSimple(name, msg) {
        if (hooks.deprecationHandler != null) {
            hooks.deprecationHandler(name, msg);
        }
        if (!deprecations[name]) {
            warn(msg);
            deprecations[name] = true;
        }
    }

    hooks.suppressDeprecationWarnings = false;
    hooks.deprecationHandler = null;

    function isFunction(input) {
        return input instanceof Function || Object.prototype.toString.call(input) === '[object Function]';
    }

    function set (config) {
        var prop, i;
        for (i in config) {
            prop = config[i];
            if (isFunction(prop)) {
                this[i] = prop;
            } else {
                this['_' + i] = prop;
            }
        }
        this._config = config;
        // Lenient ordinal parsing accepts just a number in addition to
        // number + (possibly) stuff coming from _dayOfMonthOrdinalParse.
        // TODO: Remove "ordinalParse" fallback in next major release.
        this._dayOfMonthOrdinalParseLenient = new RegExp(
            (this._dayOfMonthOrdinalParse.source || this._ordinalParse.source) +
                '|' + (/\d{1,2}/).source);
    }

    function mergeConfigs(parentConfig, childConfig) {
        var res = extend({}, parentConfig), prop;
        for (prop in childConfig) {
            if (hasOwnProp(childConfig, prop)) {
                if (isObject(parentConfig[prop]) && isObject(childConfig[prop])) {
                    res[prop] = {};
                    extend(res[prop], parentConfig[prop]);
                    extend(res[prop], childConfig[prop]);
                } else if (childConfig[prop] != null) {
                    res[prop] = childConfig[prop];
                } else {
                    delete res[prop];
                }
            }
        }
        for (prop in parentConfig) {
            if (hasOwnProp(parentConfig, prop) &&
                    !hasOwnProp(childConfig, prop) &&
                    isObject(parentConfig[prop])) {
                // make sure changes to properties don't modify parent config
                res[prop] = extend({}, res[prop]);
            }
        }
        return res;
    }

    function Locale(config) {
        if (config != null) {
            this.set(config);
        }
    }

    var keys;

    if (Object.keys) {
        keys = Object.keys;
    } else {
        keys = function (obj) {
            var i, res = [];
            for (i in obj) {
                if (hasOwnProp(obj, i)) {
                    res.push(i);
                }
            }
            return res;
        };
    }

    var defaultCalendar = {
        sameDay : '[Today at] LT',
        nextDay : '[Tomorrow at] LT',
        nextWeek : 'dddd [at] LT',
        lastDay : '[Yesterday at] LT',
        lastWeek : '[Last] dddd [at] LT',
        sameElse : 'L'
    };

    function calendar (key, mom, now) {
        var output = this._calendar[key] || this._calendar['sameElse'];
        return isFunction(output) ? output.call(mom, now) : output;
    }

    var defaultLongDateFormat = {
        LTS  : 'h:mm:ss A',
        LT   : 'h:mm A',
        L    : 'MM/DD/YYYY',
        LL   : 'MMMM D, YYYY',
        LLL  : 'MMMM D, YYYY h:mm A',
        LLLL : 'dddd, MMMM D, YYYY h:mm A'
    };

    function longDateFormat (key) {
        var format = this._longDateFormat[key],
            formatUpper = this._longDateFormat[key.toUpperCase()];

        if (format || !formatUpper) {
            return format;
        }

        this._longDateFormat[key] = formatUpper.replace(/MMMM|MM|DD|dddd/g, function (val) {
            return val.slice(1);
        });

        return this._longDateFormat[key];
    }

    var defaultInvalidDate = 'Invalid date';

    function invalidDate () {
        return this._invalidDate;
    }

    var defaultOrdinal = '%d';
    var defaultDayOfMonthOrdinalParse = /\d{1,2}/;

    function ordinal (number) {
        return this._ordinal.replace('%d', number);
    }

    var defaultRelativeTime = {
        future : 'in %s',
        past   : '%s ago',
        s  : 'a few seconds',
        ss : '%d seconds',
        m  : 'a minute',
        mm : '%d minutes',
        h  : 'an hour',
        hh : '%d hours',
        d  : 'a day',
        dd : '%d days',
        M  : 'a month',
        MM : '%d months',
        y  : 'a year',
        yy : '%d years'
    };

    function relativeTime (number, withoutSuffix, string, isFuture) {
        var output = this._relativeTime[string];
        return (isFunction(output)) ?
            output(number, withoutSuffix, string, isFuture) :
            output.replace(/%d/i, number);
    }

    function pastFuture (diff, output) {
        var format = this._relativeTime[diff > 0 ? 'future' : 'past'];
        return isFunction(format) ? format(output) : format.replace(/%s/i, output);
    }

    var aliases = {};

    function addUnitAlias (unit, shorthand) {
        var lowerCase = unit.toLowerCase();
        aliases[lowerCase] = aliases[lowerCase + 's'] = aliases[shorthand] = unit;
    }

    function normalizeUnits(units) {
        return typeof units === 'string' ? aliases[units] || aliases[units.toLowerCase()] : undefined;
    }

    function normalizeObjectUnits(inputObject) {
        var normalizedInput = {},
            normalizedProp,
            prop;

        for (prop in inputObject) {
            if (hasOwnProp(inputObject, prop)) {
                normalizedProp = normalizeUnits(prop);
                if (normalizedProp) {
                    normalizedInput[normalizedProp] = inputObject[prop];
                }
            }
        }

        return normalizedInput;
    }

    var priorities = {};

    function addUnitPriority(unit, priority) {
        priorities[unit] = priority;
    }

    function getPrioritizedUnits(unitsObj) {
        var units = [];
        for (var u in unitsObj) {
            units.push({unit: u, priority: priorities[u]});
        }
        units.sort(function (a, b) {
            return a.priority - b.priority;
        });
        return units;
    }

    function zeroFill(number, targetLength, forceSign) {
        var absNumber = '' + Math.abs(number),
            zerosToFill = targetLength - absNumber.length,
            sign = number >= 0;
        return (sign ? (forceSign ? '+' : '') : '-') +
            Math.pow(10, Math.max(0, zerosToFill)).toString().substr(1) + absNumber;
    }

    var formattingTokens = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g;

    var localFormattingTokens = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g;

    var formatFunctions = {};

    var formatTokenFunctions = {};

    // token:    'M'
    // padded:   ['MM', 2]
    // ordinal:  'Mo'
    // callback: function () { this.month() + 1 }
    function addFormatToken (token, padded, ordinal, callback) {
        var func = callback;
        if (typeof callback === 'string') {
            func = function () {
                return this[callback]();
            };
        }
        if (token) {
            formatTokenFunctions[token] = func;
        }
        if (padded) {
            formatTokenFunctions[padded[0]] = function () {
                return zeroFill(func.apply(this, arguments), padded[1], padded[2]);
            };
        }
        if (ordinal) {
            formatTokenFunctions[ordinal] = function () {
                return this.localeData().ordinal(func.apply(this, arguments), token);
            };
        }
    }

    function removeFormattingTokens(input) {
        if (input.match(/\[[\s\S]/)) {
            return input.replace(/^\[|\]$/g, '');
        }
        return input.replace(/\\/g, '');
    }

    function makeFormatFunction(format) {
        var array = format.match(formattingTokens), i, length;

        for (i = 0, length = array.length; i < length; i++) {
            if (formatTokenFunctions[array[i]]) {
                array[i] = formatTokenFunctions[array[i]];
            } else {
                array[i] = removeFormattingTokens(array[i]);
            }
        }

        return function (mom) {
            var output = '', i;
            for (i = 0; i < length; i++) {
                output += isFunction(array[i]) ? array[i].call(mom, format) : array[i];
            }
            return output;
        };
    }

    // format date using native date object
    function formatMoment(m, format) {
        if (!m.isValid()) {
            return m.localeData().invalidDate();
        }

        format = expandFormat(format, m.localeData());
        formatFunctions[format] = formatFunctions[format] || makeFormatFunction(format);

        return formatFunctions[format](m);
    }

    function expandFormat(format, locale) {
        var i = 5;

        function replaceLongDateFormatTokens(input) {
            return locale.longDateFormat(input) || input;
        }

        localFormattingTokens.lastIndex = 0;
        while (i >= 0 && localFormattingTokens.test(format)) {
            format = format.replace(localFormattingTokens, replaceLongDateFormatTokens);
            localFormattingTokens.lastIndex = 0;
            i -= 1;
        }

        return format;
    }

    var match1         = /\d/;            //       0 - 9
    var match2         = /\d\d/;          //      00 - 99
    var match3         = /\d{3}/;         //     000 - 999
    var match4         = /\d{4}/;         //    0000 - 9999
    var match6         = /[+-]?\d{6}/;    // -999999 - 999999
    var match1to2      = /\d\d?/;         //       0 - 99
    var match3to4      = /\d\d\d\d?/;     //     999 - 9999
    var match5to6      = /\d\d\d\d\d\d?/; //   99999 - 999999
    var match1to3      = /\d{1,3}/;       //       0 - 999
    var match1to4      = /\d{1,4}/;       //       0 - 9999
    var match1to6      = /[+-]?\d{1,6}/;  // -999999 - 999999

    var matchUnsigned  = /\d+/;           //       0 - inf
    var matchSigned    = /[+-]?\d+/;      //    -inf - inf

    var matchOffset    = /Z|[+-]\d\d:?\d\d/gi; // +00:00 -00:00 +0000 -0000 or Z
    var matchShortOffset = /Z|[+-]\d\d(?::?\d\d)?/gi; // +00 -00 +00:00 -00:00 +0000 -0000 or Z

    var matchTimestamp = /[+-]?\d+(\.\d{1,3})?/; // 123456789 123456789.123

    // any word (or two) characters or numbers including two/three word month in arabic.
    // includes scottish gaelic two word and hyphenated months
    var matchWord = /[0-9]{0,256}['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFF07\uFF10-\uFFEF]{1,256}|[\u0600-\u06FF\/]{1,256}(\s*?[\u0600-\u06FF]{1,256}){1,2}/i;

    var regexes = {};

    function addRegexToken (token, regex, strictRegex) {
        regexes[token] = isFunction(regex) ? regex : function (isStrict, localeData) {
            return (isStrict && strictRegex) ? strictRegex : regex;
        };
    }

    function getParseRegexForToken (token, config) {
        if (!hasOwnProp(regexes, token)) {
            return new RegExp(unescapeFormat(token));
        }

        return regexes[token](config._strict, config._locale);
    }

    // Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript
    function unescapeFormat(s) {
        return regexEscape(s.replace('\\', '').replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (matched, p1, p2, p3, p4) {
            return p1 || p2 || p3 || p4;
        }));
    }

    function regexEscape(s) {
        return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    }

    var tokens = {};

    function addParseToken (token, callback) {
        var i, func = callback;
        if (typeof token === 'string') {
            token = [token];
        }
        if (isNumber(callback)) {
            func = function (input, array) {
                array[callback] = toInt(input);
            };
        }
        for (i = 0; i < token.length; i++) {
            tokens[token[i]] = func;
        }
    }

    function addWeekParseToken (token, callback) {
        addParseToken(token, function (input, array, config, token) {
            config._w = config._w || {};
            callback(input, config._w, config, token);
        });
    }

    function addTimeToArrayFromToken(token, input, config) {
        if (input != null && hasOwnProp(tokens, token)) {
            tokens[token](input, config._a, config, token);
        }
    }

    var YEAR = 0;
    var MONTH = 1;
    var DATE = 2;
    var HOUR = 3;
    var MINUTE = 4;
    var SECOND = 5;
    var MILLISECOND = 6;
    var WEEK = 7;
    var WEEKDAY = 8;

    // FORMATTING

    addFormatToken('Y', 0, 0, function () {
        var y = this.year();
        return y <= 9999 ? '' + y : '+' + y;
    });

    addFormatToken(0, ['YY', 2], 0, function () {
        return this.year() % 100;
    });

    addFormatToken(0, ['YYYY',   4],       0, 'year');
    addFormatToken(0, ['YYYYY',  5],       0, 'year');
    addFormatToken(0, ['YYYYYY', 6, true], 0, 'year');

    // ALIASES

    addUnitAlias('year', 'y');

    // PRIORITIES

    addUnitPriority('year', 1);

    // PARSING

    addRegexToken('Y',      matchSigned);
    addRegexToken('YY',     match1to2, match2);
    addRegexToken('YYYY',   match1to4, match4);
    addRegexToken('YYYYY',  match1to6, match6);
    addRegexToken('YYYYYY', match1to6, match6);

    addParseToken(['YYYYY', 'YYYYYY'], YEAR);
    addParseToken('YYYY', function (input, array) {
        array[YEAR] = input.length === 2 ? hooks.parseTwoDigitYear(input) : toInt(input);
    });
    addParseToken('YY', function (input, array) {
        array[YEAR] = hooks.parseTwoDigitYear(input);
    });
    addParseToken('Y', function (input, array) {
        array[YEAR] = parseInt(input, 10);
    });

    // HELPERS

    function daysInYear(year) {
        return isLeapYear(year) ? 366 : 365;
    }

    function isLeapYear(year) {
        return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
    }

    // HOOKS

    hooks.parseTwoDigitYear = function (input) {
        return toInt(input) + (toInt(input) > 68 ? 1900 : 2000);
    };

    // MOMENTS

    var getSetYear = makeGetSet('FullYear', true);

    function getIsLeapYear () {
        return isLeapYear(this.year());
    }

    function makeGetSet (unit, keepTime) {
        return function (value) {
            if (value != null) {
                set$1(this, unit, value);
                hooks.updateOffset(this, keepTime);
                return this;
            } else {
                return get(this, unit);
            }
        };
    }

    function get (mom, unit) {
        return mom.isValid() ?
            mom._d['get' + (mom._isUTC ? 'UTC' : '') + unit]() : NaN;
    }

    function set$1 (mom, unit, value) {
        if (mom.isValid() && !isNaN(value)) {
            if (unit === 'FullYear' && isLeapYear(mom.year()) && mom.month() === 1 && mom.date() === 29) {
                mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value, mom.month(), daysInMonth(value, mom.month()));
            }
            else {
                mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value);
            }
        }
    }

    // MOMENTS

    function stringGet (units) {
        units = normalizeUnits(units);
        if (isFunction(this[units])) {
            return this[units]();
        }
        return this;
    }


    function stringSet (units, value) {
        if (typeof units === 'object') {
            units = normalizeObjectUnits(units);
            var prioritized = getPrioritizedUnits(units);
            for (var i = 0; i < prioritized.length; i++) {
                this[prioritized[i].unit](units[prioritized[i].unit]);
            }
        } else {
            units = normalizeUnits(units);
            if (isFunction(this[units])) {
                return this[units](value);
            }
        }
        return this;
    }

    function mod(n, x) {
        return ((n % x) + x) % x;
    }

    var indexOf;

    if (Array.prototype.indexOf) {
        indexOf = Array.prototype.indexOf;
    } else {
        indexOf = function (o) {
            // I know
            var i;
            for (i = 0; i < this.length; ++i) {
                if (this[i] === o) {
                    return i;
                }
            }
            return -1;
        };
    }

    function daysInMonth(year, month) {
        if (isNaN(year) || isNaN(month)) {
            return NaN;
        }
        var modMonth = mod(month, 12);
        year += (month - modMonth) / 12;
        return modMonth === 1 ? (isLeapYear(year) ? 29 : 28) : (31 - modMonth % 7 % 2);
    }

    // FORMATTING

    addFormatToken('M', ['MM', 2], 'Mo', function () {
        return this.month() + 1;
    });

    addFormatToken('MMM', 0, 0, function (format) {
        return this.localeData().monthsShort(this, format);
    });

    addFormatToken('MMMM', 0, 0, function (format) {
        return this.localeData().months(this, format);
    });

    // ALIASES

    addUnitAlias('month', 'M');

    // PRIORITY

    addUnitPriority('month', 8);

    // PARSING

    addRegexToken('M',    match1to2);
    addRegexToken('MM',   match1to2, match2);
    addRegexToken('MMM',  function (isStrict, locale) {
        return locale.monthsShortRegex(isStrict);
    });
    addRegexToken('MMMM', function (isStrict, locale) {
        return locale.monthsRegex(isStrict);
    });

    addParseToken(['M', 'MM'], function (input, array) {
        array[MONTH] = toInt(input) - 1;
    });

    addParseToken(['MMM', 'MMMM'], function (input, array, config, token) {
        var month = config._locale.monthsParse(input, token, config._strict);
        // if we didn't find a month name, mark the date as invalid.
        if (month != null) {
            array[MONTH] = month;
        } else {
            getParsingFlags(config).invalidMonth = input;
        }
    });

    // LOCALES

    var MONTHS_IN_FORMAT = /D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/;
    var defaultLocaleMonths = 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_');
    function localeMonths (m, format) {
        if (!m) {
            return isArray(this._months) ? this._months :
                this._months['standalone'];
        }
        return isArray(this._months) ? this._months[m.month()] :
            this._months[(this._months.isFormat || MONTHS_IN_FORMAT).test(format) ? 'format' : 'standalone'][m.month()];
    }

    var defaultLocaleMonthsShort = 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_');
    function localeMonthsShort (m, format) {
        if (!m) {
            return isArray(this._monthsShort) ? this._monthsShort :
                this._monthsShort['standalone'];
        }
        return isArray(this._monthsShort) ? this._monthsShort[m.month()] :
            this._monthsShort[MONTHS_IN_FORMAT.test(format) ? 'format' : 'standalone'][m.month()];
    }

    function handleStrictParse(monthName, format, strict) {
        var i, ii, mom, llc = monthName.toLocaleLowerCase();
        if (!this._monthsParse) {
            // this is not used
            this._monthsParse = [];
            this._longMonthsParse = [];
            this._shortMonthsParse = [];
            for (i = 0; i < 12; ++i) {
                mom = createUTC([2000, i]);
                this._shortMonthsParse[i] = this.monthsShort(mom, '').toLocaleLowerCase();
                this._longMonthsParse[i] = this.months(mom, '').toLocaleLowerCase();
            }
        }

        if (strict) {
            if (format === 'MMM') {
                ii = indexOf.call(this._shortMonthsParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._longMonthsParse, llc);
                return ii !== -1 ? ii : null;
            }
        } else {
            if (format === 'MMM') {
                ii = indexOf.call(this._shortMonthsParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._longMonthsParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._longMonthsParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._shortMonthsParse, llc);
                return ii !== -1 ? ii : null;
            }
        }
    }

    function localeMonthsParse (monthName, format, strict) {
        var i, mom, regex;

        if (this._monthsParseExact) {
            return handleStrictParse.call(this, monthName, format, strict);
        }

        if (!this._monthsParse) {
            this._monthsParse = [];
            this._longMonthsParse = [];
            this._shortMonthsParse = [];
        }

        // TODO: add sorting
        // Sorting makes sure if one month (or abbr) is a prefix of another
        // see sorting in computeMonthsParse
        for (i = 0; i < 12; i++) {
            // make the regex if we don't have it already
            mom = createUTC([2000, i]);
            if (strict && !this._longMonthsParse[i]) {
                this._longMonthsParse[i] = new RegExp('^' + this.months(mom, '').replace('.', '') + '$', 'i');
                this._shortMonthsParse[i] = new RegExp('^' + this.monthsShort(mom, '').replace('.', '') + '$', 'i');
            }
            if (!strict && !this._monthsParse[i]) {
                regex = '^' + this.months(mom, '') + '|^' + this.monthsShort(mom, '');
                this._monthsParse[i] = new RegExp(regex.replace('.', ''), 'i');
            }
            // test the regex
            if (strict && format === 'MMMM' && this._longMonthsParse[i].test(monthName)) {
                return i;
            } else if (strict && format === 'MMM' && this._shortMonthsParse[i].test(monthName)) {
                return i;
            } else if (!strict && this._monthsParse[i].test(monthName)) {
                return i;
            }
        }
    }

    // MOMENTS

    function setMonth (mom, value) {
        var dayOfMonth;

        if (!mom.isValid()) {
            // No op
            return mom;
        }

        if (typeof value === 'string') {
            if (/^\d+$/.test(value)) {
                value = toInt(value);
            } else {
                value = mom.localeData().monthsParse(value);
                // TODO: Another silent failure?
                if (!isNumber(value)) {
                    return mom;
                }
            }
        }

        dayOfMonth = Math.min(mom.date(), daysInMonth(mom.year(), value));
        mom._d['set' + (mom._isUTC ? 'UTC' : '') + 'Month'](value, dayOfMonth);
        return mom;
    }

    function getSetMonth (value) {
        if (value != null) {
            setMonth(this, value);
            hooks.updateOffset(this, true);
            return this;
        } else {
            return get(this, 'Month');
        }
    }

    function getDaysInMonth () {
        return daysInMonth(this.year(), this.month());
    }

    var defaultMonthsShortRegex = matchWord;
    function monthsShortRegex (isStrict) {
        if (this._monthsParseExact) {
            if (!hasOwnProp(this, '_monthsRegex')) {
                computeMonthsParse.call(this);
            }
            if (isStrict) {
                return this._monthsShortStrictRegex;
            } else {
                return this._monthsShortRegex;
            }
        } else {
            if (!hasOwnProp(this, '_monthsShortRegex')) {
                this._monthsShortRegex = defaultMonthsShortRegex;
            }
            return this._monthsShortStrictRegex && isStrict ?
                this._monthsShortStrictRegex : this._monthsShortRegex;
        }
    }

    var defaultMonthsRegex = matchWord;
    function monthsRegex (isStrict) {
        if (this._monthsParseExact) {
            if (!hasOwnProp(this, '_monthsRegex')) {
                computeMonthsParse.call(this);
            }
            if (isStrict) {
                return this._monthsStrictRegex;
            } else {
                return this._monthsRegex;
            }
        } else {
            if (!hasOwnProp(this, '_monthsRegex')) {
                this._monthsRegex = defaultMonthsRegex;
            }
            return this._monthsStrictRegex && isStrict ?
                this._monthsStrictRegex : this._monthsRegex;
        }
    }

    function computeMonthsParse () {
        function cmpLenRev(a, b) {
            return b.length - a.length;
        }

        var shortPieces = [], longPieces = [], mixedPieces = [],
            i, mom;
        for (i = 0; i < 12; i++) {
            // make the regex if we don't have it already
            mom = createUTC([2000, i]);
            shortPieces.push(this.monthsShort(mom, ''));
            longPieces.push(this.months(mom, ''));
            mixedPieces.push(this.months(mom, ''));
            mixedPieces.push(this.monthsShort(mom, ''));
        }
        // Sorting makes sure if one month (or abbr) is a prefix of another it
        // will match the longer piece.
        shortPieces.sort(cmpLenRev);
        longPieces.sort(cmpLenRev);
        mixedPieces.sort(cmpLenRev);
        for (i = 0; i < 12; i++) {
            shortPieces[i] = regexEscape(shortPieces[i]);
            longPieces[i] = regexEscape(longPieces[i]);
        }
        for (i = 0; i < 24; i++) {
            mixedPieces[i] = regexEscape(mixedPieces[i]);
        }

        this._monthsRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
        this._monthsShortRegex = this._monthsRegex;
        this._monthsStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
        this._monthsShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
    }

    function createDate (y, m, d, h, M, s, ms) {
        // can't just apply() to create a date:
        // https://stackoverflow.com/q/181348
        var date = new Date(y, m, d, h, M, s, ms);

        // the date constructor remaps years 0-99 to 1900-1999
        if (y < 100 && y >= 0 && isFinite(date.getFullYear())) {
            date.setFullYear(y);
        }
        return date;
    }

    function createUTCDate (y) {
        var date = new Date(Date.UTC.apply(null, arguments));

        // the Date.UTC function remaps years 0-99 to 1900-1999
        if (y < 100 && y >= 0 && isFinite(date.getUTCFullYear())) {
            date.setUTCFullYear(y);
        }
        return date;
    }

    // start-of-first-week - start-of-year
    function firstWeekOffset(year, dow, doy) {
        var // first-week day -- which january is always in the first week (4 for iso, 1 for other)
            fwd = 7 + dow - doy,
            // first-week day local weekday -- which local weekday is fwd
            fwdlw = (7 + createUTCDate(year, 0, fwd).getUTCDay() - dow) % 7;

        return -fwdlw + fwd - 1;
    }

    // https://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday
    function dayOfYearFromWeeks(year, week, weekday, dow, doy) {
        var localWeekday = (7 + weekday - dow) % 7,
            weekOffset = firstWeekOffset(year, dow, doy),
            dayOfYear = 1 + 7 * (week - 1) + localWeekday + weekOffset,
            resYear, resDayOfYear;

        if (dayOfYear <= 0) {
            resYear = year - 1;
            resDayOfYear = daysInYear(resYear) + dayOfYear;
        } else if (dayOfYear > daysInYear(year)) {
            resYear = year + 1;
            resDayOfYear = dayOfYear - daysInYear(year);
        } else {
            resYear = year;
            resDayOfYear = dayOfYear;
        }

        return {
            year: resYear,
            dayOfYear: resDayOfYear
        };
    }

    function weekOfYear(mom, dow, doy) {
        var weekOffset = firstWeekOffset(mom.year(), dow, doy),
            week = Math.floor((mom.dayOfYear() - weekOffset - 1) / 7) + 1,
            resWeek, resYear;

        if (week < 1) {
            resYear = mom.year() - 1;
            resWeek = week + weeksInYear(resYear, dow, doy);
        } else if (week > weeksInYear(mom.year(), dow, doy)) {
            resWeek = week - weeksInYear(mom.year(), dow, doy);
            resYear = mom.year() + 1;
        } else {
            resYear = mom.year();
            resWeek = week;
        }

        return {
            week: resWeek,
            year: resYear
        };
    }

    function weeksInYear(year, dow, doy) {
        var weekOffset = firstWeekOffset(year, dow, doy),
            weekOffsetNext = firstWeekOffset(year + 1, dow, doy);
        return (daysInYear(year) - weekOffset + weekOffsetNext) / 7;
    }

    // FORMATTING

    addFormatToken('w', ['ww', 2], 'wo', 'week');
    addFormatToken('W', ['WW', 2], 'Wo', 'isoWeek');

    // ALIASES

    addUnitAlias('week', 'w');
    addUnitAlias('isoWeek', 'W');

    // PRIORITIES

    addUnitPriority('week', 5);
    addUnitPriority('isoWeek', 5);

    // PARSING

    addRegexToken('w',  match1to2);
    addRegexToken('ww', match1to2, match2);
    addRegexToken('W',  match1to2);
    addRegexToken('WW', match1to2, match2);

    addWeekParseToken(['w', 'ww', 'W', 'WW'], function (input, week, config, token) {
        week[token.substr(0, 1)] = toInt(input);
    });

    // HELPERS

    // LOCALES

    function localeWeek (mom) {
        return weekOfYear(mom, this._week.dow, this._week.doy).week;
    }

    var defaultLocaleWeek = {
        dow : 0, // Sunday is the first day of the week.
        doy : 6  // The week that contains Jan 1st is the first week of the year.
    };

    function localeFirstDayOfWeek () {
        return this._week.dow;
    }

    function localeFirstDayOfYear () {
        return this._week.doy;
    }

    // MOMENTS

    function getSetWeek (input) {
        var week = this.localeData().week(this);
        return input == null ? week : this.add((input - week) * 7, 'd');
    }

    function getSetISOWeek (input) {
        var week = weekOfYear(this, 1, 4).week;
        return input == null ? week : this.add((input - week) * 7, 'd');
    }

    // FORMATTING

    addFormatToken('d', 0, 'do', 'day');

    addFormatToken('dd', 0, 0, function (format) {
        return this.localeData().weekdaysMin(this, format);
    });

    addFormatToken('ddd', 0, 0, function (format) {
        return this.localeData().weekdaysShort(this, format);
    });

    addFormatToken('dddd', 0, 0, function (format) {
        return this.localeData().weekdays(this, format);
    });

    addFormatToken('e', 0, 0, 'weekday');
    addFormatToken('E', 0, 0, 'isoWeekday');

    // ALIASES

    addUnitAlias('day', 'd');
    addUnitAlias('weekday', 'e');
    addUnitAlias('isoWeekday', 'E');

    // PRIORITY
    addUnitPriority('day', 11);
    addUnitPriority('weekday', 11);
    addUnitPriority('isoWeekday', 11);

    // PARSING

    addRegexToken('d',    match1to2);
    addRegexToken('e',    match1to2);
    addRegexToken('E',    match1to2);
    addRegexToken('dd',   function (isStrict, locale) {
        return locale.weekdaysMinRegex(isStrict);
    });
    addRegexToken('ddd',   function (isStrict, locale) {
        return locale.weekdaysShortRegex(isStrict);
    });
    addRegexToken('dddd',   function (isStrict, locale) {
        return locale.weekdaysRegex(isStrict);
    });

    addWeekParseToken(['dd', 'ddd', 'dddd'], function (input, week, config, token) {
        var weekday = config._locale.weekdaysParse(input, token, config._strict);
        // if we didn't get a weekday name, mark the date as invalid
        if (weekday != null) {
            week.d = weekday;
        } else {
            getParsingFlags(config).invalidWeekday = input;
        }
    });

    addWeekParseToken(['d', 'e', 'E'], function (input, week, config, token) {
        week[token] = toInt(input);
    });

    // HELPERS

    function parseWeekday(input, locale) {
        if (typeof input !== 'string') {
            return input;
        }

        if (!isNaN(input)) {
            return parseInt(input, 10);
        }

        input = locale.weekdaysParse(input);
        if (typeof input === 'number') {
            return input;
        }

        return null;
    }

    function parseIsoWeekday(input, locale) {
        if (typeof input === 'string') {
            return locale.weekdaysParse(input) % 7 || 7;
        }
        return isNaN(input) ? null : input;
    }

    // LOCALES

    var defaultLocaleWeekdays = 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_');
    function localeWeekdays (m, format) {
        if (!m) {
            return isArray(this._weekdays) ? this._weekdays :
                this._weekdays['standalone'];
        }
        return isArray(this._weekdays) ? this._weekdays[m.day()] :
            this._weekdays[this._weekdays.isFormat.test(format) ? 'format' : 'standalone'][m.day()];
    }

    var defaultLocaleWeekdaysShort = 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_');
    function localeWeekdaysShort (m) {
        return (m) ? this._weekdaysShort[m.day()] : this._weekdaysShort;
    }

    var defaultLocaleWeekdaysMin = 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_');
    function localeWeekdaysMin (m) {
        return (m) ? this._weekdaysMin[m.day()] : this._weekdaysMin;
    }

    function handleStrictParse$1(weekdayName, format, strict) {
        var i, ii, mom, llc = weekdayName.toLocaleLowerCase();
        if (!this._weekdaysParse) {
            this._weekdaysParse = [];
            this._shortWeekdaysParse = [];
            this._minWeekdaysParse = [];

            for (i = 0; i < 7; ++i) {
                mom = createUTC([2000, 1]).day(i);
                this._minWeekdaysParse[i] = this.weekdaysMin(mom, '').toLocaleLowerCase();
                this._shortWeekdaysParse[i] = this.weekdaysShort(mom, '').toLocaleLowerCase();
                this._weekdaysParse[i] = this.weekdays(mom, '').toLocaleLowerCase();
            }
        }

        if (strict) {
            if (format === 'dddd') {
                ii = indexOf.call(this._weekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else if (format === 'ddd') {
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            }
        } else {
            if (format === 'dddd') {
                ii = indexOf.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else if (format === 'ddd') {
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._minWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            }
        }
    }

    function localeWeekdaysParse (weekdayName, format, strict) {
        var i, mom, regex;

        if (this._weekdaysParseExact) {
            return handleStrictParse$1.call(this, weekdayName, format, strict);
        }

        if (!this._weekdaysParse) {
            this._weekdaysParse = [];
            this._minWeekdaysParse = [];
            this._shortWeekdaysParse = [];
            this._fullWeekdaysParse = [];
        }

        for (i = 0; i < 7; i++) {
            // make the regex if we don't have it already

            mom = createUTC([2000, 1]).day(i);
            if (strict && !this._fullWeekdaysParse[i]) {
                this._fullWeekdaysParse[i] = new RegExp('^' + this.weekdays(mom, '').replace('.', '\\.?') + '$', 'i');
                this._shortWeekdaysParse[i] = new RegExp('^' + this.weekdaysShort(mom, '').replace('.', '\\.?') + '$', 'i');
                this._minWeekdaysParse[i] = new RegExp('^' + this.weekdaysMin(mom, '').replace('.', '\\.?') + '$', 'i');
            }
            if (!this._weekdaysParse[i]) {
                regex = '^' + this.weekdays(mom, '') + '|^' + this.weekdaysShort(mom, '') + '|^' + this.weekdaysMin(mom, '');
                this._weekdaysParse[i] = new RegExp(regex.replace('.', ''), 'i');
            }
            // test the regex
            if (strict && format === 'dddd' && this._fullWeekdaysParse[i].test(weekdayName)) {
                return i;
            } else if (strict && format === 'ddd' && this._shortWeekdaysParse[i].test(weekdayName)) {
                return i;
            } else if (strict && format === 'dd' && this._minWeekdaysParse[i].test(weekdayName)) {
                return i;
            } else if (!strict && this._weekdaysParse[i].test(weekdayName)) {
                return i;
            }
        }
    }

    // MOMENTS

    function getSetDayOfWeek (input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }
        var day = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
        if (input != null) {
            input = parseWeekday(input, this.localeData());
            return this.add(input - day, 'd');
        } else {
            return day;
        }
    }

    function getSetLocaleDayOfWeek (input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }
        var weekday = (this.day() + 7 - this.localeData()._week.dow) % 7;
        return input == null ? weekday : this.add(input - weekday, 'd');
    }

    function getSetISODayOfWeek (input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }

        // behaves the same as moment#day except
        // as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)
        // as a setter, sunday should belong to the previous week.

        if (input != null) {
            var weekday = parseIsoWeekday(input, this.localeData());
            return this.day(this.day() % 7 ? weekday : weekday - 7);
        } else {
            return this.day() || 7;
        }
    }

    var defaultWeekdaysRegex = matchWord;
    function weekdaysRegex (isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysStrictRegex;
            } else {
                return this._weekdaysRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                this._weekdaysRegex = defaultWeekdaysRegex;
            }
            return this._weekdaysStrictRegex && isStrict ?
                this._weekdaysStrictRegex : this._weekdaysRegex;
        }
    }

    var defaultWeekdaysShortRegex = matchWord;
    function weekdaysShortRegex (isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysShortStrictRegex;
            } else {
                return this._weekdaysShortRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysShortRegex')) {
                this._weekdaysShortRegex = defaultWeekdaysShortRegex;
            }
            return this._weekdaysShortStrictRegex && isStrict ?
                this._weekdaysShortStrictRegex : this._weekdaysShortRegex;
        }
    }

    var defaultWeekdaysMinRegex = matchWord;
    function weekdaysMinRegex (isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysMinStrictRegex;
            } else {
                return this._weekdaysMinRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysMinRegex')) {
                this._weekdaysMinRegex = defaultWeekdaysMinRegex;
            }
            return this._weekdaysMinStrictRegex && isStrict ?
                this._weekdaysMinStrictRegex : this._weekdaysMinRegex;
        }
    }


    function computeWeekdaysParse () {
        function cmpLenRev(a, b) {
            return b.length - a.length;
        }

        var minPieces = [], shortPieces = [], longPieces = [], mixedPieces = [],
            i, mom, minp, shortp, longp;
        for (i = 0; i < 7; i++) {
            // make the regex if we don't have it already
            mom = createUTC([2000, 1]).day(i);
            minp = this.weekdaysMin(mom, '');
            shortp = this.weekdaysShort(mom, '');
            longp = this.weekdays(mom, '');
            minPieces.push(minp);
            shortPieces.push(shortp);
            longPieces.push(longp);
            mixedPieces.push(minp);
            mixedPieces.push(shortp);
            mixedPieces.push(longp);
        }
        // Sorting makes sure if one weekday (or abbr) is a prefix of another it
        // will match the longer piece.
        minPieces.sort(cmpLenRev);
        shortPieces.sort(cmpLenRev);
        longPieces.sort(cmpLenRev);
        mixedPieces.sort(cmpLenRev);
        for (i = 0; i < 7; i++) {
            shortPieces[i] = regexEscape(shortPieces[i]);
            longPieces[i] = regexEscape(longPieces[i]);
            mixedPieces[i] = regexEscape(mixedPieces[i]);
        }

        this._weekdaysRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
        this._weekdaysShortRegex = this._weekdaysRegex;
        this._weekdaysMinRegex = this._weekdaysRegex;

        this._weekdaysStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
        this._weekdaysShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
        this._weekdaysMinStrictRegex = new RegExp('^(' + minPieces.join('|') + ')', 'i');
    }

    // FORMATTING

    function hFormat() {
        return this.hours() % 12 || 12;
    }

    function kFormat() {
        return this.hours() || 24;
    }

    addFormatToken('H', ['HH', 2], 0, 'hour');
    addFormatToken('h', ['hh', 2], 0, hFormat);
    addFormatToken('k', ['kk', 2], 0, kFormat);

    addFormatToken('hmm', 0, 0, function () {
        return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2);
    });

    addFormatToken('hmmss', 0, 0, function () {
        return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2) +
            zeroFill(this.seconds(), 2);
    });

    addFormatToken('Hmm', 0, 0, function () {
        return '' + this.hours() + zeroFill(this.minutes(), 2);
    });

    addFormatToken('Hmmss', 0, 0, function () {
        return '' + this.hours() + zeroFill(this.minutes(), 2) +
            zeroFill(this.seconds(), 2);
    });

    function meridiem (token, lowercase) {
        addFormatToken(token, 0, 0, function () {
            return this.localeData().meridiem(this.hours(), this.minutes(), lowercase);
        });
    }

    meridiem('a', true);
    meridiem('A', false);

    // ALIASES

    addUnitAlias('hour', 'h');

    // PRIORITY
    addUnitPriority('hour', 13);

    // PARSING

    function matchMeridiem (isStrict, locale) {
        return locale._meridiemParse;
    }

    addRegexToken('a',  matchMeridiem);
    addRegexToken('A',  matchMeridiem);
    addRegexToken('H',  match1to2);
    addRegexToken('h',  match1to2);
    addRegexToken('k',  match1to2);
    addRegexToken('HH', match1to2, match2);
    addRegexToken('hh', match1to2, match2);
    addRegexToken('kk', match1to2, match2);

    addRegexToken('hmm', match3to4);
    addRegexToken('hmmss', match5to6);
    addRegexToken('Hmm', match3to4);
    addRegexToken('Hmmss', match5to6);

    addParseToken(['H', 'HH'], HOUR);
    addParseToken(['k', 'kk'], function (input, array, config) {
        var kInput = toInt(input);
        array[HOUR] = kInput === 24 ? 0 : kInput;
    });
    addParseToken(['a', 'A'], function (input, array, config) {
        config._isPm = config._locale.isPM(input);
        config._meridiem = input;
    });
    addParseToken(['h', 'hh'], function (input, array, config) {
        array[HOUR] = toInt(input);
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('hmm', function (input, array, config) {
        var pos = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos));
        array[MINUTE] = toInt(input.substr(pos));
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('hmmss', function (input, array, config) {
        var pos1 = input.length - 4;
        var pos2 = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos1));
        array[MINUTE] = toInt(input.substr(pos1, 2));
        array[SECOND] = toInt(input.substr(pos2));
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('Hmm', function (input, array, config) {
        var pos = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos));
        array[MINUTE] = toInt(input.substr(pos));
    });
    addParseToken('Hmmss', function (input, array, config) {
        var pos1 = input.length - 4;
        var pos2 = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos1));
        array[MINUTE] = toInt(input.substr(pos1, 2));
        array[SECOND] = toInt(input.substr(pos2));
    });

    // LOCALES

    function localeIsPM (input) {
        // IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays
        // Using charAt should be more compatible.
        return ((input + '').toLowerCase().charAt(0) === 'p');
    }

    var defaultLocaleMeridiemParse = /[ap]\.?m?\.?/i;
    function localeMeridiem (hours, minutes, isLower) {
        if (hours > 11) {
            return isLower ? 'pm' : 'PM';
        } else {
            return isLower ? 'am' : 'AM';
        }
    }


    // MOMENTS

    // Setting the hour should keep the time, because the user explicitly
    // specified which hour they want. So trying to maintain the same hour (in
    // a new timezone) makes sense. Adding/subtracting hours does not follow
    // this rule.
    var getSetHour = makeGetSet('Hours', true);

    var baseConfig = {
        calendar: defaultCalendar,
        longDateFormat: defaultLongDateFormat,
        invalidDate: defaultInvalidDate,
        ordinal: defaultOrdinal,
        dayOfMonthOrdinalParse: defaultDayOfMonthOrdinalParse,
        relativeTime: defaultRelativeTime,

        months: defaultLocaleMonths,
        monthsShort: defaultLocaleMonthsShort,

        week: defaultLocaleWeek,

        weekdays: defaultLocaleWeekdays,
        weekdaysMin: defaultLocaleWeekdaysMin,
        weekdaysShort: defaultLocaleWeekdaysShort,

        meridiemParse: defaultLocaleMeridiemParse
    };

    // internal storage for locale config files
    var locales = {};
    var localeFamilies = {};
    var globalLocale;

    function normalizeLocale(key) {
        return key ? key.toLowerCase().replace('_', '-') : key;
    }

    // pick the locale from the array
    // try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each
    // substring from most specific to least, but move to the next array item if it's a more specific variant than the current root
    function chooseLocale(names) {
        var i = 0, j, next, locale, split;

        while (i < names.length) {
            split = normalizeLocale(names[i]).split('-');
            j = split.length;
            next = normalizeLocale(names[i + 1]);
            next = next ? next.split('-') : null;
            while (j > 0) {
                locale = loadLocale(split.slice(0, j).join('-'));
                if (locale) {
                    return locale;
                }
                if (next && next.length >= j && compareArrays(split, next, true) >= j - 1) {
                    //the next array item is better than a shallower substring of this one
                    break;
                }
                j--;
            }
            i++;
        }
        return globalLocale;
    }

    function loadLocale(name) {
        var oldLocale = null;
        // TODO: Find a better way to register and load all the locales in Node
        if (!locales[name] && (typeof module !== 'undefined') &&
                module && module.exports) {
            try {
                oldLocale = globalLocale._abbr;
                var aliasedRequire = require;
                __webpack_require__("b311")("./" + name);
                getSetGlobalLocale(oldLocale);
            } catch (e) {}
        }
        return locales[name];
    }

    // This function will load locale and then set the global locale.  If
    // no arguments are passed in, it will simply return the current global
    // locale key.
    function getSetGlobalLocale (key, values) {
        var data;
        if (key) {
            if (isUndefined(values)) {
                data = getLocale(key);
            }
            else {
                data = defineLocale(key, values);
            }

            if (data) {
                // moment.duration._locale = moment._locale = data;
                globalLocale = data;
            }
            else {
                if ((typeof console !==  'undefined') && console.warn) {
                    //warn user if arguments are passed but the locale could not be set
                    console.warn('Locale ' + key +  ' not found. Did you forget to load it?');
                }
            }
        }

        return globalLocale._abbr;
    }

    function defineLocale (name, config) {
        if (config !== null) {
            var locale, parentConfig = baseConfig;
            config.abbr = name;
            if (locales[name] != null) {
                deprecateSimple('defineLocaleOverride',
                        'use moment.updateLocale(localeName, config) to change ' +
                        'an existing locale. moment.defineLocale(localeName, ' +
                        'config) should only be used for creating a new locale ' +
                        'See http://momentjs.com/guides/#/warnings/define-locale/ for more info.');
                parentConfig = locales[name]._config;
            } else if (config.parentLocale != null) {
                if (locales[config.parentLocale] != null) {
                    parentConfig = locales[config.parentLocale]._config;
                } else {
                    locale = loadLocale(config.parentLocale);
                    if (locale != null) {
                        parentConfig = locale._config;
                    } else {
                        if (!localeFamilies[config.parentLocale]) {
                            localeFamilies[config.parentLocale] = [];
                        }
                        localeFamilies[config.parentLocale].push({
                            name: name,
                            config: config
                        });
                        return null;
                    }
                }
            }
            locales[name] = new Locale(mergeConfigs(parentConfig, config));

            if (localeFamilies[name]) {
                localeFamilies[name].forEach(function (x) {
                    defineLocale(x.name, x.config);
                });
            }

            // backwards compat for now: also set the locale
            // make sure we set the locale AFTER all child locales have been
            // created, so we won't end up with the child locale set.
            getSetGlobalLocale(name);


            return locales[name];
        } else {
            // useful for testing
            delete locales[name];
            return null;
        }
    }

    function updateLocale(name, config) {
        if (config != null) {
            var locale, tmpLocale, parentConfig = baseConfig;
            // MERGE
            tmpLocale = loadLocale(name);
            if (tmpLocale != null) {
                parentConfig = tmpLocale._config;
            }
            config = mergeConfigs(parentConfig, config);
            locale = new Locale(config);
            locale.parentLocale = locales[name];
            locales[name] = locale;

            // backwards compat for now: also set the locale
            getSetGlobalLocale(name);
        } else {
            // pass null for config to unupdate, useful for tests
            if (locales[name] != null) {
                if (locales[name].parentLocale != null) {
                    locales[name] = locales[name].parentLocale;
                } else if (locales[name] != null) {
                    delete locales[name];
                }
            }
        }
        return locales[name];
    }

    // returns locale data
    function getLocale (key) {
        var locale;

        if (key && key._locale && key._locale._abbr) {
            key = key._locale._abbr;
        }

        if (!key) {
            return globalLocale;
        }

        if (!isArray(key)) {
            //short-circuit everything else
            locale = loadLocale(key);
            if (locale) {
                return locale;
            }
            key = [key];
        }

        return chooseLocale(key);
    }

    function listLocales() {
        return keys(locales);
    }

    function checkOverflow (m) {
        var overflow;
        var a = m._a;

        if (a && getParsingFlags(m).overflow === -2) {
            overflow =
                a[MONTH]       < 0 || a[MONTH]       > 11  ? MONTH :
                a[DATE]        < 1 || a[DATE]        > daysInMonth(a[YEAR], a[MONTH]) ? DATE :
                a[HOUR]        < 0 || a[HOUR]        > 24 || (a[HOUR] === 24 && (a[MINUTE] !== 0 || a[SECOND] !== 0 || a[MILLISECOND] !== 0)) ? HOUR :
                a[MINUTE]      < 0 || a[MINUTE]      > 59  ? MINUTE :
                a[SECOND]      < 0 || a[SECOND]      > 59  ? SECOND :
                a[MILLISECOND] < 0 || a[MILLISECOND] > 999 ? MILLISECOND :
                -1;

            if (getParsingFlags(m)._overflowDayOfYear && (overflow < YEAR || overflow > DATE)) {
                overflow = DATE;
            }
            if (getParsingFlags(m)._overflowWeeks && overflow === -1) {
                overflow = WEEK;
            }
            if (getParsingFlags(m)._overflowWeekday && overflow === -1) {
                overflow = WEEKDAY;
            }

            getParsingFlags(m).overflow = overflow;
        }

        return m;
    }

    // Pick the first defined of two or three arguments.
    function defaults(a, b, c) {
        if (a != null) {
            return a;
        }
        if (b != null) {
            return b;
        }
        return c;
    }

    function currentDateArray(config) {
        // hooks is actually the exported moment object
        var nowValue = new Date(hooks.now());
        if (config._useUTC) {
            return [nowValue.getUTCFullYear(), nowValue.getUTCMonth(), nowValue.getUTCDate()];
        }
        return [nowValue.getFullYear(), nowValue.getMonth(), nowValue.getDate()];
    }

    // convert an array to a date.
    // the array should mirror the parameters below
    // note: all values past the year are optional and will default to the lowest possible value.
    // [year, month, day , hour, minute, second, millisecond]
    function configFromArray (config) {
        var i, date, input = [], currentDate, expectedWeekday, yearToUse;

        if (config._d) {
            return;
        }

        currentDate = currentDateArray(config);

        //compute day of the year from weeks and weekdays
        if (config._w && config._a[DATE] == null && config._a[MONTH] == null) {
            dayOfYearFromWeekInfo(config);
        }

        //if the day of the year is set, figure out what it is
        if (config._dayOfYear != null) {
            yearToUse = defaults(config._a[YEAR], currentDate[YEAR]);

            if (config._dayOfYear > daysInYear(yearToUse) || config._dayOfYear === 0) {
                getParsingFlags(config)._overflowDayOfYear = true;
            }

            date = createUTCDate(yearToUse, 0, config._dayOfYear);
            config._a[MONTH] = date.getUTCMonth();
            config._a[DATE] = date.getUTCDate();
        }

        // Default to current date.
        // * if no year, month, day of month are given, default to today
        // * if day of month is given, default month and year
        // * if month is given, default only year
        // * if year is given, don't default anything
        for (i = 0; i < 3 && config._a[i] == null; ++i) {
            config._a[i] = input[i] = currentDate[i];
        }

        // Zero out whatever was not defaulted, including time
        for (; i < 7; i++) {
            config._a[i] = input[i] = (config._a[i] == null) ? (i === 2 ? 1 : 0) : config._a[i];
        }

        // Check for 24:00:00.000
        if (config._a[HOUR] === 24 &&
                config._a[MINUTE] === 0 &&
                config._a[SECOND] === 0 &&
                config._a[MILLISECOND] === 0) {
            config._nextDay = true;
            config._a[HOUR] = 0;
        }

        config._d = (config._useUTC ? createUTCDate : createDate).apply(null, input);
        expectedWeekday = config._useUTC ? config._d.getUTCDay() : config._d.getDay();

        // Apply timezone offset from input. The actual utcOffset can be changed
        // with parseZone.
        if (config._tzm != null) {
            config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);
        }

        if (config._nextDay) {
            config._a[HOUR] = 24;
        }

        // check for mismatching day of week
        if (config._w && typeof config._w.d !== 'undefined' && config._w.d !== expectedWeekday) {
            getParsingFlags(config).weekdayMismatch = true;
        }
    }

    function dayOfYearFromWeekInfo(config) {
        var w, weekYear, week, weekday, dow, doy, temp, weekdayOverflow;

        w = config._w;
        if (w.GG != null || w.W != null || w.E != null) {
            dow = 1;
            doy = 4;

            // TODO: We need to take the current isoWeekYear, but that depends on
            // how we interpret now (local, utc, fixed offset). So create
            // a now version of current config (take local/utc/offset flags, and
            // create now).
            weekYear = defaults(w.GG, config._a[YEAR], weekOfYear(createLocal(), 1, 4).year);
            week = defaults(w.W, 1);
            weekday = defaults(w.E, 1);
            if (weekday < 1 || weekday > 7) {
                weekdayOverflow = true;
            }
        } else {
            dow = config._locale._week.dow;
            doy = config._locale._week.doy;

            var curWeek = weekOfYear(createLocal(), dow, doy);

            weekYear = defaults(w.gg, config._a[YEAR], curWeek.year);

            // Default to current week.
            week = defaults(w.w, curWeek.week);

            if (w.d != null) {
                // weekday -- low day numbers are considered next week
                weekday = w.d;
                if (weekday < 0 || weekday > 6) {
                    weekdayOverflow = true;
                }
            } else if (w.e != null) {
                // local weekday -- counting starts from begining of week
                weekday = w.e + dow;
                if (w.e < 0 || w.e > 6) {
                    weekdayOverflow = true;
                }
            } else {
                // default to begining of week
                weekday = dow;
            }
        }
        if (week < 1 || week > weeksInYear(weekYear, dow, doy)) {
            getParsingFlags(config)._overflowWeeks = true;
        } else if (weekdayOverflow != null) {
            getParsingFlags(config)._overflowWeekday = true;
        } else {
            temp = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy);
            config._a[YEAR] = temp.year;
            config._dayOfYear = temp.dayOfYear;
        }
    }

    // iso 8601 regex
    // 0000-00-00 0000-W00 or 0000-W00-0 + T + 00 or 00:00 or 00:00:00 or 00:00:00.000 + +00:00 or +0000 or +00)
    var extendedIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;
    var basicIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;

    var tzRegex = /Z|[+-]\d\d(?::?\d\d)?/;

    var isoDates = [
        ['YYYYYY-MM-DD', /[+-]\d{6}-\d\d-\d\d/],
        ['YYYY-MM-DD', /\d{4}-\d\d-\d\d/],
        ['GGGG-[W]WW-E', /\d{4}-W\d\d-\d/],
        ['GGGG-[W]WW', /\d{4}-W\d\d/, false],
        ['YYYY-DDD', /\d{4}-\d{3}/],
        ['YYYY-MM', /\d{4}-\d\d/, false],
        ['YYYYYYMMDD', /[+-]\d{10}/],
        ['YYYYMMDD', /\d{8}/],
        // YYYYMM is NOT allowed by the standard
        ['GGGG[W]WWE', /\d{4}W\d{3}/],
        ['GGGG[W]WW', /\d{4}W\d{2}/, false],
        ['YYYYDDD', /\d{7}/]
    ];

    // iso time formats and regexes
    var isoTimes = [
        ['HH:mm:ss.SSSS', /\d\d:\d\d:\d\d\.\d+/],
        ['HH:mm:ss,SSSS', /\d\d:\d\d:\d\d,\d+/],
        ['HH:mm:ss', /\d\d:\d\d:\d\d/],
        ['HH:mm', /\d\d:\d\d/],
        ['HHmmss.SSSS', /\d\d\d\d\d\d\.\d+/],
        ['HHmmss,SSSS', /\d\d\d\d\d\d,\d+/],
        ['HHmmss', /\d\d\d\d\d\d/],
        ['HHmm', /\d\d\d\d/],
        ['HH', /\d\d/]
    ];

    var aspNetJsonRegex = /^\/?Date\((\-?\d+)/i;

    // date from iso format
    function configFromISO(config) {
        var i, l,
            string = config._i,
            match = extendedIsoRegex.exec(string) || basicIsoRegex.exec(string),
            allowTime, dateFormat, timeFormat, tzFormat;

        if (match) {
            getParsingFlags(config).iso = true;

            for (i = 0, l = isoDates.length; i < l; i++) {
                if (isoDates[i][1].exec(match[1])) {
                    dateFormat = isoDates[i][0];
                    allowTime = isoDates[i][2] !== false;
                    break;
                }
            }
            if (dateFormat == null) {
                config._isValid = false;
                return;
            }
            if (match[3]) {
                for (i = 0, l = isoTimes.length; i < l; i++) {
                    if (isoTimes[i][1].exec(match[3])) {
                        // match[2] should be 'T' or space
                        timeFormat = (match[2] || ' ') + isoTimes[i][0];
                        break;
                    }
                }
                if (timeFormat == null) {
                    config._isValid = false;
                    return;
                }
            }
            if (!allowTime && timeFormat != null) {
                config._isValid = false;
                return;
            }
            if (match[4]) {
                if (tzRegex.exec(match[4])) {
                    tzFormat = 'Z';
                } else {
                    config._isValid = false;
                    return;
                }
            }
            config._f = dateFormat + (timeFormat || '') + (tzFormat || '');
            configFromStringAndFormat(config);
        } else {
            config._isValid = false;
        }
    }

    // RFC 2822 regex: For details see https://tools.ietf.org/html/rfc2822#section-3.3
    var rfc2822 = /^(?:(Mon|Tue|Wed|Thu|Fri|Sat|Sun),?\s)?(\d{1,2})\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(\d{2,4})\s(\d\d):(\d\d)(?::(\d\d))?\s(?:(UT|GMT|[ECMP][SD]T)|([Zz])|([+-]\d{4}))$/;

    function extractFromRFC2822Strings(yearStr, monthStr, dayStr, hourStr, minuteStr, secondStr) {
        var result = [
            untruncateYear(yearStr),
            defaultLocaleMonthsShort.indexOf(monthStr),
            parseInt(dayStr, 10),
            parseInt(hourStr, 10),
            parseInt(minuteStr, 10)
        ];

        if (secondStr) {
            result.push(parseInt(secondStr, 10));
        }

        return result;
    }

    function untruncateYear(yearStr) {
        var year = parseInt(yearStr, 10);
        if (year <= 49) {
            return 2000 + year;
        } else if (year <= 999) {
            return 1900 + year;
        }
        return year;
    }

    function preprocessRFC2822(s) {
        // Remove comments and folding whitespace and replace multiple-spaces with a single space
        return s.replace(/\([^)]*\)|[\n\t]/g, ' ').replace(/(\s\s+)/g, ' ').replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    }

    function checkWeekday(weekdayStr, parsedInput, config) {
        if (weekdayStr) {
            // TODO: Replace the vanilla JS Date object with an indepentent day-of-week check.
            var weekdayProvided = defaultLocaleWeekdaysShort.indexOf(weekdayStr),
                weekdayActual = new Date(parsedInput[0], parsedInput[1], parsedInput[2]).getDay();
            if (weekdayProvided !== weekdayActual) {
                getParsingFlags(config).weekdayMismatch = true;
                config._isValid = false;
                return false;
            }
        }
        return true;
    }

    var obsOffsets = {
        UT: 0,
        GMT: 0,
        EDT: -4 * 60,
        EST: -5 * 60,
        CDT: -5 * 60,
        CST: -6 * 60,
        MDT: -6 * 60,
        MST: -7 * 60,
        PDT: -7 * 60,
        PST: -8 * 60
    };

    function calculateOffset(obsOffset, militaryOffset, numOffset) {
        if (obsOffset) {
            return obsOffsets[obsOffset];
        } else if (militaryOffset) {
            // the only allowed military tz is Z
            return 0;
        } else {
            var hm = parseInt(numOffset, 10);
            var m = hm % 100, h = (hm - m) / 100;
            return h * 60 + m;
        }
    }

    // date and time from ref 2822 format
    function configFromRFC2822(config) {
        var match = rfc2822.exec(preprocessRFC2822(config._i));
        if (match) {
            var parsedArray = extractFromRFC2822Strings(match[4], match[3], match[2], match[5], match[6], match[7]);
            if (!checkWeekday(match[1], parsedArray, config)) {
                return;
            }

            config._a = parsedArray;
            config._tzm = calculateOffset(match[8], match[9], match[10]);

            config._d = createUTCDate.apply(null, config._a);
            config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);

            getParsingFlags(config).rfc2822 = true;
        } else {
            config._isValid = false;
        }
    }

    // date from iso format or fallback
    function configFromString(config) {
        var matched = aspNetJsonRegex.exec(config._i);

        if (matched !== null) {
            config._d = new Date(+matched[1]);
            return;
        }

        configFromISO(config);
        if (config._isValid === false) {
            delete config._isValid;
        } else {
            return;
        }

        configFromRFC2822(config);
        if (config._isValid === false) {
            delete config._isValid;
        } else {
            return;
        }

        // Final attempt, use Input Fallback
        hooks.createFromInputFallback(config);
    }

    hooks.createFromInputFallback = deprecate(
        'value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), ' +
        'which is not reliable across all browsers and versions. Non RFC2822/ISO date formats are ' +
        'discouraged and will be removed in an upcoming major release. Please refer to ' +
        'http://momentjs.com/guides/#/warnings/js-date/ for more info.',
        function (config) {
            config._d = new Date(config._i + (config._useUTC ? ' UTC' : ''));
        }
    );

    // constant that refers to the ISO standard
    hooks.ISO_8601 = function () {};

    // constant that refers to the RFC 2822 form
    hooks.RFC_2822 = function () {};

    // date from string and format string
    function configFromStringAndFormat(config) {
        // TODO: Move this to another part of the creation flow to prevent circular deps
        if (config._f === hooks.ISO_8601) {
            configFromISO(config);
            return;
        }
        if (config._f === hooks.RFC_2822) {
            configFromRFC2822(config);
            return;
        }
        config._a = [];
        getParsingFlags(config).empty = true;

        // This array is used to make a Date, either with `new Date` or `Date.UTC`
        var string = '' + config._i,
            i, parsedInput, tokens, token, skipped,
            stringLength = string.length,
            totalParsedInputLength = 0;

        tokens = expandFormat(config._f, config._locale).match(formattingTokens) || [];

        for (i = 0; i < tokens.length; i++) {
            token = tokens[i];
            parsedInput = (string.match(getParseRegexForToken(token, config)) || [])[0];
            // console.log('token', token, 'parsedInput', parsedInput,
            //         'regex', getParseRegexForToken(token, config));
            if (parsedInput) {
                skipped = string.substr(0, string.indexOf(parsedInput));
                if (skipped.length > 0) {
                    getParsingFlags(config).unusedInput.push(skipped);
                }
                string = string.slice(string.indexOf(parsedInput) + parsedInput.length);
                totalParsedInputLength += parsedInput.length;
            }
            // don't parse if it's not a known token
            if (formatTokenFunctions[token]) {
                if (parsedInput) {
                    getParsingFlags(config).empty = false;
                }
                else {
                    getParsingFlags(config).unusedTokens.push(token);
                }
                addTimeToArrayFromToken(token, parsedInput, config);
            }
            else if (config._strict && !parsedInput) {
                getParsingFlags(config).unusedTokens.push(token);
            }
        }

        // add remaining unparsed input length to the string
        getParsingFlags(config).charsLeftOver = stringLength - totalParsedInputLength;
        if (string.length > 0) {
            getParsingFlags(config).unusedInput.push(string);
        }

        // clear _12h flag if hour is <= 12
        if (config._a[HOUR] <= 12 &&
            getParsingFlags(config).bigHour === true &&
            config._a[HOUR] > 0) {
            getParsingFlags(config).bigHour = undefined;
        }

        getParsingFlags(config).parsedDateParts = config._a.slice(0);
        getParsingFlags(config).meridiem = config._meridiem;
        // handle meridiem
        config._a[HOUR] = meridiemFixWrap(config._locale, config._a[HOUR], config._meridiem);

        configFromArray(config);
        checkOverflow(config);
    }


    function meridiemFixWrap (locale, hour, meridiem) {
        var isPm;

        if (meridiem == null) {
            // nothing to do
            return hour;
        }
        if (locale.meridiemHour != null) {
            return locale.meridiemHour(hour, meridiem);
        } else if (locale.isPM != null) {
            // Fallback
            isPm = locale.isPM(meridiem);
            if (isPm && hour < 12) {
                hour += 12;
            }
            if (!isPm && hour === 12) {
                hour = 0;
            }
            return hour;
        } else {
            // this is not supposed to happen
            return hour;
        }
    }

    // date from string and array of format strings
    function configFromStringAndArray(config) {
        var tempConfig,
            bestMoment,

            scoreToBeat,
            i,
            currentScore;

        if (config._f.length === 0) {
            getParsingFlags(config).invalidFormat = true;
            config._d = new Date(NaN);
            return;
        }

        for (i = 0; i < config._f.length; i++) {
            currentScore = 0;
            tempConfig = copyConfig({}, config);
            if (config._useUTC != null) {
                tempConfig._useUTC = config._useUTC;
            }
            tempConfig._f = config._f[i];
            configFromStringAndFormat(tempConfig);

            if (!isValid(tempConfig)) {
                continue;
            }

            // if there is any input that was not parsed add a penalty for that format
            currentScore += getParsingFlags(tempConfig).charsLeftOver;

            //or tokens
            currentScore += getParsingFlags(tempConfig).unusedTokens.length * 10;

            getParsingFlags(tempConfig).score = currentScore;

            if (scoreToBeat == null || currentScore < scoreToBeat) {
                scoreToBeat = currentScore;
                bestMoment = tempConfig;
            }
        }

        extend(config, bestMoment || tempConfig);
    }

    function configFromObject(config) {
        if (config._d) {
            return;
        }

        var i = normalizeObjectUnits(config._i);
        config._a = map([i.year, i.month, i.day || i.date, i.hour, i.minute, i.second, i.millisecond], function (obj) {
            return obj && parseInt(obj, 10);
        });

        configFromArray(config);
    }

    function createFromConfig (config) {
        var res = new Moment(checkOverflow(prepareConfig(config)));
        if (res._nextDay) {
            // Adding is smart enough around DST
            res.add(1, 'd');
            res._nextDay = undefined;
        }

        return res;
    }

    function prepareConfig (config) {
        var input = config._i,
            format = config._f;

        config._locale = config._locale || getLocale(config._l);

        if (input === null || (format === undefined && input === '')) {
            return createInvalid({nullInput: true});
        }

        if (typeof input === 'string') {
            config._i = input = config._locale.preparse(input);
        }

        if (isMoment(input)) {
            return new Moment(checkOverflow(input));
        } else if (isDate(input)) {
            config._d = input;
        } else if (isArray(format)) {
            configFromStringAndArray(config);
        } else if (format) {
            configFromStringAndFormat(config);
        }  else {
            configFromInput(config);
        }

        if (!isValid(config)) {
            config._d = null;
        }

        return config;
    }

    function configFromInput(config) {
        var input = config._i;
        if (isUndefined(input)) {
            config._d = new Date(hooks.now());
        } else if (isDate(input)) {
            config._d = new Date(input.valueOf());
        } else if (typeof input === 'string') {
            configFromString(config);
        } else if (isArray(input)) {
            config._a = map(input.slice(0), function (obj) {
                return parseInt(obj, 10);
            });
            configFromArray(config);
        } else if (isObject(input)) {
            configFromObject(config);
        } else if (isNumber(input)) {
            // from milliseconds
            config._d = new Date(input);
        } else {
            hooks.createFromInputFallback(config);
        }
    }

    function createLocalOrUTC (input, format, locale, strict, isUTC) {
        var c = {};

        if (locale === true || locale === false) {
            strict = locale;
            locale = undefined;
        }

        if ((isObject(input) && isObjectEmpty(input)) ||
                (isArray(input) && input.length === 0)) {
            input = undefined;
        }
        // object construction must be done this way.
        // https://github.com/moment/moment/issues/1423
        c._isAMomentObject = true;
        c._useUTC = c._isUTC = isUTC;
        c._l = locale;
        c._i = input;
        c._f = format;
        c._strict = strict;

        return createFromConfig(c);
    }

    function createLocal (input, format, locale, strict) {
        return createLocalOrUTC(input, format, locale, strict, false);
    }

    var prototypeMin = deprecate(
        'moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/',
        function () {
            var other = createLocal.apply(null, arguments);
            if (this.isValid() && other.isValid()) {
                return other < this ? this : other;
            } else {
                return createInvalid();
            }
        }
    );

    var prototypeMax = deprecate(
        'moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/',
        function () {
            var other = createLocal.apply(null, arguments);
            if (this.isValid() && other.isValid()) {
                return other > this ? this : other;
            } else {
                return createInvalid();
            }
        }
    );

    // Pick a moment m from moments so that m[fn](other) is true for all
    // other. This relies on the function fn to be transitive.
    //
    // moments should either be an array of moment objects or an array, whose
    // first element is an array of moment objects.
    function pickBy(fn, moments) {
        var res, i;
        if (moments.length === 1 && isArray(moments[0])) {
            moments = moments[0];
        }
        if (!moments.length) {
            return createLocal();
        }
        res = moments[0];
        for (i = 1; i < moments.length; ++i) {
            if (!moments[i].isValid() || moments[i][fn](res)) {
                res = moments[i];
            }
        }
        return res;
    }

    // TODO: Use [].sort instead?
    function min () {
        var args = [].slice.call(arguments, 0);

        return pickBy('isBefore', args);
    }

    function max () {
        var args = [].slice.call(arguments, 0);

        return pickBy('isAfter', args);
    }

    var now = function () {
        return Date.now ? Date.now() : +(new Date());
    };

    var ordering = ['year', 'quarter', 'month', 'week', 'day', 'hour', 'minute', 'second', 'millisecond'];

    function isDurationValid(m) {
        for (var key in m) {
            if (!(indexOf.call(ordering, key) !== -1 && (m[key] == null || !isNaN(m[key])))) {
                return false;
            }
        }

        var unitHasDecimal = false;
        for (var i = 0; i < ordering.length; ++i) {
            if (m[ordering[i]]) {
                if (unitHasDecimal) {
                    return false; // only allow non-integers for smallest unit
                }
                if (parseFloat(m[ordering[i]]) !== toInt(m[ordering[i]])) {
                    unitHasDecimal = true;
                }
            }
        }

        return true;
    }

    function isValid$1() {
        return this._isValid;
    }

    function createInvalid$1() {
        return createDuration(NaN);
    }

    function Duration (duration) {
        var normalizedInput = normalizeObjectUnits(duration),
            years = normalizedInput.year || 0,
            quarters = normalizedInput.quarter || 0,
            months = normalizedInput.month || 0,
            weeks = normalizedInput.week || 0,
            days = normalizedInput.day || 0,
            hours = normalizedInput.hour || 0,
            minutes = normalizedInput.minute || 0,
            seconds = normalizedInput.second || 0,
            milliseconds = normalizedInput.millisecond || 0;

        this._isValid = isDurationValid(normalizedInput);

        // representation for dateAddRemove
        this._milliseconds = +milliseconds +
            seconds * 1e3 + // 1000
            minutes * 6e4 + // 1000 * 60
            hours * 1000 * 60 * 60; //using 1000 * 60 * 60 instead of 36e5 to avoid floating point rounding errors https://github.com/moment/moment/issues/2978
        // Because of dateAddRemove treats 24 hours as different from a
        // day when working around DST, we need to store them separately
        this._days = +days +
            weeks * 7;
        // It is impossible to translate months into days without knowing
        // which months you are are talking about, so we have to store
        // it separately.
        this._months = +months +
            quarters * 3 +
            years * 12;

        this._data = {};

        this._locale = getLocale();

        this._bubble();
    }

    function isDuration (obj) {
        return obj instanceof Duration;
    }

    function absRound (number) {
        if (number < 0) {
            return Math.round(-1 * number) * -1;
        } else {
            return Math.round(number);
        }
    }

    // FORMATTING

    function offset (token, separator) {
        addFormatToken(token, 0, 0, function () {
            var offset = this.utcOffset();
            var sign = '+';
            if (offset < 0) {
                offset = -offset;
                sign = '-';
            }
            return sign + zeroFill(~~(offset / 60), 2) + separator + zeroFill(~~(offset) % 60, 2);
        });
    }

    offset('Z', ':');
    offset('ZZ', '');

    // PARSING

    addRegexToken('Z',  matchShortOffset);
    addRegexToken('ZZ', matchShortOffset);
    addParseToken(['Z', 'ZZ'], function (input, array, config) {
        config._useUTC = true;
        config._tzm = offsetFromString(matchShortOffset, input);
    });

    // HELPERS

    // timezone chunker
    // '+10:00' > ['10',  '00']
    // '-1530'  > ['-15', '30']
    var chunkOffset = /([\+\-]|\d\d)/gi;

    function offsetFromString(matcher, string) {
        var matches = (string || '').match(matcher);

        if (matches === null) {
            return null;
        }

        var chunk   = matches[matches.length - 1] || [];
        var parts   = (chunk + '').match(chunkOffset) || ['-', 0, 0];
        var minutes = +(parts[1] * 60) + toInt(parts[2]);

        return minutes === 0 ?
          0 :
          parts[0] === '+' ? minutes : -minutes;
    }

    // Return a moment from input, that is local/utc/zone equivalent to model.
    function cloneWithOffset(input, model) {
        var res, diff;
        if (model._isUTC) {
            res = model.clone();
            diff = (isMoment(input) || isDate(input) ? input.valueOf() : createLocal(input).valueOf()) - res.valueOf();
            // Use low-level api, because this fn is low-level api.
            res._d.setTime(res._d.valueOf() + diff);
            hooks.updateOffset(res, false);
            return res;
        } else {
            return createLocal(input).local();
        }
    }

    function getDateOffset (m) {
        // On Firefox.24 Date#getTimezoneOffset returns a floating point.
        // https://github.com/moment/moment/pull/1871
        return -Math.round(m._d.getTimezoneOffset() / 15) * 15;
    }

    // HOOKS

    // This function will be called whenever a moment is mutated.
    // It is intended to keep the offset in sync with the timezone.
    hooks.updateOffset = function () {};

    // MOMENTS

    // keepLocalTime = true means only change the timezone, without
    // affecting the local hour. So 5:31:26 +0300 --[utcOffset(2, true)]-->
    // 5:31:26 +0200 It is possible that 5:31:26 doesn't exist with offset
    // +0200, so we adjust the time as needed, to be valid.
    //
    // Keeping the time actually adds/subtracts (one hour)
    // from the actual represented time. That is why we call updateOffset
    // a second time. In case it wants us to change the offset again
    // _changeInProgress == true case, then we have to adjust, because
    // there is no such time in the given timezone.
    function getSetOffset (input, keepLocalTime, keepMinutes) {
        var offset = this._offset || 0,
            localAdjust;
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }
        if (input != null) {
            if (typeof input === 'string') {
                input = offsetFromString(matchShortOffset, input);
                if (input === null) {
                    return this;
                }
            } else if (Math.abs(input) < 16 && !keepMinutes) {
                input = input * 60;
            }
            if (!this._isUTC && keepLocalTime) {
                localAdjust = getDateOffset(this);
            }
            this._offset = input;
            this._isUTC = true;
            if (localAdjust != null) {
                this.add(localAdjust, 'm');
            }
            if (offset !== input) {
                if (!keepLocalTime || this._changeInProgress) {
                    addSubtract(this, createDuration(input - offset, 'm'), 1, false);
                } else if (!this._changeInProgress) {
                    this._changeInProgress = true;
                    hooks.updateOffset(this, true);
                    this._changeInProgress = null;
                }
            }
            return this;
        } else {
            return this._isUTC ? offset : getDateOffset(this);
        }
    }

    function getSetZone (input, keepLocalTime) {
        if (input != null) {
            if (typeof input !== 'string') {
                input = -input;
            }

            this.utcOffset(input, keepLocalTime);

            return this;
        } else {
            return -this.utcOffset();
        }
    }

    function setOffsetToUTC (keepLocalTime) {
        return this.utcOffset(0, keepLocalTime);
    }

    function setOffsetToLocal (keepLocalTime) {
        if (this._isUTC) {
            this.utcOffset(0, keepLocalTime);
            this._isUTC = false;

            if (keepLocalTime) {
                this.subtract(getDateOffset(this), 'm');
            }
        }
        return this;
    }

    function setOffsetToParsedOffset () {
        if (this._tzm != null) {
            this.utcOffset(this._tzm, false, true);
        } else if (typeof this._i === 'string') {
            var tZone = offsetFromString(matchOffset, this._i);
            if (tZone != null) {
                this.utcOffset(tZone);
            }
            else {
                this.utcOffset(0, true);
            }
        }
        return this;
    }

    function hasAlignedHourOffset (input) {
        if (!this.isValid()) {
            return false;
        }
        input = input ? createLocal(input).utcOffset() : 0;

        return (this.utcOffset() - input) % 60 === 0;
    }

    function isDaylightSavingTime () {
        return (
            this.utcOffset() > this.clone().month(0).utcOffset() ||
            this.utcOffset() > this.clone().month(5).utcOffset()
        );
    }

    function isDaylightSavingTimeShifted () {
        if (!isUndefined(this._isDSTShifted)) {
            return this._isDSTShifted;
        }

        var c = {};

        copyConfig(c, this);
        c = prepareConfig(c);

        if (c._a) {
            var other = c._isUTC ? createUTC(c._a) : createLocal(c._a);
            this._isDSTShifted = this.isValid() &&
                compareArrays(c._a, other.toArray()) > 0;
        } else {
            this._isDSTShifted = false;
        }

        return this._isDSTShifted;
    }

    function isLocal () {
        return this.isValid() ? !this._isUTC : false;
    }

    function isUtcOffset () {
        return this.isValid() ? this._isUTC : false;
    }

    function isUtc () {
        return this.isValid() ? this._isUTC && this._offset === 0 : false;
    }

    // ASP.NET json date format regex
    var aspNetRegex = /^(\-|\+)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)(\.\d*)?)?$/;

    // from http://docs.closure-library.googlecode.com/git/closure_goog_date_date.js.source.html
    // somewhat more in line with 4.4.3.2 2004 spec, but allows decimal anywhere
    // and further modified to allow for strings containing both week and day
    var isoRegex = /^(-|\+)?P(?:([-+]?[0-9,.]*)Y)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)W)?(?:([-+]?[0-9,.]*)D)?(?:T(?:([-+]?[0-9,.]*)H)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)S)?)?$/;

    function createDuration (input, key) {
        var duration = input,
            // matching against regexp is expensive, do it on demand
            match = null,
            sign,
            ret,
            diffRes;

        if (isDuration(input)) {
            duration = {
                ms : input._milliseconds,
                d  : input._days,
                M  : input._months
            };
        } else if (isNumber(input)) {
            duration = {};
            if (key) {
                duration[key] = input;
            } else {
                duration.milliseconds = input;
            }
        } else if (!!(match = aspNetRegex.exec(input))) {
            sign = (match[1] === '-') ? -1 : 1;
            duration = {
                y  : 0,
                d  : toInt(match[DATE])                         * sign,
                h  : toInt(match[HOUR])                         * sign,
                m  : toInt(match[MINUTE])                       * sign,
                s  : toInt(match[SECOND])                       * sign,
                ms : toInt(absRound(match[MILLISECOND] * 1000)) * sign // the millisecond decimal point is included in the match
            };
        } else if (!!(match = isoRegex.exec(input))) {
            sign = (match[1] === '-') ? -1 : (match[1] === '+') ? 1 : 1;
            duration = {
                y : parseIso(match[2], sign),
                M : parseIso(match[3], sign),
                w : parseIso(match[4], sign),
                d : parseIso(match[5], sign),
                h : parseIso(match[6], sign),
                m : parseIso(match[7], sign),
                s : parseIso(match[8], sign)
            };
        } else if (duration == null) {// checks for null or undefined
            duration = {};
        } else if (typeof duration === 'object' && ('from' in duration || 'to' in duration)) {
            diffRes = momentsDifference(createLocal(duration.from), createLocal(duration.to));

            duration = {};
            duration.ms = diffRes.milliseconds;
            duration.M = diffRes.months;
        }

        ret = new Duration(duration);

        if (isDuration(input) && hasOwnProp(input, '_locale')) {
            ret._locale = input._locale;
        }

        return ret;
    }

    createDuration.fn = Duration.prototype;
    createDuration.invalid = createInvalid$1;

    function parseIso (inp, sign) {
        // We'd normally use ~~inp for this, but unfortunately it also
        // converts floats to ints.
        // inp may be undefined, so careful calling replace on it.
        var res = inp && parseFloat(inp.replace(',', '.'));
        // apply sign while we're at it
        return (isNaN(res) ? 0 : res) * sign;
    }

    function positiveMomentsDifference(base, other) {
        var res = {milliseconds: 0, months: 0};

        res.months = other.month() - base.month() +
            (other.year() - base.year()) * 12;
        if (base.clone().add(res.months, 'M').isAfter(other)) {
            --res.months;
        }

        res.milliseconds = +other - +(base.clone().add(res.months, 'M'));

        return res;
    }

    function momentsDifference(base, other) {
        var res;
        if (!(base.isValid() && other.isValid())) {
            return {milliseconds: 0, months: 0};
        }

        other = cloneWithOffset(other, base);
        if (base.isBefore(other)) {
            res = positiveMomentsDifference(base, other);
        } else {
            res = positiveMomentsDifference(other, base);
            res.milliseconds = -res.milliseconds;
            res.months = -res.months;
        }

        return res;
    }

    // TODO: remove 'name' arg after deprecation is removed
    function createAdder(direction, name) {
        return function (val, period) {
            var dur, tmp;
            //invert the arguments, but complain about it
            if (period !== null && !isNaN(+period)) {
                deprecateSimple(name, 'moment().' + name  + '(period, number) is deprecated. Please use moment().' + name + '(number, period). ' +
                'See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info.');
                tmp = val; val = period; period = tmp;
            }

            val = typeof val === 'string' ? +val : val;
            dur = createDuration(val, period);
            addSubtract(this, dur, direction);
            return this;
        };
    }

    function addSubtract (mom, duration, isAdding, updateOffset) {
        var milliseconds = duration._milliseconds,
            days = absRound(duration._days),
            months = absRound(duration._months);

        if (!mom.isValid()) {
            // No op
            return;
        }

        updateOffset = updateOffset == null ? true : updateOffset;

        if (months) {
            setMonth(mom, get(mom, 'Month') + months * isAdding);
        }
        if (days) {
            set$1(mom, 'Date', get(mom, 'Date') + days * isAdding);
        }
        if (milliseconds) {
            mom._d.setTime(mom._d.valueOf() + milliseconds * isAdding);
        }
        if (updateOffset) {
            hooks.updateOffset(mom, days || months);
        }
    }

    var add      = createAdder(1, 'add');
    var subtract = createAdder(-1, 'subtract');

    function getCalendarFormat(myMoment, now) {
        var diff = myMoment.diff(now, 'days', true);
        return diff < -6 ? 'sameElse' :
                diff < -1 ? 'lastWeek' :
                diff < 0 ? 'lastDay' :
                diff < 1 ? 'sameDay' :
                diff < 2 ? 'nextDay' :
                diff < 7 ? 'nextWeek' : 'sameElse';
    }

    function calendar$1 (time, formats) {
        // We want to compare the start of today, vs this.
        // Getting start-of-today depends on whether we're local/utc/offset or not.
        var now = time || createLocal(),
            sod = cloneWithOffset(now, this).startOf('day'),
            format = hooks.calendarFormat(this, sod) || 'sameElse';

        var output = formats && (isFunction(formats[format]) ? formats[format].call(this, now) : formats[format]);

        return this.format(output || this.localeData().calendar(format, this, createLocal(now)));
    }

    function clone () {
        return new Moment(this);
    }

    function isAfter (input, units) {
        var localInput = isMoment(input) ? input : createLocal(input);
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
        if (units === 'millisecond') {
            return this.valueOf() > localInput.valueOf();
        } else {
            return localInput.valueOf() < this.clone().startOf(units).valueOf();
        }
    }

    function isBefore (input, units) {
        var localInput = isMoment(input) ? input : createLocal(input);
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
        if (units === 'millisecond') {
            return this.valueOf() < localInput.valueOf();
        } else {
            return this.clone().endOf(units).valueOf() < localInput.valueOf();
        }
    }

    function isBetween (from, to, units, inclusivity) {
        inclusivity = inclusivity || '()';
        return (inclusivity[0] === '(' ? this.isAfter(from, units) : !this.isBefore(from, units)) &&
            (inclusivity[1] === ')' ? this.isBefore(to, units) : !this.isAfter(to, units));
    }

    function isSame (input, units) {
        var localInput = isMoment(input) ? input : createLocal(input),
            inputMs;
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(units || 'millisecond');
        if (units === 'millisecond') {
            return this.valueOf() === localInput.valueOf();
        } else {
            inputMs = localInput.valueOf();
            return this.clone().startOf(units).valueOf() <= inputMs && inputMs <= this.clone().endOf(units).valueOf();
        }
    }

    function isSameOrAfter (input, units) {
        return this.isSame(input, units) || this.isAfter(input,units);
    }

    function isSameOrBefore (input, units) {
        return this.isSame(input, units) || this.isBefore(input,units);
    }

    function diff (input, units, asFloat) {
        var that,
            zoneDelta,
            output;

        if (!this.isValid()) {
            return NaN;
        }

        that = cloneWithOffset(input, this);

        if (!that.isValid()) {
            return NaN;
        }

        zoneDelta = (that.utcOffset() - this.utcOffset()) * 6e4;

        units = normalizeUnits(units);

        switch (units) {
            case 'year': output = monthDiff(this, that) / 12; break;
            case 'month': output = monthDiff(this, that); break;
            case 'quarter': output = monthDiff(this, that) / 3; break;
            case 'second': output = (this - that) / 1e3; break; // 1000
            case 'minute': output = (this - that) / 6e4; break; // 1000 * 60
            case 'hour': output = (this - that) / 36e5; break; // 1000 * 60 * 60
            case 'day': output = (this - that - zoneDelta) / 864e5; break; // 1000 * 60 * 60 * 24, negate dst
            case 'week': output = (this - that - zoneDelta) / 6048e5; break; // 1000 * 60 * 60 * 24 * 7, negate dst
            default: output = this - that;
        }

        return asFloat ? output : absFloor(output);
    }

    function monthDiff (a, b) {
        // difference in months
        var wholeMonthDiff = ((b.year() - a.year()) * 12) + (b.month() - a.month()),
            // b is in (anchor - 1 month, anchor + 1 month)
            anchor = a.clone().add(wholeMonthDiff, 'months'),
            anchor2, adjust;

        if (b - anchor < 0) {
            anchor2 = a.clone().add(wholeMonthDiff - 1, 'months');
            // linear across the month
            adjust = (b - anchor) / (anchor - anchor2);
        } else {
            anchor2 = a.clone().add(wholeMonthDiff + 1, 'months');
            // linear across the month
            adjust = (b - anchor) / (anchor2 - anchor);
        }

        //check for negative zero, return zero if negative zero
        return -(wholeMonthDiff + adjust) || 0;
    }

    hooks.defaultFormat = 'YYYY-MM-DDTHH:mm:ssZ';
    hooks.defaultFormatUtc = 'YYYY-MM-DDTHH:mm:ss[Z]';

    function toString () {
        return this.clone().locale('en').format('ddd MMM DD YYYY HH:mm:ss [GMT]ZZ');
    }

    function toISOString(keepOffset) {
        if (!this.isValid()) {
            return null;
        }
        var utc = keepOffset !== true;
        var m = utc ? this.clone().utc() : this;
        if (m.year() < 0 || m.year() > 9999) {
            return formatMoment(m, utc ? 'YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]' : 'YYYYYY-MM-DD[T]HH:mm:ss.SSSZ');
        }
        if (isFunction(Date.prototype.toISOString)) {
            // native implementation is ~50x faster, use it when we can
            if (utc) {
                return this.toDate().toISOString();
            } else {
                return new Date(this.valueOf() + this.utcOffset() * 60 * 1000).toISOString().replace('Z', formatMoment(m, 'Z'));
            }
        }
        return formatMoment(m, utc ? 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]' : 'YYYY-MM-DD[T]HH:mm:ss.SSSZ');
    }

    /**
     * Return a human readable representation of a moment that can
     * also be evaluated to get a new moment which is the same
     *
     * @link https://nodejs.org/dist/latest/docs/api/util.html#util_custom_inspect_function_on_objects
     */
    function inspect () {
        if (!this.isValid()) {
            return 'moment.invalid(/* ' + this._i + ' */)';
        }
        var func = 'moment';
        var zone = '';
        if (!this.isLocal()) {
            func = this.utcOffset() === 0 ? 'moment.utc' : 'moment.parseZone';
            zone = 'Z';
        }
        var prefix = '[' + func + '("]';
        var year = (0 <= this.year() && this.year() <= 9999) ? 'YYYY' : 'YYYYYY';
        var datetime = '-MM-DD[T]HH:mm:ss.SSS';
        var suffix = zone + '[")]';

        return this.format(prefix + year + datetime + suffix);
    }

    function format (inputString) {
        if (!inputString) {
            inputString = this.isUtc() ? hooks.defaultFormatUtc : hooks.defaultFormat;
        }
        var output = formatMoment(this, inputString);
        return this.localeData().postformat(output);
    }

    function from (time, withoutSuffix) {
        if (this.isValid() &&
                ((isMoment(time) && time.isValid()) ||
                 createLocal(time).isValid())) {
            return createDuration({to: this, from: time}).locale(this.locale()).humanize(!withoutSuffix);
        } else {
            return this.localeData().invalidDate();
        }
    }

    function fromNow (withoutSuffix) {
        return this.from(createLocal(), withoutSuffix);
    }

    function to (time, withoutSuffix) {
        if (this.isValid() &&
                ((isMoment(time) && time.isValid()) ||
                 createLocal(time).isValid())) {
            return createDuration({from: this, to: time}).locale(this.locale()).humanize(!withoutSuffix);
        } else {
            return this.localeData().invalidDate();
        }
    }

    function toNow (withoutSuffix) {
        return this.to(createLocal(), withoutSuffix);
    }

    // If passed a locale key, it will set the locale for this
    // instance.  Otherwise, it will return the locale configuration
    // variables for this instance.
    function locale (key) {
        var newLocaleData;

        if (key === undefined) {
            return this._locale._abbr;
        } else {
            newLocaleData = getLocale(key);
            if (newLocaleData != null) {
                this._locale = newLocaleData;
            }
            return this;
        }
    }

    var lang = deprecate(
        'moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.',
        function (key) {
            if (key === undefined) {
                return this.localeData();
            } else {
                return this.locale(key);
            }
        }
    );

    function localeData () {
        return this._locale;
    }

    function startOf (units) {
        units = normalizeUnits(units);
        // the following switch intentionally omits break keywords
        // to utilize falling through the cases.
        switch (units) {
            case 'year':
                this.month(0);
                /* falls through */
            case 'quarter':
            case 'month':
                this.date(1);
                /* falls through */
            case 'week':
            case 'isoWeek':
            case 'day':
            case 'date':
                this.hours(0);
                /* falls through */
            case 'hour':
                this.minutes(0);
                /* falls through */
            case 'minute':
                this.seconds(0);
                /* falls through */
            case 'second':
                this.milliseconds(0);
        }

        // weeks are a special case
        if (units === 'week') {
            this.weekday(0);
        }
        if (units === 'isoWeek') {
            this.isoWeekday(1);
        }

        // quarters are also special
        if (units === 'quarter') {
            this.month(Math.floor(this.month() / 3) * 3);
        }

        return this;
    }

    function endOf (units) {
        units = normalizeUnits(units);
        if (units === undefined || units === 'millisecond') {
            return this;
        }

        // 'date' is an alias for 'day', so it should be considered as such.
        if (units === 'date') {
            units = 'day';
        }

        return this.startOf(units).add(1, (units === 'isoWeek' ? 'week' : units)).subtract(1, 'ms');
    }

    function valueOf () {
        return this._d.valueOf() - ((this._offset || 0) * 60000);
    }

    function unix () {
        return Math.floor(this.valueOf() / 1000);
    }

    function toDate () {
        return new Date(this.valueOf());
    }

    function toArray () {
        var m = this;
        return [m.year(), m.month(), m.date(), m.hour(), m.minute(), m.second(), m.millisecond()];
    }

    function toObject () {
        var m = this;
        return {
            years: m.year(),
            months: m.month(),
            date: m.date(),
            hours: m.hours(),
            minutes: m.minutes(),
            seconds: m.seconds(),
            milliseconds: m.milliseconds()
        };
    }

    function toJSON () {
        // new Date(NaN).toJSON() === null
        return this.isValid() ? this.toISOString() : null;
    }

    function isValid$2 () {
        return isValid(this);
    }

    function parsingFlags () {
        return extend({}, getParsingFlags(this));
    }

    function invalidAt () {
        return getParsingFlags(this).overflow;
    }

    function creationData() {
        return {
            input: this._i,
            format: this._f,
            locale: this._locale,
            isUTC: this._isUTC,
            strict: this._strict
        };
    }

    // FORMATTING

    addFormatToken(0, ['gg', 2], 0, function () {
        return this.weekYear() % 100;
    });

    addFormatToken(0, ['GG', 2], 0, function () {
        return this.isoWeekYear() % 100;
    });

    function addWeekYearFormatToken (token, getter) {
        addFormatToken(0, [token, token.length], 0, getter);
    }

    addWeekYearFormatToken('gggg',     'weekYear');
    addWeekYearFormatToken('ggggg',    'weekYear');
    addWeekYearFormatToken('GGGG',  'isoWeekYear');
    addWeekYearFormatToken('GGGGG', 'isoWeekYear');

    // ALIASES

    addUnitAlias('weekYear', 'gg');
    addUnitAlias('isoWeekYear', 'GG');

    // PRIORITY

    addUnitPriority('weekYear', 1);
    addUnitPriority('isoWeekYear', 1);


    // PARSING

    addRegexToken('G',      matchSigned);
    addRegexToken('g',      matchSigned);
    addRegexToken('GG',     match1to2, match2);
    addRegexToken('gg',     match1to2, match2);
    addRegexToken('GGGG',   match1to4, match4);
    addRegexToken('gggg',   match1to4, match4);
    addRegexToken('GGGGG',  match1to6, match6);
    addRegexToken('ggggg',  match1to6, match6);

    addWeekParseToken(['gggg', 'ggggg', 'GGGG', 'GGGGG'], function (input, week, config, token) {
        week[token.substr(0, 2)] = toInt(input);
    });

    addWeekParseToken(['gg', 'GG'], function (input, week, config, token) {
        week[token] = hooks.parseTwoDigitYear(input);
    });

    // MOMENTS

    function getSetWeekYear (input) {
        return getSetWeekYearHelper.call(this,
                input,
                this.week(),
                this.weekday(),
                this.localeData()._week.dow,
                this.localeData()._week.doy);
    }

    function getSetISOWeekYear (input) {
        return getSetWeekYearHelper.call(this,
                input, this.isoWeek(), this.isoWeekday(), 1, 4);
    }

    function getISOWeeksInYear () {
        return weeksInYear(this.year(), 1, 4);
    }

    function getWeeksInYear () {
        var weekInfo = this.localeData()._week;
        return weeksInYear(this.year(), weekInfo.dow, weekInfo.doy);
    }

    function getSetWeekYearHelper(input, week, weekday, dow, doy) {
        var weeksTarget;
        if (input == null) {
            return weekOfYear(this, dow, doy).year;
        } else {
            weeksTarget = weeksInYear(input, dow, doy);
            if (week > weeksTarget) {
                week = weeksTarget;
            }
            return setWeekAll.call(this, input, week, weekday, dow, doy);
        }
    }

    function setWeekAll(weekYear, week, weekday, dow, doy) {
        var dayOfYearData = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy),
            date = createUTCDate(dayOfYearData.year, 0, dayOfYearData.dayOfYear);

        this.year(date.getUTCFullYear());
        this.month(date.getUTCMonth());
        this.date(date.getUTCDate());
        return this;
    }

    // FORMATTING

    addFormatToken('Q', 0, 'Qo', 'quarter');

    // ALIASES

    addUnitAlias('quarter', 'Q');

    // PRIORITY

    addUnitPriority('quarter', 7);

    // PARSING

    addRegexToken('Q', match1);
    addParseToken('Q', function (input, array) {
        array[MONTH] = (toInt(input) - 1) * 3;
    });

    // MOMENTS

    function getSetQuarter (input) {
        return input == null ? Math.ceil((this.month() + 1) / 3) : this.month((input - 1) * 3 + this.month() % 3);
    }

    // FORMATTING

    addFormatToken('D', ['DD', 2], 'Do', 'date');

    // ALIASES

    addUnitAlias('date', 'D');

    // PRIORITY
    addUnitPriority('date', 9);

    // PARSING

    addRegexToken('D',  match1to2);
    addRegexToken('DD', match1to2, match2);
    addRegexToken('Do', function (isStrict, locale) {
        // TODO: Remove "ordinalParse" fallback in next major release.
        return isStrict ?
          (locale._dayOfMonthOrdinalParse || locale._ordinalParse) :
          locale._dayOfMonthOrdinalParseLenient;
    });

    addParseToken(['D', 'DD'], DATE);
    addParseToken('Do', function (input, array) {
        array[DATE] = toInt(input.match(match1to2)[0]);
    });

    // MOMENTS

    var getSetDayOfMonth = makeGetSet('Date', true);

    // FORMATTING

    addFormatToken('DDD', ['DDDD', 3], 'DDDo', 'dayOfYear');

    // ALIASES

    addUnitAlias('dayOfYear', 'DDD');

    // PRIORITY
    addUnitPriority('dayOfYear', 4);

    // PARSING

    addRegexToken('DDD',  match1to3);
    addRegexToken('DDDD', match3);
    addParseToken(['DDD', 'DDDD'], function (input, array, config) {
        config._dayOfYear = toInt(input);
    });

    // HELPERS

    // MOMENTS

    function getSetDayOfYear (input) {
        var dayOfYear = Math.round((this.clone().startOf('day') - this.clone().startOf('year')) / 864e5) + 1;
        return input == null ? dayOfYear : this.add((input - dayOfYear), 'd');
    }

    // FORMATTING

    addFormatToken('m', ['mm', 2], 0, 'minute');

    // ALIASES

    addUnitAlias('minute', 'm');

    // PRIORITY

    addUnitPriority('minute', 14);

    // PARSING

    addRegexToken('m',  match1to2);
    addRegexToken('mm', match1to2, match2);
    addParseToken(['m', 'mm'], MINUTE);

    // MOMENTS

    var getSetMinute = makeGetSet('Minutes', false);

    // FORMATTING

    addFormatToken('s', ['ss', 2], 0, 'second');

    // ALIASES

    addUnitAlias('second', 's');

    // PRIORITY

    addUnitPriority('second', 15);

    // PARSING

    addRegexToken('s',  match1to2);
    addRegexToken('ss', match1to2, match2);
    addParseToken(['s', 'ss'], SECOND);

    // MOMENTS

    var getSetSecond = makeGetSet('Seconds', false);

    // FORMATTING

    addFormatToken('S', 0, 0, function () {
        return ~~(this.millisecond() / 100);
    });

    addFormatToken(0, ['SS', 2], 0, function () {
        return ~~(this.millisecond() / 10);
    });

    addFormatToken(0, ['SSS', 3], 0, 'millisecond');
    addFormatToken(0, ['SSSS', 4], 0, function () {
        return this.millisecond() * 10;
    });
    addFormatToken(0, ['SSSSS', 5], 0, function () {
        return this.millisecond() * 100;
    });
    addFormatToken(0, ['SSSSSS', 6], 0, function () {
        return this.millisecond() * 1000;
    });
    addFormatToken(0, ['SSSSSSS', 7], 0, function () {
        return this.millisecond() * 10000;
    });
    addFormatToken(0, ['SSSSSSSS', 8], 0, function () {
        return this.millisecond() * 100000;
    });
    addFormatToken(0, ['SSSSSSSSS', 9], 0, function () {
        return this.millisecond() * 1000000;
    });


    // ALIASES

    addUnitAlias('millisecond', 'ms');

    // PRIORITY

    addUnitPriority('millisecond', 16);

    // PARSING

    addRegexToken('S',    match1to3, match1);
    addRegexToken('SS',   match1to3, match2);
    addRegexToken('SSS',  match1to3, match3);

    var token;
    for (token = 'SSSS'; token.length <= 9; token += 'S') {
        addRegexToken(token, matchUnsigned);
    }

    function parseMs(input, array) {
        array[MILLISECOND] = toInt(('0.' + input) * 1000);
    }

    for (token = 'S'; token.length <= 9; token += 'S') {
        addParseToken(token, parseMs);
    }
    // MOMENTS

    var getSetMillisecond = makeGetSet('Milliseconds', false);

    // FORMATTING

    addFormatToken('z',  0, 0, 'zoneAbbr');
    addFormatToken('zz', 0, 0, 'zoneName');

    // MOMENTS

    function getZoneAbbr () {
        return this._isUTC ? 'UTC' : '';
    }

    function getZoneName () {
        return this._isUTC ? 'Coordinated Universal Time' : '';
    }

    var proto = Moment.prototype;

    proto.add               = add;
    proto.calendar          = calendar$1;
    proto.clone             = clone;
    proto.diff              = diff;
    proto.endOf             = endOf;
    proto.format            = format;
    proto.from              = from;
    proto.fromNow           = fromNow;
    proto.to                = to;
    proto.toNow             = toNow;
    proto.get               = stringGet;
    proto.invalidAt         = invalidAt;
    proto.isAfter           = isAfter;
    proto.isBefore          = isBefore;
    proto.isBetween         = isBetween;
    proto.isSame            = isSame;
    proto.isSameOrAfter     = isSameOrAfter;
    proto.isSameOrBefore    = isSameOrBefore;
    proto.isValid           = isValid$2;
    proto.lang              = lang;
    proto.locale            = locale;
    proto.localeData        = localeData;
    proto.max               = prototypeMax;
    proto.min               = prototypeMin;
    proto.parsingFlags      = parsingFlags;
    proto.set               = stringSet;
    proto.startOf           = startOf;
    proto.subtract          = subtract;
    proto.toArray           = toArray;
    proto.toObject          = toObject;
    proto.toDate            = toDate;
    proto.toISOString       = toISOString;
    proto.inspect           = inspect;
    proto.toJSON            = toJSON;
    proto.toString          = toString;
    proto.unix              = unix;
    proto.valueOf           = valueOf;
    proto.creationData      = creationData;
    proto.year       = getSetYear;
    proto.isLeapYear = getIsLeapYear;
    proto.weekYear    = getSetWeekYear;
    proto.isoWeekYear = getSetISOWeekYear;
    proto.quarter = proto.quarters = getSetQuarter;
    proto.month       = getSetMonth;
    proto.daysInMonth = getDaysInMonth;
    proto.week           = proto.weeks        = getSetWeek;
    proto.isoWeek        = proto.isoWeeks     = getSetISOWeek;
    proto.weeksInYear    = getWeeksInYear;
    proto.isoWeeksInYear = getISOWeeksInYear;
    proto.date       = getSetDayOfMonth;
    proto.day        = proto.days             = getSetDayOfWeek;
    proto.weekday    = getSetLocaleDayOfWeek;
    proto.isoWeekday = getSetISODayOfWeek;
    proto.dayOfYear  = getSetDayOfYear;
    proto.hour = proto.hours = getSetHour;
    proto.minute = proto.minutes = getSetMinute;
    proto.second = proto.seconds = getSetSecond;
    proto.millisecond = proto.milliseconds = getSetMillisecond;
    proto.utcOffset            = getSetOffset;
    proto.utc                  = setOffsetToUTC;
    proto.local                = setOffsetToLocal;
    proto.parseZone            = setOffsetToParsedOffset;
    proto.hasAlignedHourOffset = hasAlignedHourOffset;
    proto.isDST                = isDaylightSavingTime;
    proto.isLocal              = isLocal;
    proto.isUtcOffset          = isUtcOffset;
    proto.isUtc                = isUtc;
    proto.isUTC                = isUtc;
    proto.zoneAbbr = getZoneAbbr;
    proto.zoneName = getZoneName;
    proto.dates  = deprecate('dates accessor is deprecated. Use date instead.', getSetDayOfMonth);
    proto.months = deprecate('months accessor is deprecated. Use month instead', getSetMonth);
    proto.years  = deprecate('years accessor is deprecated. Use year instead', getSetYear);
    proto.zone   = deprecate('moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/', getSetZone);
    proto.isDSTShifted = deprecate('isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information', isDaylightSavingTimeShifted);

    function createUnix (input) {
        return createLocal(input * 1000);
    }

    function createInZone () {
        return createLocal.apply(null, arguments).parseZone();
    }

    function preParsePostFormat (string) {
        return string;
    }

    var proto$1 = Locale.prototype;

    proto$1.calendar        = calendar;
    proto$1.longDateFormat  = longDateFormat;
    proto$1.invalidDate     = invalidDate;
    proto$1.ordinal         = ordinal;
    proto$1.preparse        = preParsePostFormat;
    proto$1.postformat      = preParsePostFormat;
    proto$1.relativeTime    = relativeTime;
    proto$1.pastFuture      = pastFuture;
    proto$1.set             = set;

    proto$1.months            =        localeMonths;
    proto$1.monthsShort       =        localeMonthsShort;
    proto$1.monthsParse       =        localeMonthsParse;
    proto$1.monthsRegex       = monthsRegex;
    proto$1.monthsShortRegex  = monthsShortRegex;
    proto$1.week = localeWeek;
    proto$1.firstDayOfYear = localeFirstDayOfYear;
    proto$1.firstDayOfWeek = localeFirstDayOfWeek;

    proto$1.weekdays       =        localeWeekdays;
    proto$1.weekdaysMin    =        localeWeekdaysMin;
    proto$1.weekdaysShort  =        localeWeekdaysShort;
    proto$1.weekdaysParse  =        localeWeekdaysParse;

    proto$1.weekdaysRegex       =        weekdaysRegex;
    proto$1.weekdaysShortRegex  =        weekdaysShortRegex;
    proto$1.weekdaysMinRegex    =        weekdaysMinRegex;

    proto$1.isPM = localeIsPM;
    proto$1.meridiem = localeMeridiem;

    function get$1 (format, index, field, setter) {
        var locale = getLocale();
        var utc = createUTC().set(setter, index);
        return locale[field](utc, format);
    }

    function listMonthsImpl (format, index, field) {
        if (isNumber(format)) {
            index = format;
            format = undefined;
        }

        format = format || '';

        if (index != null) {
            return get$1(format, index, field, 'month');
        }

        var i;
        var out = [];
        for (i = 0; i < 12; i++) {
            out[i] = get$1(format, i, field, 'month');
        }
        return out;
    }

    // ()
    // (5)
    // (fmt, 5)
    // (fmt)
    // (true)
    // (true, 5)
    // (true, fmt, 5)
    // (true, fmt)
    function listWeekdaysImpl (localeSorted, format, index, field) {
        if (typeof localeSorted === 'boolean') {
            if (isNumber(format)) {
                index = format;
                format = undefined;
            }

            format = format || '';
        } else {
            format = localeSorted;
            index = format;
            localeSorted = false;

            if (isNumber(format)) {
                index = format;
                format = undefined;
            }

            format = format || '';
        }

        var locale = getLocale(),
            shift = localeSorted ? locale._week.dow : 0;

        if (index != null) {
            return get$1(format, (index + shift) % 7, field, 'day');
        }

        var i;
        var out = [];
        for (i = 0; i < 7; i++) {
            out[i] = get$1(format, (i + shift) % 7, field, 'day');
        }
        return out;
    }

    function listMonths (format, index) {
        return listMonthsImpl(format, index, 'months');
    }

    function listMonthsShort (format, index) {
        return listMonthsImpl(format, index, 'monthsShort');
    }

    function listWeekdays (localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdays');
    }

    function listWeekdaysShort (localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdaysShort');
    }

    function listWeekdaysMin (localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdaysMin');
    }

    getSetGlobalLocale('en', {
        dayOfMonthOrdinalParse: /\d{1,2}(th|st|nd|rd)/,
        ordinal : function (number) {
            var b = number % 10,
                output = (toInt(number % 100 / 10) === 1) ? 'th' :
                (b === 1) ? 'st' :
                (b === 2) ? 'nd' :
                (b === 3) ? 'rd' : 'th';
            return number + output;
        }
    });

    // Side effect imports

    hooks.lang = deprecate('moment.lang is deprecated. Use moment.locale instead.', getSetGlobalLocale);
    hooks.langData = deprecate('moment.langData is deprecated. Use moment.localeData instead.', getLocale);

    var mathAbs = Math.abs;

    function abs () {
        var data           = this._data;

        this._milliseconds = mathAbs(this._milliseconds);
        this._days         = mathAbs(this._days);
        this._months       = mathAbs(this._months);

        data.milliseconds  = mathAbs(data.milliseconds);
        data.seconds       = mathAbs(data.seconds);
        data.minutes       = mathAbs(data.minutes);
        data.hours         = mathAbs(data.hours);
        data.months        = mathAbs(data.months);
        data.years         = mathAbs(data.years);

        return this;
    }

    function addSubtract$1 (duration, input, value, direction) {
        var other = createDuration(input, value);

        duration._milliseconds += direction * other._milliseconds;
        duration._days         += direction * other._days;
        duration._months       += direction * other._months;

        return duration._bubble();
    }

    // supports only 2.0-style add(1, 's') or add(duration)
    function add$1 (input, value) {
        return addSubtract$1(this, input, value, 1);
    }

    // supports only 2.0-style subtract(1, 's') or subtract(duration)
    function subtract$1 (input, value) {
        return addSubtract$1(this, input, value, -1);
    }

    function absCeil (number) {
        if (number < 0) {
            return Math.floor(number);
        } else {
            return Math.ceil(number);
        }
    }

    function bubble () {
        var milliseconds = this._milliseconds;
        var days         = this._days;
        var months       = this._months;
        var data         = this._data;
        var seconds, minutes, hours, years, monthsFromDays;

        // if we have a mix of positive and negative values, bubble down first
        // check: https://github.com/moment/moment/issues/2166
        if (!((milliseconds >= 0 && days >= 0 && months >= 0) ||
                (milliseconds <= 0 && days <= 0 && months <= 0))) {
            milliseconds += absCeil(monthsToDays(months) + days) * 864e5;
            days = 0;
            months = 0;
        }

        // The following code bubbles up values, see the tests for
        // examples of what that means.
        data.milliseconds = milliseconds % 1000;

        seconds           = absFloor(milliseconds / 1000);
        data.seconds      = seconds % 60;

        minutes           = absFloor(seconds / 60);
        data.minutes      = minutes % 60;

        hours             = absFloor(minutes / 60);
        data.hours        = hours % 24;

        days += absFloor(hours / 24);

        // convert days to months
        monthsFromDays = absFloor(daysToMonths(days));
        months += monthsFromDays;
        days -= absCeil(monthsToDays(monthsFromDays));

        // 12 months -> 1 year
        years = absFloor(months / 12);
        months %= 12;

        data.days   = days;
        data.months = months;
        data.years  = years;

        return this;
    }

    function daysToMonths (days) {
        // 400 years have 146097 days (taking into account leap year rules)
        // 400 years have 12 months === 4800
        return days * 4800 / 146097;
    }

    function monthsToDays (months) {
        // the reverse of daysToMonths
        return months * 146097 / 4800;
    }

    function as (units) {
        if (!this.isValid()) {
            return NaN;
        }
        var days;
        var months;
        var milliseconds = this._milliseconds;

        units = normalizeUnits(units);

        if (units === 'month' || units === 'year') {
            days   = this._days   + milliseconds / 864e5;
            months = this._months + daysToMonths(days);
            return units === 'month' ? months : months / 12;
        } else {
            // handle milliseconds separately because of floating point math errors (issue #1867)
            days = this._days + Math.round(monthsToDays(this._months));
            switch (units) {
                case 'week'   : return days / 7     + milliseconds / 6048e5;
                case 'day'    : return days         + milliseconds / 864e5;
                case 'hour'   : return days * 24    + milliseconds / 36e5;
                case 'minute' : return days * 1440  + milliseconds / 6e4;
                case 'second' : return days * 86400 + milliseconds / 1000;
                // Math.floor prevents floating point math errors here
                case 'millisecond': return Math.floor(days * 864e5) + milliseconds;
                default: throw new Error('Unknown unit ' + units);
            }
        }
    }

    // TODO: Use this.as('ms')?
    function valueOf$1 () {
        if (!this.isValid()) {
            return NaN;
        }
        return (
            this._milliseconds +
            this._days * 864e5 +
            (this._months % 12) * 2592e6 +
            toInt(this._months / 12) * 31536e6
        );
    }

    function makeAs (alias) {
        return function () {
            return this.as(alias);
        };
    }

    var asMilliseconds = makeAs('ms');
    var asSeconds      = makeAs('s');
    var asMinutes      = makeAs('m');
    var asHours        = makeAs('h');
    var asDays         = makeAs('d');
    var asWeeks        = makeAs('w');
    var asMonths       = makeAs('M');
    var asYears        = makeAs('y');

    function clone$1 () {
        return createDuration(this);
    }

    function get$2 (units) {
        units = normalizeUnits(units);
        return this.isValid() ? this[units + 's']() : NaN;
    }

    function makeGetter(name) {
        return function () {
            return this.isValid() ? this._data[name] : NaN;
        };
    }

    var milliseconds = makeGetter('milliseconds');
    var seconds      = makeGetter('seconds');
    var minutes      = makeGetter('minutes');
    var hours        = makeGetter('hours');
    var days         = makeGetter('days');
    var months       = makeGetter('months');
    var years        = makeGetter('years');

    function weeks () {
        return absFloor(this.days() / 7);
    }

    var round = Math.round;
    var thresholds = {
        ss: 44,         // a few seconds to seconds
        s : 45,         // seconds to minute
        m : 45,         // minutes to hour
        h : 22,         // hours to day
        d : 26,         // days to month
        M : 11          // months to year
    };

    // helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize
    function substituteTimeAgo(string, number, withoutSuffix, isFuture, locale) {
        return locale.relativeTime(number || 1, !!withoutSuffix, string, isFuture);
    }

    function relativeTime$1 (posNegDuration, withoutSuffix, locale) {
        var duration = createDuration(posNegDuration).abs();
        var seconds  = round(duration.as('s'));
        var minutes  = round(duration.as('m'));
        var hours    = round(duration.as('h'));
        var days     = round(duration.as('d'));
        var months   = round(duration.as('M'));
        var years    = round(duration.as('y'));

        var a = seconds <= thresholds.ss && ['s', seconds]  ||
                seconds < thresholds.s   && ['ss', seconds] ||
                minutes <= 1             && ['m']           ||
                minutes < thresholds.m   && ['mm', minutes] ||
                hours   <= 1             && ['h']           ||
                hours   < thresholds.h   && ['hh', hours]   ||
                days    <= 1             && ['d']           ||
                days    < thresholds.d   && ['dd', days]    ||
                months  <= 1             && ['M']           ||
                months  < thresholds.M   && ['MM', months]  ||
                years   <= 1             && ['y']           || ['yy', years];

        a[2] = withoutSuffix;
        a[3] = +posNegDuration > 0;
        a[4] = locale;
        return substituteTimeAgo.apply(null, a);
    }

    // This function allows you to set the rounding function for relative time strings
    function getSetRelativeTimeRounding (roundingFunction) {
        if (roundingFunction === undefined) {
            return round;
        }
        if (typeof(roundingFunction) === 'function') {
            round = roundingFunction;
            return true;
        }
        return false;
    }

    // This function allows you to set a threshold for relative time strings
    function getSetRelativeTimeThreshold (threshold, limit) {
        if (thresholds[threshold] === undefined) {
            return false;
        }
        if (limit === undefined) {
            return thresholds[threshold];
        }
        thresholds[threshold] = limit;
        if (threshold === 's') {
            thresholds.ss = limit - 1;
        }
        return true;
    }

    function humanize (withSuffix) {
        if (!this.isValid()) {
            return this.localeData().invalidDate();
        }

        var locale = this.localeData();
        var output = relativeTime$1(this, !withSuffix, locale);

        if (withSuffix) {
            output = locale.pastFuture(+this, output);
        }

        return locale.postformat(output);
    }

    var abs$1 = Math.abs;

    function sign(x) {
        return ((x > 0) - (x < 0)) || +x;
    }

    function toISOString$1() {
        // for ISO strings we do not use the normal bubbling rules:
        //  * milliseconds bubble up until they become hours
        //  * days do not bubble at all
        //  * months bubble up until they become years
        // This is because there is no context-free conversion between hours and days
        // (think of clock changes)
        // and also not between days and months (28-31 days per month)
        if (!this.isValid()) {
            return this.localeData().invalidDate();
        }

        var seconds = abs$1(this._milliseconds) / 1000;
        var days         = abs$1(this._days);
        var months       = abs$1(this._months);
        var minutes, hours, years;

        // 3600 seconds -> 60 minutes -> 1 hour
        minutes           = absFloor(seconds / 60);
        hours             = absFloor(minutes / 60);
        seconds %= 60;
        minutes %= 60;

        // 12 months -> 1 year
        years  = absFloor(months / 12);
        months %= 12;


        // inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js
        var Y = years;
        var M = months;
        var D = days;
        var h = hours;
        var m = minutes;
        var s = seconds ? seconds.toFixed(3).replace(/\.?0+$/, '') : '';
        var total = this.asSeconds();

        if (!total) {
            // this is the same as C#'s (Noda) and python (isodate)...
            // but not other JS (goog.date)
            return 'P0D';
        }

        var totalSign = total < 0 ? '-' : '';
        var ymSign = sign(this._months) !== sign(total) ? '-' : '';
        var daysSign = sign(this._days) !== sign(total) ? '-' : '';
        var hmsSign = sign(this._milliseconds) !== sign(total) ? '-' : '';

        return totalSign + 'P' +
            (Y ? ymSign + Y + 'Y' : '') +
            (M ? ymSign + M + 'M' : '') +
            (D ? daysSign + D + 'D' : '') +
            ((h || m || s) ? 'T' : '') +
            (h ? hmsSign + h + 'H' : '') +
            (m ? hmsSign + m + 'M' : '') +
            (s ? hmsSign + s + 'S' : '');
    }

    var proto$2 = Duration.prototype;

    proto$2.isValid        = isValid$1;
    proto$2.abs            = abs;
    proto$2.add            = add$1;
    proto$2.subtract       = subtract$1;
    proto$2.as             = as;
    proto$2.asMilliseconds = asMilliseconds;
    proto$2.asSeconds      = asSeconds;
    proto$2.asMinutes      = asMinutes;
    proto$2.asHours        = asHours;
    proto$2.asDays         = asDays;
    proto$2.asWeeks        = asWeeks;
    proto$2.asMonths       = asMonths;
    proto$2.asYears        = asYears;
    proto$2.valueOf        = valueOf$1;
    proto$2._bubble        = bubble;
    proto$2.clone          = clone$1;
    proto$2.get            = get$2;
    proto$2.milliseconds   = milliseconds;
    proto$2.seconds        = seconds;
    proto$2.minutes        = minutes;
    proto$2.hours          = hours;
    proto$2.days           = days;
    proto$2.weeks          = weeks;
    proto$2.months         = months;
    proto$2.years          = years;
    proto$2.humanize       = humanize;
    proto$2.toISOString    = toISOString$1;
    proto$2.toString       = toISOString$1;
    proto$2.toJSON         = toISOString$1;
    proto$2.locale         = locale;
    proto$2.localeData     = localeData;

    proto$2.toIsoString = deprecate('toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)', toISOString$1);
    proto$2.lang = lang;

    // Side effect imports

    // FORMATTING

    addFormatToken('X', 0, 0, 'unix');
    addFormatToken('x', 0, 0, 'valueOf');

    // PARSING

    addRegexToken('x', matchSigned);
    addRegexToken('X', matchTimestamp);
    addParseToken('X', function (input, array, config) {
        config._d = new Date(parseFloat(input, 10) * 1000);
    });
    addParseToken('x', function (input, array, config) {
        config._d = new Date(toInt(input));
    });

    // Side effect imports


    hooks.version = '2.22.2';

    setHookCallback(createLocal);

    hooks.fn                    = proto;
    hooks.min                   = min;
    hooks.max                   = max;
    hooks.now                   = now;
    hooks.utc                   = createUTC;
    hooks.unix                  = createUnix;
    hooks.months                = listMonths;
    hooks.isDate                = isDate;
    hooks.locale                = getSetGlobalLocale;
    hooks.invalid               = createInvalid;
    hooks.duration              = createDuration;
    hooks.isMoment              = isMoment;
    hooks.weekdays              = listWeekdays;
    hooks.parseZone             = createInZone;
    hooks.localeData            = getLocale;
    hooks.isDuration            = isDuration;
    hooks.monthsShort           = listMonthsShort;
    hooks.weekdaysMin           = listWeekdaysMin;
    hooks.defineLocale          = defineLocale;
    hooks.updateLocale          = updateLocale;
    hooks.locales               = listLocales;
    hooks.weekdaysShort         = listWeekdaysShort;
    hooks.normalizeUnits        = normalizeUnits;
    hooks.relativeTimeRounding  = getSetRelativeTimeRounding;
    hooks.relativeTimeThreshold = getSetRelativeTimeThreshold;
    hooks.calendarFormat        = getCalendarFormat;
    hooks.prototype             = proto;

    // currently HTML5 input type only supports 24-hour formats
    hooks.HTML5_FMT = {
        DATETIME_LOCAL: 'YYYY-MM-DDTHH:mm',             // <input type="datetime-local" />
        DATETIME_LOCAL_SECONDS: 'YYYY-MM-DDTHH:mm:ss',  // <input type="datetime-local" step="1" />
        DATETIME_LOCAL_MS: 'YYYY-MM-DDTHH:mm:ss.SSS',   // <input type="datetime-local" step="0.001" />
        DATE: 'YYYY-MM-DD',                             // <input type="date" />
        TIME: 'HH:mm',                                  // <input type="time" />
        TIME_SECONDS: 'HH:mm:ss',                       // <input type="time" step="1" />
        TIME_MS: 'HH:mm:ss.SSS',                        // <input type="time" step="0.001" />
        WEEK: 'YYYY-[W]WW',                             // <input type="week" />
        MONTH: 'YYYY-MM'                                // <input type="month" />
    };

    return hooks;

})));

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("62e4")(module)))

/***/ }),

/***/ "c367":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var addToUnscopables = __webpack_require__("8436");
var step = __webpack_require__("50ed");
var Iterators = __webpack_require__("481b");
var toIObject = __webpack_require__("36c3");

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__("30f1")(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');


/***/ }),

/***/ "c3a1":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__("e6f3");
var enumBugKeys = __webpack_require__("1691");

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),

/***/ "c602":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "@media (max-width:500px){body{font-size:14px}.room-card{padding-top:15px;padding-bottom:15px}.room-card .title{padding-top:5px!important;margin-bottom:15px!important}.room-card .card-carousel,.room-card .title{padding-left:15px;padding-right:15px}.room-header .subtext{margin-bottom:30px!important}}.room-card{background-color:#fff;-webkit-box-shadow:rgba(0,0,0,.2) 0 1px 3px 0,rgba(0,0,0,.14) 0 1px 1px 0,rgba(0,0,0,.12) 0 2px 1px -1px;box-shadow:0 1px 3px 0 rgba(0,0,0,.2),0 1px 1px 0 rgba(0,0,0,.14),0 2px 1px -1px rgba(0,0,0,.12);border-radius:5px;margin-bottom:50px}.room-card .title{font-family:sans-serif;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;font-size:1.2em;color:#000;margin-bottom:20px}.hash-tags{font-size:1em;color:#000;margin:15px 0;padding-left:15px;padding-right:15px}.room-card .about-room .text{color:#787878;font-size:14px;padding-left:15px;padding-right:15px}.room-card .tags .tag{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;padding-top:7px;padding-bottom:7px;border-bottom:1px solid #dcdcdc}.room-card .tags .tag .tag-name,.room-card .tags .tag .tag-price{font-size:.83em}.room-card .tags .tag .tag-name .color{width:12px;height:12px;background-color:red;display:inline-block;border-radius:2px;margin-right:5px}.room-header .subtext{color:#626364;margin-bottom:36px;font-size:.77em}.room-header .heading{font-size:1.38em}.all-products-link{color:#f74861;text-align:center;padding-top:15px;margin-bottom:0;border-top:1px solid #dcdcdc}.colorlegend{width:16px;height:16px;border-radius:4px;margin-right:14px}.name{text-transform:capitalize;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;font-size:13px}@media(min-width:500px){.room-card{padding-top:15px;padding-bottom:15px}.room-card .card-carousel,.room-card .title{padding-left:15px;padding-right:15px}}", ""]);

// exports


/***/ }),

/***/ "ce75":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("9f91");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("2fb2").default
var update = add("1ea7227e", content, true, {});

/***/ }),

/***/ "d864":
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__("79aa");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "d9f6":
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__("e4ae");
var IE8_DOM_DEFINE = __webpack_require__("794b");
var toPrimitive = __webpack_require__("1bc3");
var dP = Object.defineProperty;

exports.f = __webpack_require__("8e60") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "dbdb":
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__("584a");
var global = __webpack_require__("e53d");
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});

(module.exports = function (key, value) {
  return store[key] || (store[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: core.version,
  mode: __webpack_require__("b8e3") ? 'pure' : 'global',
  copyright: '© 2018 Denis Pushkarev (zloirock.ru)'
});


/***/ }),

/***/ "df74":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, ".cost-breakout .nav-tabs{border:0;padding:15px 0 30px;z-index:1000}.cost-breakout .tab-content{height:auto!important;overflow:hidden!important}.cost-breakout .nav-tabs .nav-item{margin:0!important;display:inline-block}.cost-breakout .nav-tabs .nav-item .nav-link{margin:0!important;font-weight:400;padding:5px 15px!important;background-color:#f4f5f7}.cost-breakout .nav-tabs .nav-item .nav-link.active{border:0;background-color:#29475d;color:#fff}.cost-breakout .nav-tabs .nav-item:first-child .nav-link{border-top-right-radius:0;border-bottom-right-radius:0;border-top-left-radius:20px;border-bottom-left-radius:20px}.cost-breakout .nav-tabs .nav-item:last-child .nav-link{border-top-left-radius:0;border-top-right-radius:20px;border-bottom-left-radius:0;border-bottom-right-radius:20px}.cost-breakout{background-color:#fff;border-radius:5px;-webkit-box-shadow:rgba(0,0,0,.2) 0 1px 3px 0,rgba(0,0,0,.14) 0 1px 1px 0,rgba(0,0,0,.12) 0 2px 1px -1px;box-shadow:0 1px 3px 0 rgba(0,0,0,.2),0 1px 1px 0 rgba(0,0,0,.14),0 2px 1px -1px rgba(0,0,0,.12);margin-bottom:35px}.cost-breakout .cost-breakout-buttons{text-align:center;padding:20px 0 40px}.cost-breakout .cost-breakout-buttom-sm-screen{display:none}.cost-breakout .cost-breakout-buttons .btn-list{display:inline-block;background-color:#f4f5f7;border-radius:20px;overflow:hidden}.cost-breakout .cost-breakout-buttons .btn-list .button{display:inline-block;padding:7px 20px;font-size:12px;color:#595b5b}.cost-breakout .cost-breakout-buttons .btn-list .button.active{background-color:#29475d;color:#fff}.cost-breakout .breakout-details{display:-webkit-box;display:-ms-flexbox;display:flex;padding-bottom:40px}@media (max-width:500px){.cost-breakout .breakout-details{display:block}.cost-breakout .breakout-details .breakout-list{padding-right:0!important}.cost-breakout .cost-breakout-buttons{display:none}.cost-breakout .cost-breakout-buttom-sm-screen{display:block}.cost-breakout .nav.nav-tabs{display:block!important;text-align:center!important}}.coupon-details{text-align:left;font-size:16px;padding-left:15px;padding-top:15px}.cost-breakout .breakout-details .breakout-chart{-ms-flex-preferred-size:50%;flex-basis:50%;-ms-flex-line-pack:center;align-content:center}.cost-breakout .breakout-details .breakout-list{-ms-flex-preferred-size:50%;flex-basis:50%;padding:0 30px 3px 0;border-left:1px solid #e9e9e9}.cost-breakout .breakout-details .breakout-list .list-item{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;padding:15px;padding-right:10px;border-bottom:1px solid #e9e9e9;-webkit-box-align:center;-ms-flex-align:center;align-items:center}.cost-breakout .breakout-details .breakout-list .list-item:active,.cost-breakout .breakout-details .breakout-list .list-item:hover{background-color:#dcdcdc}.cost-breakout .breakout-details .breakout-list .name{text-transform:capitalize;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;font-size:13px}.cost-breakout .breakout-details .breakout-list .name>span:nth-child(2){text-align:left}.cost-breakout .breakout-details .breakout-list .colorlegend{width:16px;height:16px;border-radius:4px;margin-right:14px}.cost-breakout .breakout-details .breakout-list .prize{min-width:90px;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end}.cost-breakout .breakout-details .breakout-list .list-item .color-box{width:10px;height:10px;background-color:red;display:inline-block;margin-right:10px;border-radius:2px}.cost-breakout .breakout-details .breakout-list .list-text-item .text{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;text-align:left}.cost-breakout .breakout-details .breakout-list .list-text-item .text .label-medium{font-size:13px;font-family:Robot,sans-serif;font-weight:400}.cost-breakout .breakout-details .breakout-list .list-text-item .text span:first-child{-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1}@media (max-width:500px){.cost-breakout .breakout-details .breakout-list .list-text-item .text{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;text-align:left}.cost-breakout .breakout-details{padding-bottom:0}.cost-breakout .breakout-details .breakout-list .list-text-item{padding:10px 18px}.cost-breakout .breakout-details .breakout-list .list-text-item.list-text-item-last{padding-top:15px}.cost-breakout .breakout-details .breakout-list .list-text-item.list-text-item-last .text{margin-bottom:0}}@media(min-width:500px){.cost-breakout .breakout-details .breakout-chart{width:50%}.cost-breakout .breakout-details .breakout-list{padding:0}.cost-breakout .breakout-details div[role=costlist]{border-left:1px solid #e9e9e9}.cost-breakout .breakout-details .breakout-list-container{width:45%}}.cost-breakout .breakout-details .breakout-list .list-text-item{text-align:right;padding:10px;border-bottom:1px solid #e9e9e9}.cost-breakout .breakout-details .breakout-list .list-text-item span{padding-left:5px;padding-right:5px}.text{font-family:Roboto,sans-serif}.font-size-13{font-size:13px}.cost-breakout .breakout-details .breakout-list .list-text-item .text{font-weight:500}.cost-breakout .breakout-details .breakout-list .list-text-item:last-child{border-bottom:0}.cost-breakout .breakout-details .breakout-list .list-text-item:last-child .text{font-weight:700}.cost-breakout li[role=presentation] a[role=tab]{border:1px solid #c9c9c9!important;font-size:11px;font-weight:200;width:110px;color:#000}.cost-breakout li[role=presentation] a[role=tab].nav-link.active{border:1px solid transparent!important;color:#f9fafb}.cost-breakout li[role=presentation]:first-child a[role=\"tab\"]{border-right:none}.cost-breakout li[role=presentation]:nth-child(2) a[role=tab]{border-left:none}.cost-breakout ul[role=tablist]{border-top-left-radius:4px;border-top-right-radius:4px}", ""]);

// exports


/***/ }),

/***/ "e4ae":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("f772");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "e53d":
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "e6f3":
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__("07e3");
var toIObject = __webpack_require__("36c3");
var arrayIndexOf = __webpack_require__("5b4e")(false);
var IE_PROTO = __webpack_require__("5559")('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),

/***/ "f34d":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("2f2d");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("2fb2").default
var update = add("30173f22", content, true, {});

/***/ }),

/***/ "f493":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, ".toggle-btn{background-color:transparent;border:0;padding:0}.toggle-btn:focus{outline:none!important;-webkit-box-shadow:none!important;box-shadow:none!important}.toggle-btn:active,.toggle-btn:hover{background-color:transparent!important}.toggle-btn:active{color:#cf021a!important;border-color:transparent!important;-webkit-box-shadow:none!important;box-shadow:none!important}.room-section-type{border-radius:5px;-ms-flex-wrap:wrap;flex-wrap:wrap}.drawer-body .back-nav-container,.room-section-type{display:-webkit-box;display:-ms-flexbox;display:flex}.drawer-body .back-nav-container{-webkit-box-align:center;-ms-flex-align:center;align-items:center;font-size:12px;padding-top:10px;padding-bottom:10px;border-bottom:1px solid #dcdcdc}.drawer-body .back-nav-container:active,.drawer-body .back-nav-container:hover{background-color:#dcdcdc}.drawer-body .header{padding-left:15px;padding-right:15px;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;padding-top:10px}.room-sections{border-top:1px solid #dcdcdc}@media (max-width:500px){#drawer{position:fixed;left:0;right:0;top:100vh;bottom:0;background-color:rgba(0,0,0,.5);overflow-y:scroll;-webkit-transition:all .2s ease-in-out;transition:all .2s ease-in-out;font-size:18px;z-index:1111111}#drawer .drawer-body{min-height:100%}.open{top:0!important}}@media (min-width:500px){.drawer-body .header{padding-left:30px;padding-right:30px}#drawer{position:fixed;top:0;bottom:0;width:100%;-webkit-box-shadow:0 0 15px #cfcfcf;box-shadow:0 0 15px #cfcfcf;background-color:rgba(0,0,0,.5);overflow-y:scroll;font-size:18px;z-index:1111111}#drawer.drawer-enter-active{-webkit-animation:drawerbackgroundfadein .3s ease-out;animation:drawerbackgroundfadein .3s ease-out;-webkit-animation-iteration-count:1;animation-iteration-count:1}#drawer.drawer-leave-active{animation:drawerbackgroundfadein reverse .3s ease-out;-webkit-animation-iteration-count:1;animation-iteration-count:1;-webkit-animation-delay:.3s;animation-delay:.3s}.drawer-body{width:70%;margin-left:30%;border-radius:0!important;margin-top:0!important;position:relative;height:100%;overflow:scroll}#drawer.drawer-enter-active .drawer-body{-webkit-animation:drawermenuslidein .4s ease-out;animation:drawermenuslidein .4s ease-out;-webkit-animation-fill-mode:forwards;animation-fill-mode:forwards}#drawer.drawer-leave-active .drawer-body{animation:drawermenuslidein reverse .4s ease-out;-webkit-animation-delay:0ms;animation-delay:0ms;-webkit-animation-fill-mode:forwards;animation-fill-mode:forwards}.drawer-body .header{background:#fff}.img-carousel{padding:30px!important}#drawer div[role=costlist]{padding:15px!important}}.drawer-body{background-color:#fff;padding-bottom:50px}.drawer-body .header{position:relative}.drawer-body .header .header-text{font-size:1em;font-weight:700}.drawer-body .btn-close{width:20px;background-position-x:0;background-position-y:0;background-size:100%;background-size:20px;margin-left:15px}.img-carousel{padding:15px}.img-carousel .hash-tags{font-size:.8em;color:#000;margin:15px 0}.img-carousel .text{color:#787878;font-size:14px}.room-card .tags .tag{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between}.room-sections{padding:15px;padding-left:30px;padding-right:30px}.room-sections .tag{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;margin-bottom:5px}.room-sections .tag .tag-name,.tag-price{font-size:1em}.room-sections .tag .tag-name .color{width:15px;height:15px;background-color:red;display:inline-block;border-radius:2px;margin-right:10px}.room-sections .tag .tag-name span{text-transform:capitalize}.product-tile{max-width:100%;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-ms-flex-preferred-size:100%;flex-basis:100%;margin-bottom:16px}.product-tile:last-child{margin-bottom:0}.product-tile .product-tile-wrap{display:-webkit-box;display:-ms-flexbox;display:flex;border-bottom:1px solid #d8d8d8;padding-bottom:16px}.product-tile .product-image{width:130px;height:100%}.product-tile .product-image img{width:inherit;height:inherit}.product-tile .product-body{-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1}.product-tile .product-body,.product-tile .product-body .product-info{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between}.product-tile .product-body .product-info{font-size:.7em}.product-tile .product-body .product-info span:last-child{-ms-flex-preferred-size:30%;flex-basis:30%;text-align:right;padding-right:10px}.product-tile .product-price .price{font-size:10px;color:#4f4f4f}.product-tile .go-to-details,.product-tile .product-price .go-to-details{font-size:10px;color:#cf021a}.product-tile .product-info{color:#585858;font-size:10px}.ls-loader-circle{display:inline-block;position:relative;width:inherit;height:inherit}.ls-loader-circle div{-webkit-box-sizing:border-box;box-sizing:border-box;display:block;position:absolute;top:calc(50% - 50px);left:calc(50% - 38px);-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);width:50px;height:50px;margin:6px;border:6px solid #f74861;border-radius:50%;-webkit-animation:ls-loader-circle 1.2s cubic-bezier(.5,0,.5,1) infinite;animation:ls-loader-circle 1.2s cubic-bezier(.5,0,.5,1) infinite;border-color:#f74861 transparent transparent}.ls-loader-circle div:first-child{-webkit-animation-delay:-.45s;animation-delay:-.45s}.ls-loader-circle div:nth-child(2){-webkit-animation-delay:-.3s;animation-delay:-.3s}.ls-loader-circle div:nth-child(3){-webkit-animation-delay:-.15s;animation-delay:-.15s}@-webkit-keyframes ls-loader-circle{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}@keyframes ls-loader-circle{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}@-webkit-keyframes drawerbackgroundfadein{0%{background-color:transparent;right:-100%}1%{background-color:transparent;right:0}to{background-color:rgba(0,0,0,.5);right:0}}@keyframes drawerbackgroundfadein{0%{background-color:transparent;right:-100%}1%{background-color:transparent;right:0}to{background-color:rgba(0,0,0,.5);right:0}}@-webkit-keyframes drawermenuslidein{0%{right:-100%}to{right:0}}@keyframes drawermenuslidein{0%{right:-100%}to{right:0}}@media (max-width:500px){.label-box{max-width:70px;max-height:38px;overflow:scroll}}@media (min-width:500px){.label-box{max-width:800px;overflow:scroll}}", ""]);

// exports


/***/ }),

/***/ "f4be":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, ".designer-card .designer-img-wrap{position:relative}.designer-card{background-color:#fff;border-radius:5px;-webkit-box-shadow:rgba(0,0,0,.2) 0 1px 3px 0,rgba(0,0,0,.14) 0 1px 1px 0,rgba(0,0,0,.12) 0 2px 1px -1px;box-shadow:0 1px 3px 0 rgba(0,0,0,.2),0 1px 1px 0 rgba(0,0,0,.14),0 2px 1px -1px rgba(0,0,0,.12);padding:20px 40px}@media (max-width:500px){.designer-card{padding:20px 50px}}.designer-card .designer-img{border-radius:50%;position:absolute;width:70px;height:70px;overflow:hidden;bottom:10px;left:-5px}.designer-card .about-designer{width:70%;margin-left:95px;background-color:#f0f2f4;padding:10px 15px;border-radius:10px;color:#63696d;font-style:italic;position:relative}.notification-edge{width:0;height:0;border-top:25px solid transparent;border-bottom:1px solid transparent;border-right:46px solid #f0f2f4;position:absolute;left:-35px;bottom:10px}.btn-block{width:100%;height:40px;border:0;border-radius:25px;margin-bottom:10px}.designer-contact-info{margin-bottom:20px}", ""]);

// exports


/***/ }),

/***/ "f772":
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "fae3":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  var i
  if ((i = window.document.currentScript) && (i = i.src.match(/(.+\/)[^/]+\.js(\?.*)?$/))) {
    __webpack_require__.p = i[1] // eslint-disable-line
  }
}

// Indicate to webpack that this file can be concatenated
/* harmony default export */ var setPublicPath = (null);

// EXTERNAL MODULE: ./node_modules/moment/moment.js
var moment = __webpack_require__("c1df");
var moment_default = /*#__PURE__*/__webpack_require__.n(moment);

// CONCATENATED MODULE: ./src/services/formatter/boqV2.js


class boqV2_BOQFormatter {

  constructor (dataClient, boqId) {
    this.dataClient = dataClient;
    this.boqId = boqId;
    this.boqDetails = null;
    this.videoUrls = null;
    this.currencyType = "INR";
    this.countryISOCode = "IN";
    this.tncContent = null;
  }

  setBOQDetails(boqId, loaded, videoLoader){
    this.dataClient.getBOQDetails(boqId).then(data => {
      this.boqDetails = data.data;
      this.countryISOCode = this.boqDetails.project.country_code;
      this.currencyType = this.boqDetails.project.currency;
      loaded.value = true;
      console.log(data);
      this.getLivHomeVideos(videoLoader)
      // return this.boqDetails;
    }).catch(error => {
      console.log(error)
    })
  }

  getData(){
    return this.boqDetails;
  }

  getSummaryDetails(){
    var summary = {}
    summary["totalProducts"] = this.boqDetails.products.length;
    var rooms = new Set();
    this.boqDetails.products.forEach(function(product){
      if(!rooms.has(product.room_tag.id))
          rooms.add(product.room_tag.id);
    });
    summary["roomCount"] = rooms.size;
    summary["totalPrice"] = Math.round(this.boqDetails.proposal.net_payable);
    summary["payByDate"] = moment_default()(this.boqDetails.proposal.pay_by_date).format('Do MMM, YYYY');
    summary["remainingDays"] = Math.round((new Date(this.boqDetails.proposal.pay_by_date.replace(/-/g, "/")) - new Date())/(1000*60*60*24))
    return summary;
  }

  getLivHomeVideoUrls(){
    return this.videoUrls;
  }


  getLivHomeVideos(isLoaded){
    //get custom sku details and fetch url in it
    var modular_skus = []
    this.boqDetails.products.forEach(function(product){
      if(product.group_skus.length>0){
        modular_skus.push(product.sku_code);
      }
    });
    var videoUrls = new Set();

    if(modular_skus.length>0){
      this.dataClient.getCMSDetails(modular_skus).then(data => {
        data.data.forEach(function(skuDetail){
          skuDetail.images.forEach(function(image){
            var imageUrl = image.image_url;
            var parser = new URL(imageUrl);
            if(parser.pathname.substring(parser.pathname.length-4, parser.pathname.length) == "webm" || parser.pathname.substring(parser.pathname.length-3, parser.pathname.length) == "mp4")
              videoUrls.add(imageUrl);
          });
        });
        isLoaded.value = true
        var urls = Array.from(videoUrls);
        var sortedUrls = []
        urls.forEach(function(url){
          var parser = new URL(url);
          if(parser.pathname.substring(parser.pathname.length-4, parser.pathname.length) == "webm")
            sortedUrls.push(url);
          else {
            sortedUrls.unshift(url);
          }
        });

        this.videoUrls = sortedUrls;
      }).catch(error => {
        console.log(error)
      })
    }
  }

  getCostBreakOutDetails(){
    var costBreakOutDetails = {};
    var scopeWiseDetails = {
      "modular": 0,
      "civil": 0,
      "others": 0
    };
    var roomWiseDetails = new Map();
    this.boqDetails.products.forEach(function(product){
      var productPrice = Math.ceil(product.quantity * product.selling_price)
      if(!roomWiseDetails.has(product.room_tag.display_name))
          roomWiseDetails.set(product.room_tag.display_name, {'value':0, 'roomId':product.room_tag.id})
      var room = roomWiseDetails.get(product.room_tag.display_name)
      room['value'] = room['value'] + productPrice;
      if(product.group_skus.length>0)
        scopeWiseDetails["modular"] = scopeWiseDetails["modular"] + productPrice;
      else if(product.vendor_type === 'CANVAS_VENDOR')
        scopeWiseDetails["civil"] = scopeWiseDetails["civil"] + productPrice;
      else
        scopeWiseDetails["others"] = scopeWiseDetails["others"] + productPrice;
    });
    costBreakOutDetails["scope"] = scopeWiseDetails;
    costBreakOutDetails["room"] = roomWiseDetails;
    return costBreakOutDetails;
  }

  getDesignerDetails(){
    var designerDetails = {};
    var designer = this.boqDetails.primary_designer || {};
    designerDetails["designerName"] = designer.display_name;
    designerDetails["designerPhone"] = designer.phone;
    designerDetails["designerEmail"] = designer.email;
    designerDetails["designerImage"] = designer.image_url;
    designerDetails["customerName"] = (this.boqDetails.project && this.boqDetails.project.customer_display_name);
    return designerDetails;
  }

  getProjectAmountDetails(){
    var amountDetails = {};
    amountDetails["handlingFee"] = Math.round(this.boqDetails.proposal.handling_fee);
    amountDetails["subTotal"] = Math.round(this.boqDetails.proposal.total_price);
    amountDetails["discount"] = Math.round(this.boqDetails.proposal.discount);
    amountDetails["total"] = Math.round(this.boqDetails.proposal.net_payable);
    amountDetails["coupons"] = this.boqDetails.proposal.coupons_metadata
    return amountDetails;
  }

  getRoomScopeBreakOutDetails(){
    var roomScopeBreakOutDetails = new Map();
    this.boqDetails.products.forEach(function(product){
      var productPrice = Math.ceil(product.quantity * product.selling_price)
      if(!roomScopeBreakOutDetails.has(product.room_tag.id))
          roomScopeBreakOutDetails.set(product.room_tag.id, {
              'scopes':{
                'modular': {
                  'skus': [],
                  'total': 0,
                  'children': new Map(),
                  'skuQuantity': new Map(),
                  'skuPrice': new Map(),
                  'childSkuPrices': new Map()
                },
                'civil': {
                  'skus': [],
                  'total': 0,
                  'skuQuantity': new Map(),
                  'skuPrice': new Map()
                },
                'others': {
                  'skus': [],
                  'total': 0,
                  'skuQuantity': new Map(),
                  'skuPrice': new Map()
                }
              },
              'skus': [],
              'total': 0
          });
      var currentRoomBreakUp = roomScopeBreakOutDetails.get(product.room_tag.id)
      currentRoomBreakUp['skus'].push(product.sku_code);
      currentRoomBreakUp['total'] = currentRoomBreakUp['total'] + productPrice;
      if(product.group_skus.length>0){
        currentRoomBreakUp['scopes']["modular"]["total"]  = currentRoomBreakUp['scopes']["modular"]["total"] + productPrice;
        currentRoomBreakUp['scopes']["modular"]['skus'].push(product.sku_code);
        currentRoomBreakUp['scopes']["modular"]['skuQuantity'].set(product.sku_code, product.quantity);
        var price = ("selling_price" in product) ? product.selling_price: product.price;
        currentRoomBreakUp['scopes']["modular"]['skuPrice'].set(product.sku_code, price);
        currentRoomBreakUp['scopes']["modular"]["children"].set(product.sku_code, []);
        product.group_skus.forEach(function(childSku){
          currentRoomBreakUp['skus'].push(childSku.sku_code);
          currentRoomBreakUp['scopes']["modular"]["children"].get(product.sku_code).push(childSku);
          var childPrice = ("selling_price" in childSku) ? childSku.selling_price: childSku.price;
          currentRoomBreakUp['scopes']["modular"]["childSkuPrices"].set(childSku.sku_code, childPrice);
        });
      }
      else if(product.vendor_type === 'CANVAS_VENDOR'){
        currentRoomBreakUp['scopes']["civil"]["total"]  = currentRoomBreakUp['scopes']["civil"]["total"] + productPrice;
        currentRoomBreakUp['scopes']["civil"]['skus'].push(product.sku_code);
        currentRoomBreakUp['scopes']["civil"]['skuQuantity'].set(product.sku_code, product.quantity);
        currentRoomBreakUp['scopes']["civil"]['skuPrice'].set(product.sku_code, product.selling_price);
      }
      else{
        currentRoomBreakUp['scopes']["others"]["total"]  = currentRoomBreakUp['scopes']["others"]["total"] + productPrice;
        currentRoomBreakUp['scopes']["others"]['skus'].push(product.sku_code);
        currentRoomBreakUp['scopes']["others"]['skuQuantity'].set(product.sku_code, product.quantity);
        currentRoomBreakUp['scopes']["others"]['skuPrice'].set(product.sku_code, product.selling_price);
      }
    });
    return roomScopeBreakOutDetails;
  }

  getRoomsDetails(){
    var roomsDetails = this.boqDetails.rooms
    var validRooms = 0;
    var roomScopeBreakOutDetails = this.getRoomScopeBreakOutDetails();
    this.boqDetails.rooms.forEach(function(room){
      if(roomScopeBreakOutDetails.has(room.id)){
        room["scopes"] = roomScopeBreakOutDetails.get(room.id)["scopes"]
        room["total"] = roomScopeBreakOutDetails.get(room.id)["total"]
      }
      room["images"] = []
      room.attachments.forEach(function(attachment){
        if(!attachment.is_cover_image){
          room["images"].push(attachment.url)
        }
        else{
          room["images"].unshift(attachment.url)
        }
      });
      if(room.total)
        validRooms = validRooms + 1;
    });
    roomsDetails["validRooms"] = validRooms;
    return roomsDetails
  }

  getRoomDetails(roomId, isLoaded){
    var roomDetails = null
    var roomScopeBreakOutDetails = this.getRoomScopeBreakOutDetails();
    this.boqDetails.rooms.forEach(function(room){
      if(room.id == roomId){
        if(roomScopeBreakOutDetails.has(room.id)){
          room["scopes"] = roomScopeBreakOutDetails.get(room.id)["scopes"]
          room["skus"] = roomScopeBreakOutDetails.get(room.id)["skus"]
          room["skuQuantity"] = roomScopeBreakOutDetails.get(room.id)["skuQuantity"]
          room["items"] = new Map()
        }
        room["images"] = []
        room.attachments.forEach(function(attachment){
          room["images"].push(attachment.url)
        });
        roomDetails = room;
      }
    });
    if(roomDetails.skus.length>0){
      this.dataClient.getCMSDetails(roomDetails.skus).then(data => {
        data.data.forEach(function(skuDetail){
          roomDetails.items.set(skuDetail.code, skuDetail)
        });
        isLoaded.value = true
      }).catch(error => {
        console.log(error)
      })
    }
    return roomDetails
  }

  getTnCContent(isLoaded){
    var slug = "boq-tnc-"+ this.countryISOCode.toLowerCase() +"?include=content.blocks"
    var self = this;
    this.dataClient.getNovaContent(slug).then(data => {
      console.log(data);
      isLoaded.value = true;
      self.tncContent = data.data;
    });
  }



}

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/SummaryCard.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var SummaryCard = ({
  props: {
    formatter: {
      type: Object,
      required: true
    }
  },
  data: function data() {
    return {
      summaryDetails: {}
    };
  },

  mounted: function mounted() {
    this.summaryDetails = this.formatter.getSummaryDetails();
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-11d1f9d2","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/SummaryCard.vue
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"summary-card"},[_c('div',{staticClass:"card"},[_c('div',{staticClass:"card-item"},[_c('span',{staticClass:"title"},[_vm._v(" Quote includes ")]),_vm._v(" "),_c('p',{staticClass:"text"},[_vm._v(" "+_vm._s(_vm.summaryDetails.roomCount)+" Sections, "+_vm._s(_vm.summaryDetails.totalProducts)+" Products ")])]),_vm._v(" "),_c('div',{staticClass:"card-item"},[_c('span',{staticClass:"title"},[_vm._v(" Total price ")]),_vm._v(" "),_c('p',{staticClass:"text"},[_vm._v(" "+_vm._s(_vm._f("toCurrency")(_vm.summaryDetails.totalPrice,_vm.formatter.currencyType, _vm.formatter.countryISOCode))+" ")])]),_vm._v(" "),_c('div',{staticClass:"card-item-last"},[_c('div',{staticClass:"card-item"},[_c('span',{staticClass:"title"},[_vm._v("Quote valid till")]),_vm._v(" "),_c('p',{staticClass:"text"},[_vm._v(" "+_vm._s(_vm.summaryDetails.payByDate)+" ")])]),_vm._v(" "),(_vm.summaryDetails.remainingDays>=0)?_c('div',{staticClass:"card-item"},[_c('p',{staticClass:"days-in-circle"},[_vm._v(" "+_vm._s(_vm.summaryDetails.remainingDays))]),_vm._v(" "),_vm._m(0)]):_vm._e(),_vm._v(" "),(_vm.summaryDetails.remainingDays<0)?_c('div',{staticClass:"card-item",staticStyle:{"display":"block"}},[_c('p',{staticStyle:{"color":"rgb(247, 72, 97)","margin":"0"}},[_vm._v(" Expired")]),_vm._v(" "),_c('p',{staticClass:"title",staticStyle:{"margin-bottom":"0px"}},[_vm._v("Please contact your designer")])]):_vm._e()])])])}
var staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('p',{staticClass:"title",staticStyle:{"margin-bottom":"0px"}},[_vm._v(" days "),_c('br'),_vm._v(" till expiration")])}]

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/runtime/component-normalizer.js
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  scriptExports = scriptExports || {}

  // ES6 modules interop
  var type = typeof scriptExports.default
  if (type === 'object' || type === 'function') {
    scriptExports = scriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}

// CONCATENATED MODULE: ./src/components/SummaryCard.vue
function injectStyle (context) {
  __webpack_require__("199b")
}
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null

var Component = normalizeComponent(
  SummaryCard,
  render,
  staticRenderFns,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ var components_SummaryCard = (Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/DesignerCard.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var DesignerCard = ({
  props: {
    formatter: {
      type: Object,
      required: true
    }
  },
  data: function data() {
    return {
      designerDetails: {}
    };
  },

  mounted: function mounted() {
    this.designerDetails = this.formatter.getDesignerDetails();
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-47c0f1e5","hasScoped":false,"optionsId":"2","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/DesignerCard.vue
var DesignerCard_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"designer-card"},[_c('div',{staticClass:"designer-img-wrap"},[_c('div',{staticClass:"designer-img"},[_c('img',{staticClass:"img-responsive",attrs:{"src":_vm.designerDetails.designerImage,"alt":"designer_image"}})]),_vm._v(" "),_c('div',{staticClass:"about-designer"},[_c('p',[_vm._v("Hi "+_vm._s(_vm.designerDetails.customerName)+", please review the quote and reach out to me if you have any questions or concerns.")]),_vm._v(" "),_c('div',{staticClass:"notification-edge"})])]),_vm._v(" "),_c('div',{staticClass:"designer-contact-info"},[_c('h5',{staticStyle:{"font-size":"18px","margin":"20px 0px"}},[_vm._v(_vm._s(_vm.designerDetails.designerName))]),_vm._v(" "),_c('p',{staticStyle:{"color":"#979797","font-size":"12px","margin-bottom":"5px"}},[_c('span',{staticClass:"glyphicon glyphicon-envelope"}),_vm._v(" "),_c('span',{staticStyle:{"display":"inline-block","padding-left":"10px"}},[_vm._v(_vm._s(_vm.designerDetails.designerEmail))])]),_vm._v(" "),_c('p',{staticStyle:{"color":"#979797","font-size":"12px"}},[_c('span',{staticClass:"glyphicon glyphicon-earphone"}),_vm._v(" "),_c('span',{staticStyle:{"display":"inline-block","padding-left":"10px"}},[_vm._v(_vm._s(_vm.designerDetails.designerPhone))])])]),_vm._v(" "),_c('a',{staticClass:"btn-block",staticStyle:{"text-align":"center","background-color":"rgb(247, 72, 97)","color":"rgb(255, 255, 255)","line-height":"40px"},attrs:{"href":'tel:+'+_vm.designerDetails.designerPhone}},[_vm._v(" Call Designer ")])])}
var DesignerCard_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/DesignerCard.vue
function DesignerCard_injectStyle (context) {
  __webpack_require__("4f25")
}
/* script */


/* template */

/* template functional */
var DesignerCard_vue_template_functional_ = false
/* styles */
var DesignerCard_vue_styles_ = DesignerCard_injectStyle
/* scopeId */
var DesignerCard_vue_scopeId_ = null
/* moduleIdentifier (server only) */
var DesignerCard_vue_module_identifier_ = null

var DesignerCard_Component = normalizeComponent(
  DesignerCard,
  DesignerCard_render,
  DesignerCard_staticRenderFns,
  DesignerCard_vue_template_functional_,
  DesignerCard_vue_styles_,
  DesignerCard_vue_scopeId_,
  DesignerCard_vue_module_identifier_
)

/* harmony default export */ var components_DesignerCard = (DesignerCard_Component.exports);

// EXTERNAL MODULE: ./node_modules/babel-runtime/helpers/slicedToArray.js
var slicedToArray = __webpack_require__("b24f");
var slicedToArray_default = /*#__PURE__*/__webpack_require__.n(slicedToArray);

// EXTERNAL MODULE: ./node_modules/babel-runtime/core-js/object/entries.js
var entries = __webpack_require__("a05d");
var entries_default = /*#__PURE__*/__webpack_require__.n(entries);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/chevron.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var chevron = ({
  data: function data() {
    return {};
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-15468234","hasScoped":false,"optionsId":"2","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/chevron.vue
var chevron_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('svg',{attrs:{"width":"24px","height":"24px","viewBox":"0 0 24 24","version":"1.1","xmlns":"http://www.w3.org/2000/svg","xmlns:xlink":"http://www.w3.org/1999/xlink"}},[_c('title',[_vm._v("E4B125F3-9117-4245-A4A2-7AA13330A26B")]),_vm._v(" "),_c('desc',[_vm._v("Created with sketchtool.")]),_vm._v(" "),_c('g',{attrs:{"id":"Final","stroke":"none","stroke-width":"1","fill":"none","fill-rule":"evenodd"}},[_c('g',{attrs:{"id":"9.-icons","transform":"translate(-252.000000, -296.000000)","fill":"#FF3F6E","fill-rule":"nonzero"}},[_c('g',{attrs:{"id":"Group","transform":"translate(252.000000, 296.000000)"}},[_c('g',{attrs:{"id":"ic-round-expand-more-copy-4"}},[_c('g',{attrs:{"id":"Icon"}},[_c('path',{attrs:{"d":"M15.88,9.29 L12,13.17 L8.12,9.29 C7.86812776,9.03812775 7.50101597,8.93976044 7.15695208,9.03195208 C6.8128882,9.12414372 6.54414372,9.3928882 6.45195208,9.73695208 C6.35976044,10.081016 6.45812775,10.4481278 6.71,10.7 L11.3,15.29 C11.69,15.68 12.32,15.68 12.71,15.29 L17.3,10.7 C17.4872281,10.5131554 17.592444,10.2595095 17.592444,9.995 C17.592444,9.73049053 17.4872281,9.47684459 17.3,9.29 C16.91,8.91 16.27,8.9 15.88,9.29 L15.88,9.29 Z","id":"Icon-Path"}})])])])])])])}
var chevron_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/chevron.vue
/* script */


/* template */

/* template functional */
var chevron_vue_template_functional_ = false
/* styles */
var chevron_vue_styles_ = null
/* scopeId */
var chevron_vue_scopeId_ = null
/* moduleIdentifier (server only) */
var chevron_vue_module_identifier_ = null

var chevron_Component = normalizeComponent(
  chevron,
  chevron_render,
  chevron_staticRenderFns,
  chevron_vue_template_functional_,
  chevron_vue_styles_,
  chevron_vue_scopeId_,
  chevron_vue_module_identifier_
)

/* harmony default export */ var components_chevron = (chevron_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/CostList.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ var CostList = ({
  components: {
    chevron: components_chevron
  },
  props: {
    listElements: {
      type: Array, /* [{name, currencyvalue, color}, ...] */
      required: true
    },
    chevronrotation: {
      type: Number,
      default: 0
    },
    showchevron: {
      type: Boolean,
      default: true
    },
    compact: {
      type: Boolean,
      default: false
    },
    formatter: {
      type: Object,
      required: true
    }
  },
  methods: {
    openDrawer: function openDrawer(val) {
      if (val) this.$emit('openRoom', val);
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-b5fb8f50","hasScoped":false,"optionsId":"2","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/CostList.vue
var CostList_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:_vm.$style.costlist,attrs:{"role":"costlist"}},_vm._l((_vm.listElements),function(item,index){return _c('div',{key:index,class:( _obj = {}, _obj[_vm.$style.listitem] = true, _obj[_vm.$style.compact] = _vm.compact, _obj ),on:{"click":function($event){return _vm.openDrawer(item.roomId)}}},[_c('div',{staticClass:"name"},[_c('div',{class:_vm.$style.colorlegend,style:({'background-color': item.color})}),_vm._v(" "),_c('span',{class:_vm.$style.propertyname},[_vm._v("\n          "+_vm._s(item.name)+"\n        ")])]),_vm._v(" "),_c('div',{class:( _obj$1 = {}, _obj$1[_vm.$style.price] = true, _obj$1 )},[_c('span',[_vm._v(_vm._s(_vm._f("toCurrency")(item.currencyvalue,_vm.formatter.currencyType, _vm.formatter.countryISOCode)))]),_vm._v(" "),(_vm.showchevron)?_c('div',{class:_vm.$style.chevronstyle,style:({transform: 'rotateZ('+_vm.chevronrotation+'Deg)'})},[_c('chevron')],1):_vm._e()])])
var _obj;
var _obj$1;}),0)}
var CostList_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/CostList.vue
function CostList_injectStyle (context) {
  this["$style"] = __webpack_require__("b6c3")
}
/* script */


/* template */

/* template functional */
var CostList_vue_template_functional_ = false
/* styles */
var CostList_vue_styles_ = CostList_injectStyle
/* scopeId */
var CostList_vue_scopeId_ = null
/* moduleIdentifier (server only) */
var CostList_vue_module_identifier_ = null

var CostList_Component = normalizeComponent(
  CostList,
  CostList_render,
  CostList_staticRenderFns,
  CostList_vue_template_functional_,
  CostList_vue_styles_,
  CostList_vue_scopeId_,
  CostList_vue_module_identifier_
)

/* harmony default export */ var components_CostList = (CostList_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/PaymentDetails.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var PaymentDetails = ({
  props: {
    paymentDetails: {
      type: Object,
      required: true
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-220f2064","hasScoped":false,"optionsId":"2","buble":{"transforms":{"stripWithFunctional":true}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/PaymentDetails.vue
var PaymentDetails_render = function (_h,_vm) {var _c=_vm._c;return _c('div',{staticClass:"breakout-list"},[_c('div',{staticClass:"list-text-item"},[_c('p',{staticClass:"text"},[_c('span',{staticClass:"label-medium"},[_vm._v(" Sub Total ")]),_vm._v(" "),_c('span',{staticStyle:{"flex-basis":"150px","text-align":"right"}},[_vm._v(" \n            "+_vm._s(_vm._f("toCurrency")(_vm.paymentDetails.subTotal))+"\n          ")])]),_vm._v(" "),(_vm.paymentDetails.discount)?_c('p',{staticClass:"text"},[_c('span',{staticClass:"label-medium"},[_vm._v(" Discount ")]),_vm._v(" "),_c('span',{staticStyle:{"display":"inline-block","padding-left":"40px"}},[_vm._v(" "+_vm._s(_vm._f("toCurrency")(_vm.paymentDetails.discount)))])]):_vm._e(),_vm._v(" "),_c('p',{staticClass:"text"},[_c('span',{staticClass:"label-medium"},[_vm._v(" Design & project management fee ")]),_vm._v(" "),_c('span',{staticStyle:{"flex-basis":"150px","text-align":"right"}},[_vm._v(" "+_vm._s(_vm._f("toCurrency")(_vm.paymentDetails.handlingFee)))])])]),_vm._v(" "),_c('div',{staticClass:"list-text-item list-text-item-last"},[_c('p',{staticClass:"text"},[_c('span',{staticClass:"font-size-13"},[_vm._v(" Total project estimate ")]),_vm._v(" "),_c('span',{staticStyle:{"text-align":"right"}},[_vm._v(" "+_vm._s(_vm._f("toCurrency")(_vm.paymentDetails.total)))])])])])}
var PaymentDetails_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/PaymentDetails.vue
/* script */


/* template */

/* template functional */
var PaymentDetails_vue_template_functional_ = true
/* styles */
var PaymentDetails_vue_styles_ = null
/* scopeId */
var PaymentDetails_vue_scopeId_ = null
/* moduleIdentifier (server only) */
var PaymentDetails_vue_module_identifier_ = null

var PaymentDetails_Component = normalizeComponent(
  PaymentDetails,
  PaymentDetails_render,
  PaymentDetails_staticRenderFns,
  PaymentDetails_vue_template_functional_,
  PaymentDetails_vue_styles_,
  PaymentDetails_vue_scopeId_,
  PaymentDetails_vue_module_identifier_
)

/* harmony default export */ var components_PaymentDetails = (PaymentDetails_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/CostBreakout.vue


//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


// import VueApexCharts from 'vue-apexcharts'


var scopeColors = {
  MODULAR: '#F2AA36',
  CIVIL: '#35C3CC',
  OTHERS: '#208F9C'
};

var colors = [scopeColors.MODULAR, scopeColors.CIVIL, scopeColors.OTHERS, '#C2E7D5', '#2A465B', '#E75E45', '#6236FF', '#CABBFF', '#9B575B', '#2C1320'];
var plotOptions = {
  pie: {
    offsetY: 5,
    customScale: 0.8,
    dataLabels: {
      offset: 60
    },
    donut: {
      size: '50%'
    }
  }
};
var dataLabels = {
  enabled: true,
  enabledOnSeries: undefined,
  textAnchor: 'middle',
  offsetX: 0,
  offsetY: 0,
  formatter: formatter,
  style: {
    fontSize: '14px',
    fontFamily: 'Helvetica, Arial, sans-serif',
    colors: ["#000000"]
  },
  dropShadow: {
    enabled: false,
    top: 1,
    left: 1,
    blur: 1,
    opacity: 0.45
  }
};
function formatter(val, opt) {
  var s = opt.w.globals.labels[opt.seriesIndex];
  s = s.charAt(0).toUpperCase() + s.slice(1);
  if (s.length > 9) {
    s = s.substr(0, 9) + "...";
  }
  return s;
}

/* harmony default export */ var CostBreakout = ({
  props: {
    formatter: {
      type: Object,
      required: true
    }
  },
  components: {
    CostList: components_CostList,
    PaymentDetails: components_PaymentDetails
  },
  computed: {
    roomCostBreakoutList: function roomCostBreakoutList() {
      var _this = this;

      if (this.costBreakOutDetails.room) {
        var breakoutlist = [];
        this.costBreakOutDetails.room.forEach(function (room, name) {
          breakoutlist.push({
            name: name,
            color: _this.getLegendColor([name, room.value]),
            currencyvalue: room.value,
            roomId: room.roomId
          });
        });
        return breakoutlist;
      } else {
        return [];
      }

      // .map((name, cost) => {
      //   return {
      //     name,
      //     color: this.getLegendColor([name, cost]),
      //     currencyvalue: cost
      //   }
      // })
    },
    scopeCostBreakoutList: function scopeCostBreakoutList() {
      if (this.costBreakOutDetails.scope) {
        var breakoutlist = [];
        entries_default()(this.costBreakOutDetails.scope).forEach(function (_ref) {
          var _ref2 = slicedToArray_default()(_ref, 2),
              name = _ref2[0],
              cost = _ref2[1];

          breakoutlist.push({
            name: name,
            color: scopeColors[name.toUpperCase()] || "#2A465B",
            currencyvalue: cost
          });
        });
        return breakoutlist;
      } else {
        return [];
      }
    }
  },
  data: function data() {
    return {
      dummylist: [{
        name: 'Item1',
        currencyvalue: 1231,
        color: '#C2E7D5'
      }, {
        name: 'Item2',
        currencyvalue: 998,
        color: '#35C3CC'
      }, {
        name: 'Item1',
        currencyvalue: 654,
        color: '#208F9C'
      }],
      roomOptions: {
        colors: colors,
        plotOptions: plotOptions,
        labels: [],
        legend: {
          show: false

        },
        dataLabels: dataLabels
      },
      scopeOptions: {
        colors: colors,
        labels: [],
        legend: {
          show: false
        },
        plotOptions: plotOptions,
        dataLabels: dataLabels
      },

      roomSeries: [],
      roomLabels: [],
      scopeSeries: [],
      scopeLabels: [],
      costBreakOutDetails: {}
    };
  },

  created: function created() {
    this.paymentDetails = this.paymentDetails || { subTotal: 0 };
  },
  mounted: function mounted() {
    this.costBreakOutDetails = this.formatter.getCostBreakOutDetails();
    this.paymentDetails = this.formatter.getProjectAmountDetails();
    var that = this;
    this.costBreakOutDetails.room.forEach(function (value, key) {
      that.roomOptions.labels.push(key);
      that.roomSeries.push(value.value);
    });
    for (var scope in this.costBreakOutDetails.scope) {
      this.scopeOptions.labels.push(scope);
      this.scopeSeries.push(this.costBreakOutDetails.scope[scope]);
    }
  },
  methods: {
    getLegendColor: function getLegendColor(value) {
      var indexofroom = this.roomOptions.labels.indexOf(value[0]);
      var colorsarraylength = this.roomOptions.colors.length;
      return this.roomOptions.colors[indexofroom % colorsarraylength];
    },
    openRoom: function openRoom(val) {
      this.$emit('openRoom', val);
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-6bbe8bce","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/CostBreakout.vue
var CostBreakout_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"cost-breakout"},[_c('div',[_c('div',{staticClass:"breakout-details"},[_c('div',{staticClass:"breakout-chart"},[_c('apexchart',{attrs:{"type":"donut","options":_vm.roomOptions,"series":_vm.roomSeries}})],1),_vm._v(" "),_c('div',{staticClass:"breakout-list-container"},[_c('CostList',{attrs:{"listElements":_vm.roomCostBreakoutList,"chevronrotation":-90,"formatter":_vm.formatter},on:{"openRoom":_vm.openRoom}}),_vm._v(" "),_c('div',{staticClass:"breakout-list"},[_c('div',{staticClass:"list-text-item"},[_c('p',{staticClass:"text"},[_c('span',{staticClass:"label-medium"},[_vm._v(" Sub Total ")]),_vm._v(" "),_c('span',{staticStyle:{"flex-basis":"150px","text-align":"right"}},[_vm._v("\n                          "+_vm._s(_vm._f("toCurrency")(_vm.paymentDetails.subTotal,_vm.formatter.currencyType, _vm.formatter.countryISOCode))+"\n                        ")])]),_vm._v(" "),_c('p',{staticClass:"text"},[_c('span',{staticClass:"label-medium"},[_vm._v(" Design & project management fee ")]),_vm._v(" "),_c('span',{staticStyle:{"flex-basis":"150px","text-align":"right"}},[_vm._v(" "+_vm._s(_vm._f("toCurrency")(_vm.paymentDetails.handlingFee,_vm.formatter.currencyType, _vm.formatter.countryISOCode)))])]),_vm._v(" "),(_vm.paymentDetails.discount)?_c('p',{staticClass:"text"},[_c('span',{staticClass:"label-medium"},[_vm._v(" Total order value")]),_vm._v(" "),_c('span',{staticStyle:{"flex-basis":"150px","text-align":"right"}},[_vm._v(" "+_vm._s(_vm._f("toCurrency")((_vm.paymentDetails.subTotal + _vm.paymentDetails.handlingFee),_vm.formatter.currencyType, _vm.formatter.countryISOCode)))])]):_vm._e(),_vm._v(" "),(_vm.paymentDetails.discount)?_c('p',{staticClass:"text"},[_c('span',{staticClass:"label-medium"},[_vm._v(" Discount ")]),_vm._v(" "),_c('span',{staticStyle:{"display":"inline-block","padding-left":"40px"}},[_vm._v(" "+_vm._s(_vm._f("toCurrency")(_vm.paymentDetails.discount,_vm.formatter.currencyType, _vm.formatter.countryISOCode)))])]):_vm._e()]),_vm._v(" "),(_vm.paymentDetails.coupons && _vm.paymentDetails.coupons.length>0)?_c('div',{staticStyle:{"border-bottom":"1px solid #e9e9e9","font-family":"'Robot', sans-serif"}},[_c('p',{staticClass:"coupon-details"},[_vm._v("\n                     Coupon Details\n                  ")]),_vm._v(" "),_vm._l((_vm.paymentDetails.coupons),function(coupon){return _c('div',[_c('div',{staticStyle:{"display":"flex","padding-left":"15px","margin-bottom":"15px","font-size":"13px"}},[_c('div',{staticStyle:{"flex-basis":"30%","text-align":"left"}},[_c('span',{staticStyle:{"text-transform":"uppercase"}},[_vm._v(_vm._s(coupon.coupon_code))])]),_vm._v(" "),_c('div',{staticStyle:{"width":"70%","text-align":"left"}},[_c('span',[_vm._v(_vm._s(coupon.description))])])])])})],2):_vm._e(),_vm._v(" "),_c('div',{staticClass:"list-text-item list-text-item-last"},[_c('p',{staticClass:"text"},[_c('span',{staticClass:"font-size-13"},[_vm._v(" Total project estimate ")]),_vm._v(" "),_c('span',{staticStyle:{"text-align":"right"}},[_vm._v(" "+_vm._s(_vm._f("toCurrency")(_vm.paymentDetails.total,_vm.formatter.currencyType, _vm.formatter.countryISOCode)))])])])])],1)])])])}
var CostBreakout_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/CostBreakout.vue
function CostBreakout_injectStyle (context) {
  __webpack_require__("6605")
}
/* script */


/* template */

/* template functional */
var CostBreakout_vue_template_functional_ = false
/* styles */
var CostBreakout_vue_styles_ = CostBreakout_injectStyle
/* scopeId */
var CostBreakout_vue_scopeId_ = null
/* moduleIdentifier (server only) */
var CostBreakout_vue_module_identifier_ = null

var CostBreakout_Component = normalizeComponent(
  CostBreakout,
  CostBreakout_render,
  CostBreakout_staticRenderFns,
  CostBreakout_vue_template_functional_,
  CostBreakout_vue_styles_,
  CostBreakout_vue_scopeId_,
  CostBreakout_vue_module_identifier_
)

/* harmony default export */ var components_CostBreakout = (CostBreakout_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/paymentSummaryV2.vue
//
//
//
//


/* harmony default export */ var paymentSummaryV2 = ({
  props: {
    formatter: {
      type: Object,
      required: true
    },
    subs: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    return {
      summaryDetails: {},
      tenPercent: 0,
      fiftyPercent: 0,
      complete: 0
    };
  },

  beforeMount: function beforeMount() {
    this.summaryDetails = this.formatter.getSummaryDetails();
    this.tenPercent = Math.round(this.summaryDetails.totalPrice * 0.1);
    this.fiftyPercent = Math.round(this.summaryDetails.totalPrice * 0.4);
    this.complete = Math.round(this.summaryDetails.totalPrice * 0.5);
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-20bda4b8","hasScoped":true,"optionsId":"2","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/paymentSummaryV2.vue
var paymentSummaryV2_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{domProps:{"innerHTML":_vm._s(_vm.formatter.boqDetails.project.boq_payment_stages)}})}
var paymentSummaryV2_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/paymentSummaryV2.vue
function paymentSummaryV2_injectStyle (context) {
  __webpack_require__("7e6a")
}
/* script */


/* template */

/* template functional */
var paymentSummaryV2_vue_template_functional_ = false
/* styles */
var paymentSummaryV2_vue_styles_ = paymentSummaryV2_injectStyle
/* scopeId */
var paymentSummaryV2_vue_scopeId_ = "data-v-20bda4b8"
/* moduleIdentifier (server only) */
var paymentSummaryV2_vue_module_identifier_ = null

var paymentSummaryV2_Component = normalizeComponent(
  paymentSummaryV2,
  paymentSummaryV2_render,
  paymentSummaryV2_staticRenderFns,
  paymentSummaryV2_vue_template_functional_,
  paymentSummaryV2_vue_styles_,
  paymentSummaryV2_vue_scopeId_,
  paymentSummaryV2_vue_module_identifier_
)

/* harmony default export */ var components_paymentSummaryV2 = (paymentSummaryV2_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/ImageArrow.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var ImageArrow = ({
  data: function data() {
    return {};
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-1a074f8d","hasScoped":false,"optionsId":"2","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/ImageArrow.vue
var ImageArrow_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('svg',{attrs:{"width":"30px","height":"30px","viewBox":"0 0 64 64","version":"1.1","xmlns":"http://www.w3.org/2000/svg","xmlns:xlink":"http://www.w3.org/1999/xlink"}},[_c('title',[_vm._v("Group 3")]),_vm._v(" "),_c('desc',[_vm._v("Created with Sketch.")]),_vm._v(" "),_c('g',{attrs:{"id":"Page-1","stroke":"none","stroke-width":"1","fill":"none","fill-rule":"evenodd"}},[_c('g',{attrs:{"id":"Group-3"}},[_c('ellipse',{attrs:{"id":"Oval-Copy","fill":"#fff","opacity":"0.7","cx":"32","cy":"31.830837","rx":"32","ry":"31.830837"}}),_vm._v(" "),_c('g',{attrs:{"id":"ic-round-keyboard-arrow-right-copy","transform":"translate(32.000000, 31.703965) scale(-1, 1) rotate(-180.000000) translate(-32.000000, -31.703965) translate(8.000000, 7.703965)","fill-rule":"nonzero"}},[_c('g',{attrs:{"id":"Icon","transform":"translate(16.000000, 11.936564)","fill":"#000"}},[_c('path',{attrs:{"d":"M2.58,19.6555419 L10.34,11.9365639 L2.58,4.2175859 C2.07625551,3.71650439 1.87952088,2.98616216 2.06390416,2.30167208 C2.24828743,1.61718199 2.7857764,1.08253437 3.47390417,0.899125809 C4.16203194,0.715717247 4.89625552,0.911411864 5.4,1.41249339 L14.58,10.5439648 C15.36,11.3198414 15.36,12.5731806 14.58,13.3490573 L5.4,22.4805286 C5.02631081,22.8530053 4.51901894,23.0623248 3.99,23.0623248 C3.46098106,23.0623248 2.95368919,22.8530053 2.58,22.4805286 C1.82,21.704652 1.8,20.4314185 2.58,19.6555419 Z","id":"Icon-Path"}})]),_vm._v(" "),_c('rect',{attrs:{"id":"ViewBox","x":"0","y":"0","width":"48","height":"47.7462555"}})])])])])}
var ImageArrow_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/ImageArrow.vue
/* script */


/* template */

/* template functional */
var ImageArrow_vue_template_functional_ = false
/* styles */
var ImageArrow_vue_styles_ = null
/* scopeId */
var ImageArrow_vue_scopeId_ = null
/* moduleIdentifier (server only) */
var ImageArrow_vue_module_identifier_ = null

var ImageArrow_Component = normalizeComponent(
  ImageArrow,
  ImageArrow_render,
  ImageArrow_staticRenderFns,
  ImageArrow_vue_template_functional_,
  ImageArrow_vue_styles_,
  ImageArrow_vue_scopeId_,
  ImageArrow_vue_module_identifier_
)

/* harmony default export */ var components_ImageArrow = (ImageArrow_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/LsCarousel.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ var LsCarousel = ({
    components: {
        arrow: components_ImageArrow
    },
    props: {
        images: Array
    },
    data: function data() {
        return {
            activeImage: 0,
            window: {
                width: 0,
                height: 0
            }
        };
    },
    created: function created() {
        window.addEventListener('resize', this.handleResize);
        this.handleResize();
    },
    destroyed: function destroyed() {
        window.removeEventListener('resize', this.handleResize);
    },

    computed: {
        currentImage: function currentImage() {
            return this.images[this.activeImage];
        }
    },
    methods: {
        nextImage: function nextImage() {
            var active = this.activeImage + 1;
            if (active >= this.images.length) {
                active = 0;
            }
            this.activateImage(active);
        },
        prevImage: function prevImage() {
            var active = this.activeImage - 1;
            if (active < 0) {
                active = this.images.length - 1;
            }
            this.activateImage(active);
        },
        activateImage: function activateImage(imageIndex) {
            this.activeImage = imageIndex;
        },
        handleResize: function handleResize() {
            this.window.width = window.innerWidth;
            this.window.height = window.innerHeight;
        }
    }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-2554e4e7","hasScoped":false,"optionsId":"2","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/LsCarousel.vue
var LsCarousel_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"card-carousel"},[_c('div',{staticClass:"card-img"},[_c('img',{staticClass:"img-responsive",attrs:{"src":_vm.currentImage,"alt":"big image"}}),_vm._v(" "),(_vm.images.length>1)?_c('div',{staticClass:"actions"},[_c('span',{staticClass:"prev",on:{"click":_vm.prevImage}},[_c('arrow')],1),_vm._v(" "),_c('span',{staticClass:"next",on:{"click":_vm.nextImage}},[_c('arrow')],1)]):_vm._e()]),_vm._v(" "),(_vm.window.width > 500)?_c('div',{staticClass:"thumbnails"},_vm._l((_vm.images),function(image,index){return _c('div',{class:['thumbnail-image', (_vm.activeImage == index) ? 'active' : ''],on:{"click":function($event){return _vm.activateImage(index)}}},[_c('img',{attrs:{"src":image}})])}),0):_vm._e()])}
var LsCarousel_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/LsCarousel.vue
function LsCarousel_injectStyle (context) {
  __webpack_require__("699e")
}
/* script */


/* template */

/* template functional */
var LsCarousel_vue_template_functional_ = false
/* styles */
var LsCarousel_vue_styles_ = LsCarousel_injectStyle
/* scopeId */
var LsCarousel_vue_scopeId_ = null
/* moduleIdentifier (server only) */
var LsCarousel_vue_module_identifier_ = null

var LsCarousel_Component = normalizeComponent(
  LsCarousel,
  LsCarousel_render,
  LsCarousel_staticRenderFns,
  LsCarousel_vue_template_functional_,
  LsCarousel_vue_styles_,
  LsCarousel_vue_scopeId_,
  LsCarousel_vue_module_identifier_
)

/* harmony default export */ var components_LsCarousel = (LsCarousel_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/DownArrow.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var DownArrow = ({
  data: function data() {
    return {};
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-19624965","hasScoped":false,"optionsId":"2","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/DownArrow.vue
var DownArrow_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('svg',{attrs:{"width":"13px","height":"8px","viewBox":"0 0 13 8","version":"1.1","xmlns":"http://www.w3.org/2000/svg","xmlns:xlink":"http://www.w3.org/1999/xlink"}},[_c('title',[_vm._v("Down Arrow")]),_vm._v(" "),_c('g',{attrs:{"id":"Page-1","stroke":"none","stroke-width":"1","fill":"none","fill-rule":"evenodd"}},[_c('g',{attrs:{"id":"04--Kitchen-View-Appliances-Expanded","transform":"translate(-326.000000, -1333.000000)","fill":"#3D3D3D"}},[_c('g',{attrs:{"id":"Group-7","transform":"translate(0.000000, 105.000000)"}},[_c('g',{attrs:{"id":"Kitchen","transform":"translate(0.000000, 1172.000000)"}},[_c('g',{attrs:{"id":"Group-4","transform":"translate(0.000000, 37.000000)"}},[_c('polygon',{attrs:{"id":"ic_expand_collapse","points":"327.930286 19.424 332.514286 24.032 337.098286 19.424 338.514286 20.84 332.514286 26.84 326.514286 20.84"}})])])])])])])}
var DownArrow_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/DownArrow.vue
/* script */


/* template */

/* template functional */
var DownArrow_vue_template_functional_ = false
/* styles */
var DownArrow_vue_styles_ = null
/* scopeId */
var DownArrow_vue_scopeId_ = null
/* moduleIdentifier (server only) */
var DownArrow_vue_module_identifier_ = null

var DownArrow_Component = normalizeComponent(
  DownArrow,
  DownArrow_render,
  DownArrow_staticRenderFns,
  DownArrow_vue_template_functional_,
  DownArrow_vue_styles_,
  DownArrow_vue_scopeId_,
  DownArrow_vue_module_identifier_
)

/* harmony default export */ var components_DownArrow = (DownArrow_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/ProductDetails.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var ProductDetails = ({
    components: {
        arrow: components_DownArrow
    },
    props: {
        open: Boolean,
        product: {}
    },
    methods: {
        closeProductDetails: function closeProductDetails() {
            this.$emit('closeProductDetails');
        }
    }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-33ab3dbd","hasScoped":false,"optionsId":"2","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/ProductDetails.vue
var ProductDetails_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.open)?_c('div',{class:{ openProductDetials : _vm.open },attrs:{"id":"product-detials"}},[_c('div',{staticClass:"product-detials-wrap"},[_c('div',{staticClass:"header"},[_c('span',{staticStyle:{"transform":"rotateZ(90deg)","display":"inline-block"},on:{"click":_vm.closeProductDetails}},[_c('arrow')],1),_vm._v(" "),_c('span',{staticClass:"header-text"},[_vm._v("Product Details")])]),_vm._v(" "),_c('div',{staticClass:"details"},[_c('p',{staticClass:"product-name"},[_vm._v(_vm._s(_vm.product.display_name))]),_vm._v(" "),_c('div',{staticClass:"product-image"},[_c('img',{attrs:{"src":_vm.product.cover_image_url,"alt":"image"}})]),_vm._v(" "),_c('div',{staticClass:"product-information"},[_c('h5',{staticClass:"heading"},[_vm._v("Product information")]),_vm._v(" "),(_vm.product.dimensions)?_c('p',{staticClass:"info-item"},[_c('span',[_vm._v("Dimension WXDXH(mm)")]),_vm._v(" "),_c('span',{staticClass:"item-type"},[_vm._v(_vm._s(_vm.product.dimensions.width)+" X "+_vm._s(_vm.product.dimensions.depth)+" X "+_vm._s(_vm.product.dimensions.height))])]):_vm._e(),_vm._v(" "),_vm._l((_vm.product.fields),function(value,key){return _c('div',[(value.display_name)?_c('div',{staticClass:"info-item"},[_c('span',{staticClass:"item-name"},[_vm._v(_vm._s(value.display_name))]),_vm._v(" "),_c('div',{staticClass:"item-image"},[(value.option.swatch_image_url)?_c('img',{attrs:{"src":"value.option.swatch_image_url","width":"30","height":"30"}}):_vm._e(),_vm._v(" "),_c('span',[_vm._v(_vm._s(value.option.display_name))])])]):_vm._e()])}),_vm._v(" "),(_vm.product.sku_definition?JSON.parse(_vm.product.sku_definition).measurement?true:false:false)?_c('p',{staticClass:"info-item"},[_c('span',[_vm._v("Measurment("+_vm._s(JSON.parse(_vm.product.sku_definition).unit)+")")]),_vm._v(" "),_c('span',{staticClass:"item-type"},[_vm._v(_vm._s(JSON.parse(_vm.product.sku_definition).measurement))])]):_vm._e()],2)])])]):_vm._e()}
var ProductDetails_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/ProductDetails.vue
function ProductDetails_injectStyle (context) {
  __webpack_require__("b7fc")
}
/* script */


/* template */

/* template functional */
var ProductDetails_vue_template_functional_ = false
/* styles */
var ProductDetails_vue_styles_ = ProductDetails_injectStyle
/* scopeId */
var ProductDetails_vue_scopeId_ = null
/* moduleIdentifier (server only) */
var ProductDetails_vue_module_identifier_ = null

var ProductDetails_Component = normalizeComponent(
  ProductDetails,
  ProductDetails_render,
  ProductDetails_staticRenderFns,
  ProductDetails_vue_template_functional_,
  ProductDetails_vue_styles_,
  ProductDetails_vue_scopeId_,
  ProductDetails_vue_module_identifier_
)

/* harmony default export */ var components_ProductDetails = (ProductDetails_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/back.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var back = ({
  data: function data() {
    return {};
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-52c90c87","hasScoped":false,"optionsId":"2","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/back.vue
var back_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('svg',{attrs:{"width":"24px","height":"24px","viewBox":"0 0 24 24","version":"1.1","xmlns":"http://www.w3.org/2000/svg","xmlns:xlink":"http://www.w3.org/1999/xlink"}},[_c('title',[_vm._v("8C02861A-5A37-442F-A45F-EA29AF3D1BB3")]),_vm._v(" "),_c('desc',[_vm._v("Created with sketchtool.")]),_vm._v(" "),_c('g',{attrs:{"id":"Final","stroke":"none","stroke-width":"1","fill":"none","fill-rule":"evenodd"}},[_c('g',{attrs:{"id":"9.-icons","transform":"translate(-252.000000, -251.000000)","fill":"#D0021B","fill-rule":"nonzero"}},[_c('g',{attrs:{"id":"Group-17","transform":"translate(252.000000, 251.000000)"}},[_c('g',{attrs:{"id":"ion-ios-arrow-round-back","transform":"translate(3.000000, 6.000000)"}},[_c('g',{attrs:{"id":"Icon"}},[_c('path',{attrs:{"d":"M6.525,0.23125 C6.84375,0.54375 6.84375,1.0625 6.53125,1.38125 L2.7375,5.1875 L17.19375,5.1875 C17.6375,5.1875 18,5.55 18,6 C18,6.45 17.6375,6.8125 17.19375,6.8125 L2.7375,6.8125 L6.5375,10.61875 C6.85,10.9375 6.84375,11.45 6.53125,11.76875 C6.2125,12.08125 5.70625,12.08125 5.3875,11.7625 L0.2375,6.575 C0.16875,6.5 0.1125,6.41875 0.06875,6.31875 C0.025,6.21875 0.00625,6.1125 0.00625,6.00625 C0.00625,5.79375 0.0875,5.59375 0.2375,5.4375 L5.3875,0.25 C5.69375,-0.075 6.20625,-0.08125 6.525,0.23125 L6.525,0.23125 Z","id":"Icon-Path"}})])])])])])])}
var back_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/back.vue
/* script */


/* template */

/* template functional */
var back_vue_template_functional_ = false
/* styles */
var back_vue_styles_ = null
/* scopeId */
var back_vue_scopeId_ = null
/* moduleIdentifier (server only) */
var back_vue_module_identifier_ = null

var back_Component = normalizeComponent(
  back,
  back_render,
  back_staticRenderFns,
  back_vue_template_functional_,
  back_vue_styles_,
  back_vue_scopeId_,
  back_vue_module_identifier_
)

/* harmony default export */ var components_back = (back_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/Drawer.vue


//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ var Drawer = ({

    props: {
        open: Boolean,
        content: Number,
        formatter: Object,
        roomId: Number
    },
    components: {
        Carousel: components_LsCarousel,
        ProductDetails: components_ProductDetails,
        CostList: components_CostList,
        back: components_back
    },
    data: function data() {
        return {
            isProductDetailsOpen: false,
            window: {
                width: window.innerWidth
            },
            roomDetails: {},
            isLoaded: {
                value: false
            },
            selectedProduct: null
        };
    },

    methods: {
        closeDrawer: function closeDrawer() {
            this.$emit('closeDrawer');
            this.isLoaded.value = false;
        },
        openProductDetails: function openProductDetails(sku) {
            this.isProductDetailsOpen = true;
            this.selectedProduct = sku;
        },
        closeProductDetailsFn: function closeProductDetailsFn() {
            this.isProductDetailsOpen = false;
        },
        getScopesWithTotal: function getScopesWithTotal(scopes) {
            return entries_default()(scopes).filter(function (_ref) {
                var _ref2 = slicedToArray_default()(_ref, 2),
                    name = _ref2[0],
                    value = _ref2[1];

                return !!value.total;
            }).map(function (_ref3) {
                var _ref4 = slicedToArray_default()(_ref3, 2),
                    name = _ref4[0],
                    value = _ref4[1];

                return {
                    currencyvalue: value.total,
                    name: name,
                    color: scopeColors[name.toUpperCase()] || "#2A465B"
                };
            });
        }
    },
    watch: {
        open: function open(val) {
            console.log(val);
            if (val) {
                this.roomDetails = this.formatter.getRoomDetails(this.roomId, this.isLoaded);
            }
        }
    }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-127c3aa8","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/Drawer.vue
var Drawer_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('transition',{attrs:{"name":"drawer"}},[(_vm.open)?_c('div',{class:{open: _vm.open},attrs:{"id":"drawer"},on:{"click":function($event){if($event.target !== $event.currentTarget){ return null; }return _vm.closeDrawer($event)}}},[_c('div',{staticClass:"drawer-body"},[(_vm.isLoaded.value)?_c('div',[_c('div',{staticClass:"back-nav-container",on:{"click":_vm.closeDrawer}},[_c('div',{staticClass:"btn-close"},[_c('back')],1),_vm._v(" "),_c('div',{staticStyle:{"color":"#D0021B","margin-left":"5px"}},[_vm._v("Back to sections")])]),_vm._v(" "),_c('div',{staticClass:"header"},[_c('span',{staticClass:"header-text"},[_vm._v(_vm._s(_vm.roomDetails.display_name))]),_vm._v(" "),_c('span',{staticClass:"header-text"},[_vm._v(_vm._s(_vm._f("toCurrency")(_vm.roomDetails.total,_vm.formatter.currencyType, _vm.formatter.countryISOCode)))])]),_vm._v(" "),(_vm.roomDetails.images.length>0)?_c('div',{staticClass:"img-carousel"},[_c('Carousel',{attrs:{"images":_vm.roomDetails.images}})],1):_vm._e(),_vm._v(" "),_c('CostList',{attrs:{"listElements":_vm.getScopesWithTotal(_vm.roomDetails.scopes),"showchevron":false,"formatter":_vm.formatter,"compact":true}}),_vm._v(" "),_c('div',{staticStyle:{"height":"15px"},attrs:{"role":"spacer"}}),_vm._v(" "),_c('div',{staticClass:"room-sections"},_vm._l((_vm.roomDetails.scopes),function(value,propertyName){return (value.skus.length>0)?_c('div',{staticStyle:{"margin-bottom":"30px"}},[_c('div',{staticClass:"tag"},[_c('h5',{staticClass:"tag-name"},[_c('span',[_vm._v(_vm._s(propertyName))])]),_vm._v(" "),_c('h5',{staticClass:"tag-price"},[_vm._v(_vm._s(_vm._f("toCurrency")(value.total,_vm.formatter.currencyType, _vm.formatter.countryISOCode)))])]),_vm._v(" "),_c('div',{staticClass:"room-section-type"},_vm._l((value.skus),function(sku,index){return _c('div',{staticClass:"product-tile",staticStyle:{"background-color":"#f6f7f7"}},[_c('div',{staticClass:"product-tile-wrap",staticStyle:{"height":"150px","padding":"10px","border-bottom":"solid 1px transparent"}},[_c('div',{staticClass:"product-image",staticStyle:{"margin-right":"26px"}},[_c('img',{attrs:{"src":_vm.roomDetails.items.get(sku)['cover_image_url'],"alt":"","onError":"javascript:this.onerror=null;this.src='https://cdn.livmatrix.com//img/logos/ruby/livspace-square.png';"}})]),_vm._v(" "),_c('div',{staticClass:"product-body"},[_c('p',{staticClass:"product-info",staticStyle:{"font-weight":"700"}},[_c('span',{staticClass:"label-box"},[_vm._v(_vm._s(_vm.roomDetails.items.get(sku)['display_name']))]),_vm._v(" "),_c('span',[_vm._v(_vm._s(_vm._f("toCurrency")(value.skuPrice.get(sku)*value.skuQuantity.get(sku),_vm.formatter.currencyType, _vm.formatter.countryISOCode)))])]),_vm._v(" "),_c('div',{staticStyle:{"display":"flex","flex-direction":"column","align-items":"flex-start"}},[_c('div',{staticClass:"product-price",staticStyle:{"display":"flex","justify-content":"space-between","padding-right":"10px","align-items":"flex-end","margin-bottom":"10px"}},[_c('div',[_c('p',{staticStyle:{"font-size":"12px","margin-bottom":"0px","color":"#585858"}},[_c('span',[_vm._v("Qty: "+_vm._s(value.skuQuantity.get(sku)))])]),_vm._v(" "),_c('p',{staticStyle:{"font-size":"12px","margin-bottom":"0px","color":"#585858"}},[_c('span',[_vm._v("Unit Price: "+_vm._s(_vm._f("toCurrency")(value.skuPrice.get(sku),_vm.formatter.currencyType, _vm.formatter.countryISOCode)))])]),_vm._v(" "),(_vm.roomDetails.items.get(sku).sku_definition?JSON.parse(_vm.roomDetails.items.get(sku).sku_definition).measurement?true:false:false)?_c('p',{staticStyle:{"font-size":"12px","margin-bottom":"0px","color":"#585858"}},[_c('span',[_vm._v("Measurment("+_vm._s(JSON.parse(_vm.roomDetails.items.get(sku).sku_definition).unit)+"): "+_vm._s(JSON.parse(_vm.roomDetails.items.get(sku).sku_definition).measurement))])]):_vm._e(),_vm._v(" "),(!value.children)?_c('p',{staticClass:"product-price",staticStyle:{"display":"flex","justify-content":"space-between","padding-right":"10px"}},[_c('span',{staticClass:"go-to-details",on:{"click":function($event){return _vm.openProductDetails(sku)}}},[_vm._v("Details "),_c('span',{staticClass:"glyphicon glyphicon-menu-right"})])]):_vm._e()])]),_vm._v(" "),(value.children)?_c('b-button',{directives:[{name:"b-toggle",rawName:"v-b-toggle",value:('collapse'+sku),expression:"'collapse'+sku"}],staticClass:"go-to-details toggle-btn"},[_vm._v("Toggle details")]):_vm._e()],1)])]),_vm._v(" "),(value.children)?_c('b-collapse',{attrs:{"visible":"","id":'collapse'+sku}},[_c('b-card',_vm._l((value.children.get(sku)),function(child){return _c('div',{staticClass:"product-tile"},[_c('div',{staticClass:"product-tile-wrap"},[_c('div',{staticClass:"product-image",staticStyle:{"margin-right":"15px"}},[_c('img',{attrs:{"src":_vm.roomDetails.items.get(child.sku_code)['cover_image_url'],"alt":"product"}})]),_vm._v(" "),_c('div',{staticClass:"product-body"},[_c('p',{staticClass:"product-info",staticStyle:{"font-weight":"700"}},[_c('span',{staticClass:"label-box"},[_vm._v(_vm._s(_vm.roomDetails.items.get(child.sku_code)['display_name']))]),_vm._v(" "),_c('span',[_vm._v(_vm._s(_vm._f("toCurrency")(value.childSkuPrices.get(child.sku_code)*child.quantity,_vm.formatter.currencyType, _vm.formatter.countryISOCode)))])]),_vm._v(" "),_c('p',{staticStyle:{"font-size":"12px","margin-bottom":"0px","color":"#585858"}},[_c('span',[_vm._v("Qty: "+_vm._s(child.quantity))])]),_vm._v(" "),_c('p',{staticStyle:{"font-size":"12px","margin-bottom":"0px","color":"#585858"}},[_c('span',[_vm._v("Unit Price: "+_vm._s(_vm._f("toCurrency")(value.childSkuPrices.get(child.sku_code),_vm.formatter.currencyType, _vm.formatter.countryISOCode)))])]),_vm._v(" "),_c('p',{staticClass:"product-price",staticStyle:{"display":"flex","justify-content":"space-between","padding-right":"10px"}},[_c('span',{staticClass:"go-to-details",on:{"click":function($event){return _vm.openProductDetails(child.sku_code)}}},[_vm._v("Details "),_c('span',{staticClass:"glyphicon glyphicon-menu-right"})])])])])])}),0)],1):_vm._e()],1)}),0)]):_vm._e()}),0)],1):_vm._e(),_vm._v(" "),(!_vm.isLoaded.value)?_c('div',{staticStyle:{"text-align":"center","height":"100vh"}},[_c('div',{staticClass:"ls-loader-circle"},[_c('div'),_vm._v(" "),_c('div'),_vm._v(" "),_c('div'),_vm._v(" "),_c('div')])]):_vm._e()]),_vm._v(" "),(_vm.isProductDetailsOpen)?_c('ProductDetails',{attrs:{"open":_vm.isProductDetailsOpen,"product":_vm.roomDetails.items.get(_vm.selectedProduct)},on:{"closeProductDetails":_vm.closeProductDetailsFn}}):_vm._e()],1):_vm._e()])}
var Drawer_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/Drawer.vue
function Drawer_injectStyle (context) {
  __webpack_require__("ff4b")
}
/* script */


/* template */

/* template functional */
var Drawer_vue_template_functional_ = false
/* styles */
var Drawer_vue_styles_ = Drawer_injectStyle
/* scopeId */
var Drawer_vue_scopeId_ = null
/* moduleIdentifier (server only) */
var Drawer_vue_module_identifier_ = null

var Drawer_Component = normalizeComponent(
  Drawer,
  Drawer_render,
  Drawer_staticRenderFns,
  Drawer_vue_template_functional_,
  Drawer_vue_styles_,
  Drawer_vue_scopeId_,
  Drawer_vue_module_identifier_
)

/* harmony default export */ var components_Drawer = (Drawer_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/RoomsV2.vue


//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ var RoomsV2 = ({
  components: {
    Carousel: components_LsCarousel,
    Drawer: components_Drawer,
    CostList: components_CostList
  },
  props: {
    formatter: {
      type: Object,
      required: true
    }
  },
  data: function data() {
    return {
      roomsData: {},
      window: {
        width: window.innerWidth
      },
      selectedRoomId: 0,
      isDrawerOpen: false

    };
  },

  methods: {
    openDrawer: function openDrawer(val) {
      // console.log("roomClicked");
      this.isDrawerOpen = true;
      this.selectedRoomId = val;
      this.$emit('openRoom', val);
    },
    getScopesWithTotal: function getScopesWithTotal(scopes) {
      return entries_default()(scopes).filter(function (_ref) {
        var _ref2 = slicedToArray_default()(_ref, 2),
            name = _ref2[0],
            value = _ref2[1];

        return !!value.total;
      }).map(function (_ref3) {
        var _ref4 = slicedToArray_default()(_ref3, 2),
            name = _ref4[0],
            value = _ref4[1];

        return {
          currencyvalue: value.total,
          name: name,
          color: scopeColors[name.toUpperCase()] || "#2A465B"
        };
      });
    },
    getLegendColor: function getLegendColor(value) {
      return { 'background-color': '#C2E7D5'
        // const indexofroom = this.roomOptions.labels.indexOf(value[0]);
        // const colorsarraylength = this.roomOptions.colors.length;
        // return {'background-color': this.roomOptions.colors[indexofroom % colorsarraylength]}
      };
    }
  },
  mounted: function mounted() {
    window.vm = this;
    this.roomsData = this.formatter.getRoomsDetails();
  },
  computed: {
    roomsWithTotal: function roomsWithTotal() {
      if (this.roomsData.filter) {
        return this.roomsData.filter(function (room) {
          return !!room.total;
        });
      } else {
        return [];
      }
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-e2e756e8","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/RoomsV2.vue
var RoomsV2_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('div',{attrs:{"id":"rooms"}},[_c('div',{staticClass:"room-header"},[_c('h1',{staticClass:"heading"},[_vm._v(_vm._s(_vm.roomsData.validRooms)+" Sections ")]),_vm._v(" "),_c('p',{staticClass:"subtext"},[_vm._v("Check out the design recommendations for each section and their expected cost.")])]),_vm._v(" "),_c('div',{staticClass:"rooms-list"},[_c('masonry',{attrs:{"cols":(_vm.window.width > 500) ? 2 : 1,"gutter":(_vm.window.width > 500) ? 50 : 0}},_vm._l((_vm.roomsWithTotal),function(room,index){return _c('div',{key:index,staticClass:"room-card"},[_c('h3',{staticClass:"title"},[_c('span',[_vm._v(_vm._s(room.display_name))]),_vm._v(" "),_c('span',[_vm._v(_vm._s(_vm._f("toCurrency")(room.total,_vm.formatter.currencyType, _vm.formatter.countryISOCode)))])]),_vm._v(" "),(room.images.length)?_c('Carousel',{attrs:{"images":room.images}}):_vm._e(),_vm._v(" "),_c('div',{staticClass:"about-room",on:{"click":function($event){return _vm.openDrawer(room.id)}}},[(room.tags.length>0)?_c('h4',{staticClass:"hash-tags"},_vm._l((room.tags),function(tag,index){return _c('span',{key:index},[_vm._v("\n                  #"+_vm._s(tag.name)+"\n                ")])}),0):_vm._e(),_vm._v(" "),(room.description)?_c('p',{staticClass:"text"},[_vm._v(_vm._s(room.description))]):_vm._e(),_vm._v(" "),_c('CostList',{attrs:{"listElements":_vm.getScopesWithTotal(room.scopes),"showchevron":false,"formatter":_vm.formatter,"compact":true}}),_vm._v(" "),_c('p',{staticClass:"all-products-link"},[_vm._v(" View product details")])],1)],1)}),0)],1)])])}
var RoomsV2_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/RoomsV2.vue
function RoomsV2_injectStyle (context) {
  __webpack_require__("a58e")
}
/* script */


/* template */

/* template functional */
var RoomsV2_vue_template_functional_ = false
/* styles */
var RoomsV2_vue_styles_ = RoomsV2_injectStyle
/* scopeId */
var RoomsV2_vue_scopeId_ = null
/* moduleIdentifier (server only) */
var RoomsV2_vue_module_identifier_ = null

var RoomsV2_Component = normalizeComponent(
  RoomsV2,
  RoomsV2_render,
  RoomsV2_staticRenderFns,
  RoomsV2_vue_template_functional_,
  RoomsV2_vue_styles_,
  RoomsV2_vue_scopeId_,
  RoomsV2_vue_module_identifier_
)

/* harmony default export */ var components_RoomsV2 = (RoomsV2_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/LivhomeVideo.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var LivhomeVideo = ({
    components: {
        arrow: components_ImageArrow
    },
    props: {
        formatter: {
            type: Object,
            required: true
        }
    },
    data: function data() {
        return {
            videoUrls: [],
            activeImage: 0,
            window: {
                width: 0,
                height: 0
            },
            runSlow: 0.1
        };
    },
    created: function created() {
        this.videoUrls = this.formatter.getLivHomeVideoUrls();
        window.addEventListener('resize', this.handleResize);
        this.handleResize();
    },
    destroyed: function destroyed() {
        window.removeEventListener('resize', this.handleResize);
    },

    computed: {
        currentImage: function currentImage() {
            return this.videoUrls[this.activeImage];
        }
    },
    mounted: function mounted() {
        var video = document.getElementById("video");
        if (video) video.defaultPlaybackRate = 0.6;
    },

    methods: {
        nextImage: function nextImage() {
            var active = this.activeImage + 1;
            if (active >= this.videoUrls.length) {
                active = 0;
            }
            this.activateImage(active);
        },
        prevImage: function prevImage() {
            var active = this.activeImage - 1;
            if (active < 0) {
                active = this.videoUrls.length - 1;
            }
            this.activateImage(active);
        },
        activateImage: function activateImage(imageIndex) {
            this.activeImage = imageIndex;
        },
        handleResize: function handleResize() {
            this.window.width = window.innerWidth;
            this.window.height = window.innerHeight;
        }
    }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-dc20297c","hasScoped":false,"optionsId":"2","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/LivhomeVideo.vue
var LivhomeVideo_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.videoUrls.length>0)?_c('div',{staticClass:"card-carousel"},[_c('div',{staticClass:"card-img"},[_c('video',{key:_vm.currentImage,staticStyle:{"width":"100%","height":"100%"},attrs:{"id":"video","autoplay":"","loop":"","controls":"controls"}},[_c('source',{attrs:{"src":_vm.currentImage,"height":"552","type":"video/mp4"}}),_vm._v("\n          Your browser does not support the video tag.\n       ")]),_vm._v(" "),_c('div',{staticClass:"actions"},[_c('span',{staticClass:"prev",on:{"click":_vm.prevImage}},[_c('arrow')],1),_vm._v(" "),_c('span',{staticClass:"next",on:{"click":_vm.nextImage}},[_c('arrow')],1)])])]):_vm._e()}
var LivhomeVideo_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/LivhomeVideo.vue
function LivhomeVideo_injectStyle (context) {
  __webpack_require__("ce75")
}
/* script */


/* template */

/* template functional */
var LivhomeVideo_vue_template_functional_ = false
/* styles */
var LivhomeVideo_vue_styles_ = LivhomeVideo_injectStyle
/* scopeId */
var LivhomeVideo_vue_scopeId_ = null
/* moduleIdentifier (server only) */
var LivhomeVideo_vue_module_identifier_ = null

var LivhomeVideo_Component = normalizeComponent(
  LivhomeVideo,
  LivhomeVideo_render,
  LivhomeVideo_staticRenderFns,
  LivhomeVideo_vue_template_functional_,
  LivhomeVideo_vue_styles_,
  LivhomeVideo_vue_scopeId_,
  LivhomeVideo_vue_module_identifier_
)

/* harmony default export */ var components_LivhomeVideo = (LivhomeVideo_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/TermsAndCond.vue
//
//
//
//
//

/* harmony default export */ var TermsAndCond = ({
  name: 'TermsAndConditions',

  data: function data() {
    return {
      more: true,
      isLoaded: {
        value: false
      }
    };
  },

  props: {
    formatter: {
      type: Object,
      required: true
    }
  },
  created: function created() {
    this.formatter.getTnCContent(this.isLoaded);
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-2c9777bc","hasScoped":true,"optionsId":"2","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/TermsAndCond.vue
var TermsAndCond_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.isLoaded.value)?_c('div',{domProps:{"innerHTML":_vm._s(_vm.formatter.boqDetails.project.boq_tnc)}}):_vm._e()}
var TermsAndCond_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/TermsAndCond.vue
function TermsAndCond_injectStyle (context) {
  __webpack_require__("a022")
}
/* script */


/* template */

/* template functional */
var TermsAndCond_vue_template_functional_ = false
/* styles */
var TermsAndCond_vue_styles_ = TermsAndCond_injectStyle
/* scopeId */
var TermsAndCond_vue_scopeId_ = "data-v-2c9777bc"
/* moduleIdentifier (server only) */
var TermsAndCond_vue_module_identifier_ = null

var TermsAndCond_Component = normalizeComponent(
  TermsAndCond,
  TermsAndCond_render,
  TermsAndCond_staticRenderFns,
  TermsAndCond_vue_template_functional_,
  TermsAndCond_vue_styles_,
  TermsAndCond_vue_scopeId_,
  TermsAndCond_vue_module_identifier_
)

/* harmony default export */ var components_TermsAndCond = (TermsAndCond_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/CustomerBOQV2.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//











/* harmony default export */ var CustomerBOQV2 = ({
  name: 'CustomerBOQV2',
  components: {
    'quote-summary': components_SummaryCard,
    'designer': components_DesignerCard,
    'cost-breakout': components_CostBreakout,
    'rooms': components_RoomsV2,
    'payment': components_paymentSummaryV2,
    'ls-video': components_LivhomeVideo,
    'terms-conds': components_TermsAndCond,
    'Drawer': components_Drawer
  },
  methods: {
    scroll: function scroll() {
      window.scrollTo(0, 0);
    },
    openRoom: function openRoom(roomId) {
      this.selectedRoomId = roomId;
      this.isDrawerOpen = true;
    },
    closeDrawer: function closeDrawer() {
      this.isDrawerOpen = false;
    }
  },
  props: {
    dataClient: {
      type: Object,
      required: true
    },
    boqId: {
      type: Number,
      required: true
    },
    back: {
      type: String,
      required: false,
      default: '/boqs'
    }
  },
  data: function data() {
    return {
      isLoaded: {
        value: false
      },
      videoLoader: {
        value: false
      },
      data: {},
      formatter: null,
      selectedRoomId: 0,
      isDrawerOpen: false
    };
  },

  provide: function provide() {
    return {
      formatter: this.formatter
    };
  },
  created: function created() {
    this.formatter = new boqV2_BOQFormatter(this.dataClient, this.boqId);
    this.formatter.setBOQDetails(this.boqId, this.isLoaded, this.videoLoader);
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-c98c6cbc","hasScoped":false,"optionsId":"1","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/CustomerBOQV2.vue
var CustomerBOQV2_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[(!_vm.isLoaded.value)?_c('div',{staticClass:"w-100 full-page-size"},[_vm._m(0)]):_vm._e(),_vm._v(" "),(_vm.isLoaded.value)?_c('b-tabs',[_c('b-tab',{attrs:{"active":"","title":"Summary"},on:{"click":_vm.scroll}},[_c('div',{staticClass:"tab-body-wrap"},[_c('h4',[_vm._v("Hi "+_vm._s(_vm.formatter.boqDetails.project.customer_display_name.trim())+",")]),_vm._v(" "),_c('p',{staticStyle:{"color":"#676767","margin-bottom":"30px"}},[_vm._v("Here is the quote that you requested. Please review and reach out to\n          "+_vm._s(_vm.formatter.boqDetails.primary_designer &&
            _vm.formatter.boqDetails.primary_designer.display_name)+", your Livspace designer, for any questions.")]),_vm._v(" "),_c('p',{staticClass:"tab-body-title"},[_vm._v(" Summary")]),_vm._v(" "),_c('quote-summary',{attrs:{"formatter":_vm.formatter}}),_vm._v(" "),(_vm.videoLoader.value)?_c('ls-video',{staticStyle:{"margin-bottom":"40px"},attrs:{"formatter":_vm.formatter}}):_vm._e(),_vm._v(" "),_c('p',{staticClass:"tab-body-title"},[_vm._v("Cost breakup")]),_vm._v(" "),_c('cost-breakout',{attrs:{"formatter":_vm.formatter},on:{"openRoom":_vm.openRoom}}),_vm._v(" "),_c('div',{staticClass:"l-row"},[(_vm.formatter.boqDetails.primary_designer)?_c('div',{staticClass:"l-col"},[_c('p',{staticClass:"tab-body-title"},[_vm._v("Your Designer")]),_vm._v(" "),_c('designer',{attrs:{"formatter":_vm.formatter}})],1):_vm._e(),_vm._v(" "),_c('div',{staticClass:"l-col"},[_c('p',{staticClass:"tab-body-title"},[_vm._v("Payment Schedule")]),_vm._v(" "),_c('payment',{attrs:{"formatter":_vm.formatter}})],1)])],1),_vm._v(" "),(_vm.formatter.countryISOCode==='IN')?_c('div',{staticClass:"emi-banner"}):_vm._e(),_vm._v(" "),(_vm.formatter.countryISOCode==='IN')?_c('div',{staticClass:"edge-banner"},[_c('img',{staticClass:"desktop-banner",attrs:{"src":"https://d3gq2merok8n5r.cloudfront.net/img/customer_boq/desktop-edge-banner.jpg"}}),_vm._v(" "),_c('img',{staticClass:"mobile-banner",attrs:{"src":"https://d3gq2merok8n5r.cloudfront.net/img/customer_boq/mobile-edge-banner.jpg"}})]):_vm._e()]),_vm._v(" "),_c('b-tab',{attrs:{"title":"Sections"},on:{"click":_vm.scroll}},[_c('div',{staticClass:"tab-body-wrap"},[_c('rooms',{attrs:{"formatter":_vm.formatter},on:{"openRoom":_vm.openRoom}})],1)]),_vm._v(" "),_c('b-tab',{attrs:{"title":"FAQ/Help"},on:{"click":_vm.scroll}},[_c('div',{staticClass:"tab-body-wrap"},[_c('h2',{staticClass:"tab-body-title mx-3",staticStyle:{"font-size":"2rem !important"}},[_vm._v("Payment Schedule")]),_vm._v(" "),_c('payment',{staticStyle:{"margin":"15px"},attrs:{"formatter":_vm.formatter,"subs":true}}),_vm._v(" "),_c('terms-conds',{staticStyle:{"margin":"15px"},attrs:{"formatter":_vm.formatter}})],1)])],1):_vm._e(),_vm._v(" "),_c('Drawer',{attrs:{"open":_vm.isDrawerOpen,"roomId":_vm.selectedRoomId,"formatter":_vm.formatter},on:{"closeDrawer":_vm.closeDrawer}})],1)}
var CustomerBOQV2_staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('aside',{staticClass:"ls-loader-circle"},[_c('div'),_vm._v(" "),_c('div'),_vm._v(" "),_c('div'),_vm._v(" "),_c('div')])}]

// CONCATENATED MODULE: ./src/components/CustomerBOQV2.vue
function CustomerBOQV2_injectStyle (context) {
  __webpack_require__("f34d")
}
/* script */


/* template */

/* template functional */
var CustomerBOQV2_vue_template_functional_ = false
/* styles */
var CustomerBOQV2_vue_styles_ = CustomerBOQV2_injectStyle
/* scopeId */
var CustomerBOQV2_vue_scopeId_ = null
/* moduleIdentifier (server only) */
var CustomerBOQV2_vue_module_identifier_ = null

var CustomerBOQV2_Component = normalizeComponent(
  CustomerBOQV2,
  CustomerBOQV2_render,
  CustomerBOQV2_staticRenderFns,
  CustomerBOQV2_vue_template_functional_,
  CustomerBOQV2_vue_styles_,
  CustomerBOQV2_vue_scopeId_,
  CustomerBOQV2_vue_module_identifier_
)

/* harmony default export */ var components_CustomerBOQV2 = (CustomerBOQV2_Component.exports);

// CONCATENATED MODULE: ./src/services/formatter/listBoq.js
// import {getProjectBoqs} from '../http/boq.js'

class BOQListFormatter {
  constructor (dataClient) {
    this.data = {
      boqs: {},
      isLoaded: false,
      currencyType : "INR",
      countryISOCode : "IN"
    }
    this.getFormattedData(dataClient)
  }

  processData (data) {
    var fields = [
      { key: 'display_name', label: 'Name' },
      { key: 'total_price', label: 'Price' },
      { key: 'preview', label: 'Preview' }

    ]
    var items = data.data
    // console.log(items);
    for (var item of items) {
      item.preview = ''
    }

    this.data.boqs.items = items
    this.data.boqs.fields = fields

  }

  getFormattedData (dataClient) { // NOTE:
    dataClient.getProjectBoqs().then(data => {
      this.processData(data);
      var proposals = data.data;
      var proposal = null;
      if(proposals.length>0)
        proposal = proposals[0]
      if(proposal){
        if("currency" in proposal)
          this.currencyType = proposal.currency
        if("country_code" in proposal)
          this.countryISOCode = proposal.country_code
      }
      this.data.isLoaded = true;
    }).catch(error => {
      console.log(error)
    })
  }
  getData () {
    // console.log(this.data);
    return this.data
  }
}

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/BOQListing.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

// import Vue from 'vue'


/* harmony default export */ var BOQListing = ({
  components: {},
  props: {
    dataClient: {
      type: Object,
      required: true
      // ,
      // service:{
      //   type:Object,
      //   required: true
      // }
      //
    } },
  data: function data() {
    return {
      data: {},
      formatter: null
    };
  },

  methods: {
    sendEvent: function sendEvent(itemData) {
      var data = {};
      data["actionName"] = "Customer - Quote Viewed";
      data["Quote ID"] = itemData.id;
      data["Quote Expiry"] = itemData.pay_by_date;
      data["Quote Value"] = itemData.total_price;
      this.$emit('user-boq-action', data);
    }
  },
  created: function created() {
    this.formatter = new BOQListFormatter(this.dataClient);
    var data = this.formatter.getData();
    this.data = data;
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-285d4702","hasScoped":false,"optionsId":"2","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/BOQListing.vue
var BOQListing_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_vm._m(0),_vm._v(" "),_c('br'),_vm._v(" "),(_vm.data.isLoaded && _vm.data.boqs.items.length>0)?_c('div',{staticStyle:{"width":"100%","margin":"0 auto"}},[_c('b-table',{attrs:{"striped":"","bordered":"","hover":"","fields":_vm.data.boqs.fields,"items":_vm.data.boqs.items},scopedSlots:_vm._u([{key:"index",fn:function(data){return [_vm._v("\n        "+_vm._s(data.index + 1)+"\n      ")]}},{key:"display_name",fn:function(data){return [_vm._v("\n        "+_vm._s(data.item.identifier)+"\n      ")]}},{key:"total_price",fn:function(data){return [_vm._v("\n        "+_vm._s(_vm._f("toCurrency")(data.item.net_payable,_vm.formatter.currencyType, _vm.formatter.countryISOCode))+"\n      ")]}},{key:"preview",fn:function(data){return [_c('router-link',{attrs:{"to":{ path:''+data.item.id},"append":""},nativeOn:{"click":function($event){return _vm.sendEvent(data.item)}}},[_vm._v("View")])]}}])})],1):_vm._e(),_vm._v(" "),(_vm.data.boqs.items.length==0)?_c('div',[_vm._v("\n    No Shared Quotations\n  ")]):_vm._e()])}
var BOQListing_staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('header',[_c('h1',{staticClass:"display-6"},[_vm._v("Your Quotations")])])}]

// CONCATENATED MODULE: ./src/components/BOQListing.vue
/* script */


/* template */

/* template functional */
var BOQListing_vue_template_functional_ = false
/* styles */
var BOQListing_vue_styles_ = null
/* scopeId */
var BOQListing_vue_scopeId_ = null
/* moduleIdentifier (server only) */
var BOQListing_vue_module_identifier_ = null

var BOQListing_Component = normalizeComponent(
  BOQListing,
  BOQListing_render,
  BOQListing_staticRenderFns,
  BOQListing_vue_template_functional_,
  BOQListing_vue_styles_,
  BOQListing_vue_scopeId_,
  BOQListing_vue_module_identifier_
)

/* harmony default export */ var components_BOQListing = (BOQListing_Component.exports);

// CONCATENATED MODULE: ./src/index.js





// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib-no-default.js
/* concated harmony reexport CustomerBoq */__webpack_require__.d(__webpack_exports__, "CustomerBoq", function() { return components_CustomerBOQV2; });
/* concated harmony reexport BoqListing */__webpack_require__.d(__webpack_exports__, "BoqListing", function() { return components_BOQListing; });




/***/ }),

/***/ "ff4b":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("f493");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("2fb2").default
var update = add("9722ee50", content, true, {});

/***/ })

/******/ });
});
//# sourceMappingURL=livspace-customer-boq.umd.js.map